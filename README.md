# TNClassifier

[![JHEP](https://img.shields.io/static/v1?style=plastic&label=DOI&message=JHEP08(2021)112&color=blue)](https://doi.org/10.1007/JHEP08(2021)112) [![arxiv](https://img.shields.io/static/v1?style=plastic&label=arXiv&message=2106.083348&color=brightgreen)](https://arxiv.org/abs/2106.08334)

Tensor network based classifier. Currently only Matrix Product States (MPS) is available. It is possible to use Density
Matrix Renormalization Group (DMRG) or stochastic gradient decent (SGD) algorithms to optimize the network.

## Installation

`pip install -e .` or use `make`.

### MPS warm-up
```python
import TNClassifier as tnc

mps = tnc.mps.random(N_sites = 10,
                     physical_dim = 2,
                     bond_dim = 20,
                     n_labels = 3, 
                     label_position = "center")

def shapes(mps):
    for ix, t in enumerate(mps):
        print(f"{ix} {t.shape}"+ \
              (ix == mps.center_position)*" <- Center of Orthogonality" +\
              (ix == mps.label_position)*" <- label pos")

shapes(mps)
# output:
# 0 (2, 20) <- Center of Orthogonality
# 1 (20, 2, 20)
# 2 (20, 2, 20)
# 3 (20, 2, 20)
# 4 (20, 2, 20)
# 5 (20, 2, 3, 20) <- label pos
# 6 (20, 2, 20)
# 7 (20, 2, 20)
# 8 (20, 2, 20)
# 9 (20, 2)

mps.canonicalize()
shapes(mps)
# output:
# 0 (2, 2) <- Center of Orthogonality
# 1 (2, 2, 4)
# 2 (4, 2, 8)
# 3 (8, 2, 16)
# 4 (16, 2, 20)
# 5 (20, 2, 3, 16) <- label pos
# 6 (16, 2, 8)
# 7 (8, 2, 4)
# 8 (4, 2, 2)
# 9 (2, 2)

mps = tnc.mps.random(N_sites = 10,
                     physical_dim = 2,
                     bond_dim = 10,
                     n_labels = 3,
                     label_position = "left")
mps.center_position = 7
shapes(mps)
#output:
# 0 (2, 3, 6) <- label pos
# 1 (6, 2, 10)
# 2 (10, 2, 10)
# 3 (10, 2, 10)
# 4 (10, 2, 10)
# 5 (10, 2, 10)
# 6 (10, 2, 10)
# 7 (10, 2, 10) <- Center of Orthogonality
# 8 (10, 2, 10)
# 9 (10, 2)
```
Note that each tensor shows left bond, Hilbert space dimensions, and right bond except the first, last and label tensor.
First tensor only have physical dims and right bond where last tensor only has right bond and physical dims respectively.
The label tensor also includes `n_labels` before the right bond dim. Label dimension indicates number of classes.

### Classification with MPS via DMRG algorithm 
```python
import TNClassifier as tnc
import tensorflow as tf

phi = lambda im: [tf.math.cos(im * np.pi * 0.5), tf.math.sin(im * np.pi * 0.5)]

training = {"bkg" : ["samples/QCD_CovNet_0001.npz"],
            "sig" : ["samples/tt_CovNet_0001.npz"]}

training = tnc.DataGenerator(smp_path_dict = training,
                             batch_size = 128,
                             shuffle = True,
                             label_format = "onehot", # binary has not been tested yet
                             image_slice = 9,         # takes the central part of the image N pixels on one axis 37 - 9*2
                             image_processor = phi,   # processes the data with respect to the given phi function 
                                                      # output shape will be (Nt, 2, eta-pixels * phi-pixels)
                             dtype = tf.float64)

mps = tnc.mps.random(N_sites = 361,       # original image was 37*37 but since its sliced, now it is 19*19 
                     physical_dim = 2,    # Hilbert Space dimensions between an MPS tensor and pixel
                     bond_dim = 20,       # Bond dimensions between tensors
                     n_labels = 2,        # onehot
                     label_position = "centre",
                     initializer = "glorot_normal",
                     dtype = tf.float64,
                     max_truncation_err = None,
                     max_singular_values = 40)

with tnc.TrainingSuite(training_generator=training, network=mps, validation_generator = None) as training:
    history = training.sweep_based_fit(epochs = 100,
                                       n_sweep_per_batch = 1, # number of sweeps to be run for each batch 
                                       learning_rate = 1e-2)
```
if the training is run within the local environment, it will print the error types and how many times they occured during
the optimization process. Otherwise training can be run as follows as well;
```python
training = tnc.TrainingSuite(training_generator=training, network=mps, validation_generator = None)
history = training.sweep_based_fit(epochs = 100,
                                   n_sweep_per_batch = 1, # number of sweeps to be run for each batch 
                                   learning_rate = 1e-2)
```

### Classification via DMRG+SGD algorithm
```python
with tnc.TrainingSuite(training_generator=training, network=mps, validation_generator = None) as training:
    history = training.mixed_fit(
        epochs = 10,
        optimizer = tf.optimizers.get("adam"), # DMRG will use the same LR
        n_sweep_per_epoch = 3, # number of DMRG sweeps per batch
        DMRG_frac = 1./len(training), # fraction of DMRG runs per epoch if fraction is given as 0 it will be trained via SGD
        fixed_label = True,
        epoch_callbacks = [],
    )
```

### MPS Keras Layer
```python
import tensorflow as tf
import TNClassifier as tnc

model = tf.keras.Sequential([
    tf.keras.Input((2,361)),
    tnc.MPSlayer(N_sites = 361,
                 physical_dim = 2,
                 bond_dim = 10,
                 n_labels = 2,
                 label_position = "center")]
)
model.summary()
#output:
# Model: "sequential"
# _________________________________________________________________
# Layer (type)                 Output Shape              Param #   
# =================================================================
# mp_slayer (MPSlayer)         (None, 2)                 72040
# =================================================================
# Total params: 72,040
# Trainable params: 72,040
# Non-trainable params: 0
# _________________________________________________________________
```

## `examples/simpleRun.py`
This is a simple classifier script for to run the MPS shown above. To see all the options type `$ python simpleRun.py -h`.

# Citation

```latex
@article{MPS,
	author = {Araz, Jack Y. and Spannowsky, Michael},
	journal = {Journal of High Energy Physics},
	number = {8},
	pages = {112},
	title = {Quantum-inspired event reconstruction with Tensor Networks: Matrix Product States},
	volume = {2021},
	year = {2021},
    url = {https://doi.org/10.1007/JHEP08(2021)112},
    isbn = {1029-8479},
    doi = {10.1007/JHEP08(2021)112}
}
```

## TODO:

-[ ] Fix the optimization with floating label tensor.

-[ ] SGD algorithm is too heavy on RAM for higher dimensional feature space mappings. Find a way to optimize it.
