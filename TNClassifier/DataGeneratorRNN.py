import numpy      as np
import tensorflow as tf

from TNClassifier.system.exceptions     import InvalidInput
from TNClassifier.MPS.DataGeneratorBase import DataGeneratorBase

from typing                             import Optional, Union, Callable, Tuple
import os


class _DataSample:
    def __init__(self, data_tuple):
        self.data_tuple = data_tuple

    def __len__(self):
        return len(self.data_tuple)

    def __iter__(self):
        self._n = -1
        return self

    def __next__(self):
        self._n += 1
        if self._n >= len(self):
            return -1, -1
        return self.data_tuple[self._n][0], self.data_tuple[self._n][1]


class DataGenerator(tf.keras.utils.Sequence, DataGeneratorBase):
    def __init__(
        self,
        smp_path_dict: dict,
        batch_size: int = 128,
        shuffle: bool = True,
        label_format: str = "onehot",
        processor: Callable = lambda x : [x],
        dtype: tf.DType = tf.float64,
        standardize = lambda x : x,
        trim = None,
        cutoff = 0.,
    ) -> None:
        """
        Creates an iterable data generator which will feed the data
        to the network as needed.

        Parameters
        ----------
        smp_path_dict : dict[str, list]
            path dictionary..
        batch_size : Optional[int], optional
            batch size. The default is 128.
        shuffle : Optional[bool], optional
            should data be shuffled or not. The default is True.
        label_format : Optional[str], optional
            DESCRIPTION. The default is "onehot".
        image_slice : Union[Tuple[int,int], int], optional
            slice the image. If an int given, each image will be sliced in x and y axis
            symmetrically. If Tuple[int,int] given each axis will be sliced independently.
        image_processor : Callable, optional
            the function to process imate.
            The default is [cos(x pi/2), sin(x pi/2)].
        downsample : bool, optional
            downsample the image by averaging a square of 4 pixels.
        pad_downsample : bool, optional
            pad image with zeros before downsampling.
        unravel_mode: str
            how to unravel the image reshape applies simple numpy reshape function to the image
            and organizes it as a line, "s-shaped" makes an s-shaped tracing over pixels.
        """

        super().__init__(batch_size = batch_size, shuffle = shuffle, dtype = dtype)

        if label_format not in ("onehot", "binary"):
            raise InvalidInput(" Invalid label format!")

        self.label_format = label_format
        self.phi = processor
        (
            self.sample_size,
            self.signal_size,
            self.bkg_size,
            self.samples,
            self.image_paths,
            self.n_pixels,
        ) = self._initialize_samples(smp_path_dict)
        self.standardize = standardize

        self.trim = None
        if trim is not None:
            if len(trim) < 4 :
                raise InvalidInput("trim can not have less than 4 elements")
            self.trim = [int(x) for x in trim]
            self.n_pixels = sum(self.trim)

        self.cutoff = cutoff

        self.init = True

    def Reset(self):
        del self.datasample
        self.datasample = []
        self.init = True
        self.nfile = 0

    @classmethod
    def testing(cls, smp_path_dict: dict,
                batch_size: Optional[int] = 128):
        return cls(smp_path_dict, batch_size, False, "onehot")

    def __len__(self):
        'Denotes the number of batches per epoch'
        return self.sample_size//self.batch_size

    def __iter__(self):
        self.on_epoch_end()
        self._ix = -1
        return self

    def __next__(self):
        self._ix += 1
        if self._ix == len(self):
            self.Reset()
            raise StopIteration
        return self[self._ix]

    def __getitem__(self, index: int):
        assert index < self.size
        return self.__data_generation()

    @staticmethod
    def _initialize_samples(image_paths: dict):
        sort = lambda x : int(x.split('_')[-1].split('.')[0])

        if not all([(x in ['sig','bkg']) for x in image_paths.keys()]):
            raise InvalidInput("Dictionary of image paths needs to "
                               "include both signal and background.")

        image_paths['bkg'].sort(key=sort)
        image_paths['sig'].sort(key=sort)

        sample_size = 0
        signal_size = 0
        bkg_size    = 0
        file_set = []
        for fl_sig, fl_bkg in zip(image_paths['sig'],image_paths['bkg']):
            if not all([os.path.isfile(x) for x in [fl_sig, fl_bkg]]):
                continue

            sig_shape = np.load(fl_sig)['shape']
            bkg_shape = np.load(fl_bkg)['shape']
            assert (sig_shape[1:] == bkg_shape[1:]).all(), \
                f"Signal and background shapes does not match: " \
                f"{sig_shape[1:]} != {bkg_shape[1:]}"


            nsig = sig_shape[0]
            nbkg = bkg_shape[0]

            sample_size += nsig+nbkg
            signal_size += nsig
            bkg_size    += nbkg

            file_set.append((fl_sig, fl_bkg))

        return sample_size, signal_size, bkg_size, file_set, image_paths, int(sig_shape[1:])

    @property
    def _current_file_set(self):
        return self.samples[self.nfile]


    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.nfile = 0
        if self.shuffle:
            np.random.shuffle(self.samples)
        self.__read_data()
        return self


    @tf.function
    def _process_images(self, images: tf.Tensor):
        return tf.stack(tf.vectorized_map(self.phi, images), 
                        axis=1, name='processed_image')


    def __read_data(self):
        sgim, bkim = self._current_file_set
        signal_image = np.load(sgim)['sample']
        bkg_image    = np.load(bkim)['sample']

        min_size_sig = signal_image.shape[0]
        min_size_bkg = bkg_image.shape[0]

        y = []
        if self.label_format == "binary":
            y.extend([[0]]*min_size_sig)
            y.extend([[1]]*min_size_bkg)
        elif self.label_format == "onehot":
            y.extend([[1,0]]*min_size_sig)
            y.extend([[0,1]]*min_size_bkg)

        data = np.vstack((signal_image[:min_size_sig], bkg_image[:min_size_bkg]))
        if self.trim is not None:
            mass                  = data[:,:2][:,:self.trim[0]]
            leading_branch        = data[:,2:20][:,:self.trim[1]]
            second_leading_branch = data[:,20:30][:,:self.trim[2]]
            third_leading_branch  = data[:,30:40][:,:self.trim[3]]

            mass                  = np.where(mass>self.cutoff, mass, 0.)
            leading_branch        = np.where(leading_branch>self.cutoff, leading_branch, 0.)
            second_leading_branch = np.where(
                second_leading_branch>self.cutoff, second_leading_branch, 0.
            )
            third_leading_branch  = np.where(
                third_leading_branch>self.cutoff, third_leading_branch, 0.
            )

            data = np.hstack(
                (mass, leading_branch, second_leading_branch, third_leading_branch)
            )

        data = self.standardize(data)

        if self.phi is not None:
            data = tf.convert_to_tensor(data, dtype=self.dtype, name="images")
            # labels = tf.convert_to_tensor(y,      dtype=tf.int16,   name="labels")

            data = self._process_images(data).numpy()
        self.ndim = data.shape[1]
        self.n_pixels = data.shape[-1]

        data_tuple = list(zip(data,y))
        if self.shuffle:
            np.random.shuffle(data_tuple)

        self.datasample = iter(_DataSample(data_tuple))

    def __data_generation(self):
        'Generates data containing batch_size samples' 
        Xcov, y = [],[]
        while len(y) < self.batch_size:
            xcov, ytruth = next(self.datasample)
            if type(xcov) is int and type(ytruth) is int:
                self.nfile +=1
                if self.nfile == len(self.samples):
                    self.on_epoch_end()
                else:
                    self.__read_data()
                continue
            elif (xcov.shape != (self.ndim, self.n_pixels) and self.phi is not None) or \
                (xcov.shape != (self.n_pixels,) and self.phi is None):
                continue
            else:
                if self.phi is None:
                    Xcov.append(xcov.reshape(-1,1))
                else:
                    Xcov.append(xcov)
                y.append(ytruth)

        X = np.stack(Xcov, axis=0)
        Y = np.stack(y,    axis=0)

        if self.phi is not None:
            return (
                tf.convert_to_tensor(X, dtype=self.dtype, name="images"),
                tf.convert_to_tensor(Y, dtype=tf.int16,   name="labels"),
            )
        else:
            return X, Y
