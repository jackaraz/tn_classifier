class InvalidInput(Exception):
    def __init__(self,message = ""):
        message = "Invalid input!" if message == "" else message
        super().__init__(message)


class InvalidSingularValues(Exception):
    def __init__(self, message = ""):
        message = "Invalid Singular Values!" if message == "" else message
        super().__init__(message)


class InvalidExecution(Exception):
    def __init__(self, message = ""):
        message = "Invalid Execution!" if message == "" else message
        super().__init__(message)


class InvalidSingularValueDecomposition(Exception):
    def __init__(self, message = ""):
        message = "Invalid Singular Value Decomposition!" if message == "" else message
        super().__init__(message)

class InvalidFileFormat(Exception):
    def __init__(self, message = ""):
        if message == "":
            message = "Invalid File Format!"
        super().__init__(message)

class InvalidPath(Exception):
    def __init__(self,message):
        super(InvalidPath, self).__init__("Invalid Path! "+message)


class InvalidInitiation(Exception):
    def __init__(self,message = ""):
        message = "Invalid Initiation!" if message == "" else message
        super().__init__(message)
