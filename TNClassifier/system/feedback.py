"""
Created on Tue Mar 30 09:40:43 2021

@author : jackaraz
@contact: Jack Y. Araz <jackaraz@gmail.com>
"""

import sys, time


def progress(count, total, status='Complete',start_time=-1, bar_len=30, id="Progress"):
    bar_len = bar_len
    if total == 0: total = 1.
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '█' * filled_len + '-' * (bar_len - filled_len)
    if start_time > 0.:
        status += f" Elapsed: {get_elapsed_time(start_time)}"
    status = status.ljust(len(status)+5)

    sys.stdout.write(id+': |%s| \x1b[32m%s%s\x1b[0m %s\r' % \
                                                (bar, percents, '%', status.ljust(50)))
    sys.stdout.flush()


def progress_info(message='Running...\r'):
    sys.stdout.write(message.ljust(len(message)+5))
    sys.stdout.flush()


def get_elapsed_time(start_time):
    elapsed_time = time.time() - start_time
    days = 0
    if elapsed_time >= 86400.:
        days = int(elapsed_time / 86400.)
    elapsed_time = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
    if elapsed_time.split(":")[1] == "00":
        return (days>0)*(f"{days} day(s) ")+elapsed_time.split(":")[2]+" sec."
    elif elapsed_time.split(":")[0] == "00":
        return (days>0)*(f"{days} day(s) ")+":".join(elapsed_time.split(":")[1:])
    else:
        return (days>0)*(f"{days} day(s) ")+elapsed_time