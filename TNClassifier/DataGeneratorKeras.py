import numpy as np
import tensorflow as tf
import os

from TNClassifier.system.exceptions     import InvalidInput
from TNClassifier.MPS.DataGeneratorBase import DataGeneratorBase

from typing import Optional, Union, Callable, Tuple, Text


class _DataSample:
    def __init__(self, data_tuple):
        self.data_tuple = data_tuple

    def __len__(self):
        return len(self.data_tuple)

    def __iter__(self):
        self._n = -1
        return self

    def __next__(self):
        self._n += 1
        if self._n >= len(self):
            return -1, -1
        return self.data_tuple[self._n][0], self.data_tuple[self._n][1]


_phi = lambda im: [tf.math.cos(im * np.pi * 0.5), tf.math.sin(im * np.pi * 0.5)]


class DataGenerator(tf.keras.utils.Sequence, DataGeneratorBase):
    def __init__(
            self,
            smp_path_dict: dict,
            batch_size: int = 128,
            shuffle: bool = True,
            label_format: Text = "onehot",
            image_slice: Union[Tuple[int,int], int] = None,
            image_processor: Callable = _phi,
            downsample: bool = False,
            pad_downsample: bool = False,
            flatten: bool = True,
            dtype: tf.DType = tf.float64,
    ) -> None:
        """
        Creates an iterable data generator which will feed the data
        to the network as needed.

        Parameters
        ----------
        smp_path_dict : dict[Text, list]
            path dictionary..
        batch_size : Optional[int], optional
            batch size. The default is 128.
        shuffle : Optional[bool], optional
            should data be shuffled or not. The default is True.
        label_format : Optional[Text], optional
            DESCRIPTION. The default is "onehot".
        image_slice : Union[Tuple[int,int], int], optional
            slice the image. If an int given, each image will be sliced in x and y axis
            symmetrically. If Tuple[int,int] given each axis will be sliced independently.
        image_processor : Callable, optional
            the function to process imate.
            The default is [cos(x pi/2), sin(x pi/2)].
        """

        self.batch_size = batch_size
        self.shuffle = shuffle
        self.dtype = dtype

        if isinstance(image_slice, int):
            self.slice = [image_slice]
        elif isinstance(image_slice, (tuple, list)):
            self.slice = image_slice
        else:
            self.slice = None

        if label_format not in ("onehot", "binary"):
            raise InvalidInput(" Invalid label format!")

        self.label_format = label_format
        self.phi = image_processor
        (
            self.sample_size,
            self.signal_size,
            self.bkg_size,
            self.samples,
            self.image_paths,
            self.n_pixels,
        ) = self._initialize_samples(smp_path_dict, self.slice, downsample, pad_downsample, flatten)

        self.downsample, self.pad_downssample = downsample, pad_downsample
        self.flatten = flatten

        self.init = True
        self.datasample = []

    def Reset(self):
        del self.datasample
        self.datasample = []

    @classmethod
    def testing(cls, smp_path_dict: dict,
                batch_size: Optional[int] = 128):
        return cls(smp_path_dict, batch_size, False, "onehot")

    def __len__(self):
        'Denotes the number of batches per epoch'
        return self.sample_size//self.batch_size

    def __iter__(self):
        self.on_epoch_end()
        self._ix = -1
        return self

    def __next__(self):
        self._ix += 1
        if self._ix == len(self):
            self.Reset()
            raise StopIteration
        return self[self._ix]

    def __getitem__(self, index: int):
        return self.__data_generation()

    @property
    def size(self):
        return len(self)


    @staticmethod
    def _standardize(image : Union[np.ndarray, tf.Tensor]):
        return image/650.


    @staticmethod
    def _initialize_samples(
            image_paths: dict,
            image_slice: Tuple[int],
            downsample: bool,
            pad_downsample: bool,
            flatten: bool,
    ):
        sort = lambda x : int(x.split('_')[-1].split('.')[0])

        if not all([(x in ['sig','bkg']) for x in image_paths.keys()]):
            raise InvalidInput("Dictionary of image paths needs to "
                               "include both signal and background.")

        image_paths['bkg'].sort(key=sort)
        image_paths['sig'].sort(key=sort)

        sample_size = 0
        signal_size = 0
        bkg_size    = 0
        file_set = []
        for fl_sig, fl_bkg in zip(image_paths['sig'],image_paths['bkg']):
            if not all([os.path.isfile(x) for x in [fl_sig, fl_bkg]]):
                continue

            sig_shape = np.load(fl_sig)['shape']
            bkg_shape = np.load(fl_bkg)['shape']
            assert (sig_shape[1:] == bkg_shape[1:]).all(), f"Signal and background shapes does not match: " \
                                                           f"{sig_shape[1:]} != {bkg_shape[1:]}"

            if len(image_slice) == 1:
                image_shape = (
                    np.prod(
                        [sig_shape[1] - image_slice[0]*2, sig_shape[2] - image_slice[0]*2]
                    )
                    if flatten else [sig_shape[1] - image_slice[0]*2, sig_shape[2] - image_slice[0]*2, 1]
                )
                if downsample:
                    imx, imy = np.ones((sig_shape[1] - image_slice[0]*2,
                                        sig_shape[2] - image_slice[0]*2))[1:-1:2, 1:-1:2].shape
                    if pad_downsample:
                        imx += 1; imy += 1
                    image_shape = np.prod([imx, imy]) if flatten else [imx, imy, 1]
            elif len(image_slice) == 2:
                image_shape = (
                    np.prod(
                        [sig_shape[1] - image_slice[0]*2, sig_shape[2] - image_slice[1]*2]
                    )
                    if flatten else [sig_shape[1] - image_slice[0]*2, sig_shape[2] - image_slice[1]*2, 1]
                )
            else:
                image_shape = np.prod([imx, imy]) if flatten else [imx, imy, 1]


            nsig = sig_shape[0]
            nbkg = bkg_shape[0]

            sample_size += nsig+nbkg
            signal_size += nsig
            bkg_size    += nbkg

            file_set.append((fl_sig, fl_bkg))

        return sample_size, signal_size, bkg_size, file_set, image_paths, image_shape

    @property
    def _current_file_set(self):
        return self.samples[self.nfile]


    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.nfile = 0
        if self.shuffle:
            np.random.shuffle(self.samples)
        self.__read_data()
        return self


    @tf.function
    def _process_images(self, images: tf.Tensor):
        return tf.stack(tf.vectorized_map(self.phi, images),
                        axis=1, name='processed_image')


    def __read_data(self):
        sgim, bkim = self._current_file_set
        signal_image = np.load(sgim)['sample']
        bkg_image    = np.load(bkim)['sample']

        min_size_sig = signal_image.shape[0]
        min_size_bkg = bkg_image.shape[0]

        labels = []
        if self.label_format == "binary":
            labels.extend([[0]]*min_size_sig)
            labels.extend([[1]]*min_size_bkg)
        elif self.label_format == "onehot":
            labels.extend([[1,0]]*min_size_sig)
            labels.extend([[0,1]]*min_size_bkg)

        images = np.vstack((signal_image[:min_size_sig], bkg_image[:min_size_bkg]))
        images = self._standardize(images)
        if self.slice is not None:
            if len(self.slice) == 1:
                images = images[:,
                         slice(self.slice[0], images.shape[1]-self.slice[0]),
                         slice(self.slice[0], images.shape[2]-self.slice[0])
                         ]
            else:
                images = images[:,
                         slice(self.slice[0], images.shape[1]-self.slice[0]),
                         slice(self.slice[1], images.shape[2]-self.slice[1])
                         ]

        if self.downsample:
            images = self._downsample(images, self.pad_downssample)

        if self.flatten:
            images = images.reshape(images.shape[0],images.shape[1]*images.shape[2])

            images = tf.convert_to_tensor(images, dtype=self.dtype, name="images")
            labels = tf.convert_to_tensor(labels, dtype=tf.int16,   name="labels")

            images = self._process_images(images).numpy()
        else:
            images = images.reshape(images.shape[0],images.shape[1],images.shape[2],1)

        data_tuple = list(zip(images,labels))
        if self.shuffle:
            np.random.shuffle(data_tuple)

        self.datasample = iter(_DataSample(data_tuple))

    def __data_generation(self):
        'Generates data containing batch_size samples'
        Xcov, y = [],[]
        while len(y) < self.batch_size:
            xcov, ytruth = next(self.datasample)
            if type(xcov) is int and type(ytruth) is int:
                self.nfile +=1
                if self.nfile == len(self.samples):
                    self.on_epoch_end()
                else:
                    self.__read_data()
                continue
            elif (self.flatten and xcov.shape != (2, self.n_pixels)) or np.all(xcov.shape == self.n_pixels):
                continue
            else:
                Xcov.append(xcov)
                y.append(ytruth)

        return np.array(Xcov), np.array(y)
