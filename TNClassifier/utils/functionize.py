import tensorflow    as tf
import tensornetwork as tn
import numpy         as np
from typing import List, Union, Tuple

from TNClassifier.system.exceptions import InvalidInput



tf.get_logger().setLevel("ERROR")
tn.set_default_backend("tensorflow")


class Optimizer:
    def __init__(self, optimizer: str):
        self.optimizer = tf.optimizers.get(optimizer)

    @tf.function
    def __call__(self, gradient: tf.Tensor, tensor: tf.Tensor) -> tf.Tensor:
        self.optimizer.apply_gradients(zip([gradient], [tensor]))
        return tensor


@tf.function
def update(optimizer: tf.optimizers, gradient: tf.Tensor, tensor: tf.Tensor) -> tf.Tensor:
    optimizer.apply_gradients(zip([gradient], [tensor]))
    return tensor


@tf.function
def move_center(from_tensor: tf.Tensor, to_tensor: tf.Tensor, direction: Union[int, str]):
    if direction in ("right", "r", 1):
        if len(from_tensor.shape) == 3 and len(to_tensor.shape) == 3:
            node = tn.Node(tn.ncon([from_tensor,from_tensor], [[-1,-2,1],[1,-3,-4]]))
            q, r = tn.split_node_qr(node, [node[0],node[1]], [node[2],[node[3]]])
            r.tensor /= tn.norm(r.tensor)
            return q.tensor, r.tensor

        elif len(from_tensor.shape) == 2 and len(to_tensor.shape) == 3:
            node = tn.Node(tn.ncon([from_tensor,from_tensor], [[-1,1],[1,-2,-3]]))
            q, r = tn.split_node_qr(node, [node[0],node[1]], [node[2],[node[3]]])