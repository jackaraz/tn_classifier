import numpy      as np


class GradualIncrease:
    def __init__(
            self,
            factor: float = 2.,
            patience: int = 1,
            verbose: bool = True,
            max_bond: int = 10,
            cooldown=0,
    ):

        if factor <= 1.:
            raise ValueError(f"Factor should be greater than 1. {factor} is given.")

        self.factor = float(factor)
        self.patience = patience
        self.verbose = verbose
        self.max_bond = max_bond
        self.cooldown_counter = 0
        self.wait = 0
        self.cooldown = cooldown

    def __call__(self, training_suite):
        if "num_params" not in training_suite.history.keys():
            training_suite.history["num_params"] = []
        training_suite.history['num_params'].append(training_suite.network.num_params)

        if self.in_cooldown():
            self.cooldown_counter -= 1
            self.wait = 0
        else:
            self.wait += 1
            if self.wait >= self.patience:
                old_bond = training_suite.network.max_singular_values
                if old_bond < self.max_bond:
                    training_suite.network.max_singular_values = min(
                        int(np.ceil(old_bond * self.factor)), self.max_bond
                    )
                    print('\nEpoch %05d: GradualIncrease increasing the  auxiliary dimension '
                          ' to %s.' % (training_suite._epoch + 1, training_suite.network.max_singular_values))
                    self.cooldown_counter = self.cooldown
                    self.wait = 0

    def in_cooldown(self):
        return self.cooldown_counter > 0