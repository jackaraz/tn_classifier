import tensorflow    as tf
import tensornetwork as tn
import numpy         as np

class ModelCheckpoint:
    def __init__(
            self,
            filepath,
            monitor='val_loss',
            verbose=1,
            save_best_only=True,
    ):

        self.monitor = monitor
        self.verbose = verbose
        self.filepath = filepath
        self.save_best_only = save_best_only

        self.monitor_op = np.less
        self.best = np.Inf

    def __call__(self, training_suite):
        if self.save_best_only:
            current = training_suite.history.get(self.monitor)[-1]
            if current is None:
                print(f"{self.monitor} does not exist in training history!")
            elif self.monitor_op(current, self.best):
                if self.verbose > 0:
                    print(f"\nEpoch {training_suite._epoch + 1:05d}: {self.monitor} improved "
                          f"from {self.best:0.5f} to {current:0.5f}, "
                          f"saving model to {self.filepath}")
                self.best = current
                training_suite.network.save_mps(self.filepath)
        else:
            if self.verbose > 0:
                print(f"Epoch {training_suite._epoch + 1:05d}: saving model to {self.filepath}")
            self.best = current
            training_suite.network.save_mps(self.filepath)


class EarlyStopping:
    def __init__(
            self,
            monitor='val_loss',
            min_delta=0,
            patience=0,
            verbose=1,
            restore_best_weights=False
    ):
        self.monitor = monitor
        self.patience = patience
        self.verbose = verbose
        self.min_delta = abs(min_delta)
        self.wait = 0
        self.stopped_epoch = 0
        self.restore_best_weights = restore_best_weights
        self.best_weights = None
        self.best = np.Inf

        self.monitor_op = np.less
        self.min_delta *= -1


    def __call__(self, training_suite):
        current = training_suite.history.get(self.monitor)[-1]
        train_loss = training_suite.history.get("train_loss")[-1]
        if current is None:
            return False
        if self.monitor_op(current - self.min_delta, self.best) and train_loss - current > -1e-3:
            self.best = current
            self.wait = 0
            if self.restore_best_weights:
                self.best_weights = training_suite.network._tensor
        else:
            self.wait += 1
            if self.wait >= self.patience:
                self.stopped_epoch = training_suite._epoch
                print("Stopping the training.")
                if self.restore_best_weights:
                    if self.verbose > 0:
                        print('Restoring model weights from the end of the best epoch.')
                    training_suite.network._tensor = self.best_weights
                return True