import numpy      as np
import tensorflow as tf


class ReduceLRonPlateau:
    """
    adapted from
    https://github.com/tensorflow/tensorflow/blob/85c8b2a817f95a3e979ecd1ed95bff1dc1335cff/tensorflow/python/keras/callbacks.py#L2434-L2554

    mode is set to min
    """
    def __init__(
            self,
            factor: float = 0.1,
            patience: int = 10,
            verbose: bool = True,
            min_delta: float = 1e-4,
            cooldown: int = 0,
            min_lr: float = 1e-10,
    ):

        if factor >= 1.0:
            raise ValueError('ReduceLROnPlateau ' 'does not support a factor >= 1.0.')

        self.factor = factor
        self.min_lr = min_lr
        self.min_delta = min_delta
        self.patience = patience
        self.verbose = verbose
        self.cooldown = cooldown
        self.cooldown_counter = 0  # Cooldown counter.
        self.wait = 0
        self.best = 0
        self.monitor_op = None
        self._reset()

    def _reset(self):
        """Resets wait counter and cooldown counter.
        """
        self.monitor_op = lambda a, b: np.less(a, b - float(self.min_delta))
        self.best = np.Inf
        self.cooldown_counter = 0
        self.wait = 0

    def __call__(self, training_suite):
        logs = training_suite.history
        if "lr" not in logs.keys():
            logs["lr"] = []
        if isinstance(training_suite.optimizer, tf.optimizers.Optimizer):
            logs['lr'].append(float(training_suite.optimizer.lr.numpy()))
        else:
            logs['lr'].append(float(training_suite.optimizer))
        current = logs.get("val_loss")[-1]
        if current is None:
            print('Learning rate reduction is conditioned on metric `%s` '
                    'which is not available. Available metrics are: %s',
                    "val_loss", ','.join(list(logs.keys())))

        else:
            if self.in_cooldown():
                self.cooldown_counter -= 1
                self.wait = 0

            if self.monitor_op(current, self.best):
                self.best = current
                self.wait = 0
            elif not self.in_cooldown():
                self.wait += 1
                if self.wait >= self.patience:
                    old_lr = (
                        float(training_suite.optimizer.lr.numpy())
                        if isinstance(training_suite.optimizer, tf.optimizers.Optimizer)
                        else float(training_suite.optimizer)
                    )
                    if old_lr > self.min_lr:
                        new_lr = old_lr * self.factor
                        new_lr = max(new_lr, self.min_lr)
                        if isinstance(training_suite.optimizer, tf.optimizers.Optimizer):
                            training_suite.optimizer.lr = new_lr
                        else:
                            training_suite.optimizer = new_lr
                        if self.verbose > 0 and \
                                isinstance(training_suite.optimizer, tf.optimizers.Optimizer):
                            print('\nEpoch %05d: ReduceLROnPlateau reducing learning '
                                  'rate to %s.' % (training_suite._epoch + 1, training_suite.optimizer.lr.numpy()))
                        elif self.verbose > 0:
                            print('\nEpoch %05d: ReduceLROnPlateau reducing learning '
                                  'rate to %s.' % (training_suite._epoch + 1, training_suite.optimizer))
                        self.cooldown_counter = self.cooldown
                        self.wait = 0

    def in_cooldown(self):
        return self.cooldown_counter > 0