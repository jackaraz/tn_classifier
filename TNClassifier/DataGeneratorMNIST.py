import numpy      as np
import tensorflow as tf

from enum import Enum, auto

from TNClassifier.system.exceptions     import InvalidInput, InvalidInitiation
from TNClassifier.MPS.DataGeneratorBase import DataGeneratorBase

from typing                             import Optional, Union, Callable, Tuple
import os


class DataGeneratorMNIST(DataGeneratorBase):

    class Mode(Enum):
        UNKNOWN = auto()
        TRAINING = auto()
        VALIDATION = auto()

    def __init__(
            self,
            batch_size: int = 10000,
            shuffle: bool = True,
            image_slice: int = 0,
            image_processor: Callable = lambda im: [tf.math.cos(im * np.pi * 0.5 / 255.),
                                                    tf.math.sin(im * np.pi * 0.5 / 255.)],
            downsample: bool = False,
            pad_downsample: bool = False,
            dtype: tf.DType = tf.float64,
    ) -> None:

        self.mnist = tf.keras.datasets.fashion_mnist

        super().__init__(batch_size = batch_size, shuffle = shuffle, dtype = dtype)

        assert image_slice >= 0 or image_slice < 28, f"Image slice can not be greater " \
                                                     f"than 28 pixels: {image_slice}"

        self.slice = image_slice

        self.phi = image_processor
        self.sample_size = -1

        self.downsample, self.pad_downssample = downsample, pad_downsample

        image_shape = np.prod([28 - image_slice*2, 28 - image_slice*2])
        if downsample:
            imx, imy = np.zeros((28 - image_slice*2, 28 - image_slice*2))[1:-1:2, 1:-1:2].shape
            if pad_downsample:
                imx += 1; imy += 1
            image_shape = np.prod([imx, imy])
        self.n_pixels = image_shape
        self._type = self.Mode.UNKNOWN
        self._batch_index = -1


    @classmethod
    def train(
            cls,
            batch_size: int = 10000,
            shuffle: bool = True,
            image_slice: int = None,
            image_processor: Callable = lambda im: [tf.math.cos(im * np.pi * 0.5 / 255.),
                                                    tf.math.sin(im * np.pi * 0.5 / 255.)],
            downsample: bool = False,
            pad_downsample: bool = False,
            dtype: tf.DType = tf.float64,
    ):
        generator = cls(
            batch_size = batch_size,
            shuffle = shuffle,
            image_slice = image_slice,
            image_processor = image_processor,
            downsample = downsample,
            pad_downsample = pad_downsample,
            dtype = dtype
        )
        generator.sample_size = 60000
        generator._type = generator.Mode.TRAINING
        return generator

    @classmethod
    def validation(
            cls,
            batch_size: int = 10000,
            shuffle: bool = True,
            image_slice: int = None,
            image_processor: Callable = lambda im: [tf.math.cos(im * np.pi * 0.5 / 255.),
                                                    tf.math.sin(im * np.pi * 0.5 / 255.)],
            downsample: bool = False,
            pad_downsample: bool = False,
            dtype: tf.DType = tf.float64,
    ):
        generator = cls(
            batch_size = batch_size,
            shuffle = shuffle,
            image_slice = image_slice,
            image_processor = image_processor,
            downsample = downsample,
            pad_downsample = pad_downsample,
            dtype = dtype
        )
        generator.sample_size = 10000
        generator._type = generator.Mode.VALIDATION
        return generator

    def __len__(self):
        'Denotes the number of batches per epoch'
        return self.sample_size//self.batch_size

    def __iter__(self):
        self.on_epoch_end()
        self._batch_index = -1
        return self

    def __next__(self):
        self._batch_index += 1
        if self._batch_index == len(self):
            raise StopIteration
        return self[self._batch_index]

    def __getitem__(self, index: int):
        assert index < self.size, "Index out of range"
        x, y = self.samples
        return (
            x[index*self.batch_size:(index+1)*self.batch_size],
            y[index*self.batch_size:(index+1)*self.batch_size],
        )

    @property
    def sample_type(self):
        if self._type not in (self.Mode.TRAINING, self.Mode.VALIDATION, self.Mode.UNKNOWN):
            raise InvalidInitiation("Invalid type, please use training or validation "
                                    "class methods")
        return self._type

    def Reset(self):
        if hasattr(self, "samples"):
            del self.samples
        self.samples = ()
        self._batch_index = -1


    @tf.function
    def _process_images(self, images: tf.Tensor):
        return tf.stack(tf.vectorized_map(self.phi, images),
                        axis=1, name='processed_image')


    def on_epoch_end(self):
        self.Reset()
        if self.sample_type == self.Mode.TRAINING:
            (images, labels), (_,_) = self.mnist.load_data()
        elif self.sample_type == self.Mode.VALIDATION:
            (_,_), (images, labels) = self.mnist.load_data()
        elif self.sample_type == self.Mode.UNKNOWN:
            raise InvalidInitiation("Unknown sample type, please use training or validation "
                                    "class methods")

        images = images[:,
                 slice(self.slice, images.shape[1]-self.slice),
                 slice(self.slice, images.shape[2]-self.slice)
                 ]

        if self.downsample:
            images = self._downsample(images, self.pad_downssample)
            self.n_pixels = np.prod(images.shape[1:])

        images = images.reshape(images.shape[0],images.shape[1]*images.shape[2])

        images = self._process_images(
            tf.convert_to_tensor(images, dtype=self.dtype, name="images")
        )
        labels = tf.one_hot(labels, 10, dtype=tf.int16)

        self.samples = list(zip(images, labels))

        if self.shuffle:
            np.random.shuffle(self.samples)

        X, Y = [], []
        for ix, (x, y) in enumerate(self.samples):
            X.append(x); Y.append(y)
        X = tf.stack(X, axis=0); Y = tf.stack(Y, axis=0)
        self.samples = (X,Y)


