import tensorflow    as tf
import tensornetwork as tn
import numpy         as np

tf.get_logger().setLevel("ERROR")
tn.set_default_backend("tensorflow")

from TNClassifier.MPS.MPSBase               import MPSBase
from TNClassifier.tensortools.misc          import ncon, normalize, transpose, is_nan
from TNClassifier.system.feedback           import progress_info, get_elapsed_time, progress
from TNClassifier.tensortools.decomposition import split_node_full_svd, split_node_full_svd_numpy
from TNClassifier.system.exceptions         import (InvalidInput, InvalidExecution,
                                                    InvalidSingularValueDecomposition)

from functools                              import partial
from typing                                 import List, Any, Union, Tuple, Text, Sequence

import time

Tensor = Any


class MPS(MPSBase):
    def __init__(
            self,
            tensors: List[Tensor],
            label_position: int,
            max_truncation_err: float = None,
            max_singular_values: int = None,
            trimming_threshold: float = 0.0,
            dropout: float = 0.0,
            compressed_sites: List[int] = None,
        ) -> None:

        self._left_env  = {}
        self._right_env = {}
        self._debug_count = {}
        self.activation = None
        self.normalized_gradient = True
        self.compressed_sites = compressed_sites

        super().__init__(
            tensors,
            label_position,
            max_truncation_err,
            max_singular_values,
            trimming_threshold,
            dropout
        )

        if (self.label_tensor.shape[-2] == 1 and self.label_position != len(self) -1) or \
            (self.label_tensor.shape[-1] == 1 and self.label_position == len(self) -1):
            self.accuracy = tf.metrics.Accuracy()
        else:
            # CHECK
            #  there is a problem with binary calculation, for some reason it doesn't
            #  converge.
            self.accuracy = tf.metrics.CategoricalAccuracy()

        self.loss = tf.losses.MeanSquaredError(
            reduction=tf.losses.Reduction.SUM_OVER_BATCH_SIZE
        )
        self._metrics = []

        self.lambda_normB = 0.0

        if compressed_sites is not None:
            assert len(self) == len(self.compressed_sites), "Sample sites does not match with " \
                                                            "compressed network."

    @classmethod
    def random(cls,
               N_sites: int,
               physical_dim: int,
               bond_dim: int,
               n_labels: int = 1,
               label_position: Union[int,Text] = "center",
               initializer: Text = "glorot_uniform",
               dtype: tf.DType = tf.float64,
               max_truncation_err: float = None,
               max_singular_values: int = None,
               trimming_threshold: float = 0.0,
               dropout: float = 0.0,
           ):
        """
        Generate a random matrix product state.

        Parameters
        ----------
        N_sites: int
            number of sites
        physical_dim: int
            number of Hilber-space dimensions
        bond_dim: int
            bond dimensions between tensors
        n_labels: int
            number of labels
        label_position: str or int, optional
            position of the label matrix. Default center
        initializer: str, optional
            random value initializer. Default glorot_normal
        dtype: tf.DType, optional
            data type. default `tf.float64`
        max_truncation_err: float, optional
            maximum SVD truncation error
        max_singular_values: float, optional
            maximum number of singular values to be taken during SVD
        trimming_threshold: float, optional
            Acceptance threshold for the singular values.
            The singular values below this threshold will be truncated. trimming_threshold > 0
        dropout: float, optional
            dropout probability during tensor decomposition
        """

        init = tf.initializers.get(initializer)

        if type(label_position) == str:
            if label_position in ("left", "start", "beginning") :
                label_position = 0
            elif label_position in ("right", "end") :
                label_position = N_sites - 1
            elif label_position in ("middle", "center", "centre"):
                label_position = N_sites//2
            else:
                raise InvalidInput(f"Unknown label position : {label_position}")
        elif type(label_position) ==  int:
            label_position = label_position if label_position < N_sites else N_sites - 1

        tensors = []
        for site in range(N_sites):
            if site == 0:
                shape = [physical_dim] + (site == label_position) * [n_labels] + [bond_dim]
            elif site == N_sites -1:
                shape = [bond_dim, physical_dim] + (site == label_position) * [n_labels]
            else:
                shape = (
                    [bond_dim, physical_dim, bond_dim]
                    if site != label_position else [bond_dim, physical_dim, n_labels, bond_dim]
                )
            tensors.append(init(tuple(shape), dtype=dtype))

        return cls(
            tensors = tensors,
            label_position = label_position,
            max_truncation_err = max_truncation_err,
            max_singular_values = max_singular_values,
            trimming_threshold = trimming_threshold,
            dropout = dropout,
        )

    @classmethod
    def load_random_from_file(cls, filename):
        import json
        with open(filename, "r") as f:
            mps = json.load(f)

        return MPS.random(N_sites = mps["nsites"],
                          physical_dim = mps["phys_dim"],
                          bond_dim = mps["bond_dim"],
                          n_labels = mps["nlabels"],
                          label_position = mps["label_position"],
                          max_truncation_err = mps["max_truncation_err"],
                          max_singular_values = mps["max_singular_values"],
                          trimming_threshold = mps["trimming_threshold"])

    @property
    def metrics(self) -> List:
        return self._metrics

    @metrics.setter
    def metrics(self, val) -> None:
        if isinstance(val, (list, tuple)):
            for metric in val:
                if isinstance(metric, tf.metrics.Metric):
                    self._metrics.append(metric)
        else:
            if isinstance(val, tf.metrics.Metric):
                self._metrics.append(val)
            else:
                raise InvalidInput("Unknown metric.")

    def set_loss(self, val):
        if isinstance(val, str):
            self.loss = tf.losses.get(val)
        elif hasattr(val, "__call__"):
            self.loss = val
        else:
            raise InvalidInput("Unknown loss!")

    @property
    def trainable_variables(self) -> List[tf.Variable]:
        return [tf.Variable(t, trainable = True, dtype=self.dtype) for t in self]

    def reset_env(self) -> None:
        del self._left_env, self._right_env
        self._left_env, self._right_env = {}, {}

    def set_tensors(self, tensors: Sequence[tf.Tensor]) -> None:
        if isinstance(tensors, (tuple, list)):
            for ix, tensor in enumerate(tensors):
                if isinstance(tensor, tf.Variable):
                    self[ix] = tensor.value()
                elif isinstance(tensor, tf.Tensor):
                    self[ix] = tensor
                else:
                    raise InvalidInput("Input is neither a variable nor tensor.")
        else:
            raise InvalidInput("Input has to be a sequence of tensors.")


    @staticmethod
    @tf.function
    def _map_multip(inpt: List[tf.Tensor], order: List[List[int]]) -> tf.Tensor:
        def map_multip(x):
            return tn.ncon([*x], order, backend = "tensorflow", check_network = False)

        return tf.vectorized_map(map_multip, inpt)


    def _compute_right_environments(self, samples: tf.Tensor, site: int) -> Tensor:
        """
        Compute the environments at the site

        Parameters
        ----------
        samples: tf.Tensor
            input samples shape (Nt, phys dim, Nsites)
        site: int
            site to be calculated
        """
        label_check = False
        if site >= self.label_position:
            if site == len(self) - 1:
                # insert ones this will not be used
                self._right_env[site] = tf.ones((samples.shape[0],self[site].shape[0]),
                                                dtype=self.dtype)

            elif site == len(self) - 2:
                # take last site and concatenate it with the sample
                self._right_env[ site ] = ncon([samples[:,:,site+1], self[site+1]],
                                               [[-1,1], [-2,1]])

            else:
                # take site to the right concantenate it with sample
                right_site = ncon([samples[:,:,site+1], self[site+1]],
                                  [[-1,1], [-2,1,-3]])                   # (Nt, l bond, r bond)
                # take the environment to the right which is fully calculated
                renv = self._right_env[site+1]                           # (Nt, l bond)
                self._right_env[site] = self._map_multip([right_site, renv],
                                                         [[-1,1],[1]])   # (Nt, l bond)

        elif site == self.label_position - 1:
            if self.label_position != len(self) - 1:
                right_site = ncon([samples[:, :, site + 1], self.label_tensor],
                                  [[-1, 1], [-2, 1, -3, -4]])            # (Nt, l bond, nlabel, r bond)
                renv = self._right_env[site + 1]                         # (Nt, l bond)
                self._right_env[site] = self._map_multip(
                            [right_site, renv], [[-1, -2, 1], [1]]
                    )                                                    # (Nt, l bond, nlabel)
            else:
                self._right_env[site] = ncon([samples[:, :, site + 1], self.label_tensor],
                                             [[-1, 1], [-2, 1, -3]])     # (Nt, l bond, nlabel)
            label_check = True

        else:
            right_site = ncon([samples[:, :, site + 1], self[site + 1]],
                              [[-1, 1], [-2, 1, -3]])                     # (Nt, l bond, r bond)
            renv = self._right_env[site + 1]                              # (Nt, l bond, nlabel)
            self._right_env[site] = self._map_multip([right_site, renv],
                                                     [[-1,1],[1,-2]])     # (Nt, l bond, nlabel)
            label_check = True

        # normalize the environment otherwise it will go to inf
        # axis = [0] + (label_check)*[-1]
        # self._right_env[site], _ = tf.linalg.normalize(
        #     self._right_env[site], axis = tuple(axis) if len(axis) > 1 else axis[0]
        # )
        self._right_env[site], _ = normalize(self._right_env[site])
        return self._right_env[site]


    def _compute_left_environments(self, samples: tf.Tensor, site: int) -> Tensor:
        """
        Compute the environments at the site

        Parameters
        ----------
        samples: tf.Tensor
            input samples shape (Nt, phys dim, Nsites)
        site: int
            site to be calculated
        """

        label_check = False
        if site <= self.label_position:
            if site == 0:
                self._left_env[site] = tf.ones((samples.shape[0],self[site].shape[1]),
                                               dtype=self.dtype)
            elif site == 1:
                self._left_env[site] = ncon([samples[:,:,site - 1], self[site - 1]],
                                            [[-1,1], [1,-2]])                   # (Nt, r bond)
            else:
                left_site = ncon([samples[:,:,site - 1], self[site - 1]],
                                 [[-1,1], [-2,1,-3]])                           # (Nt, l bond, r bond)
                lenv = self._left_env[site - 1]                                 # (Nt, r bond)
                self._left_env[site] = self._map_multip([lenv, left_site],
                                                        [[1], [1,-1]])          # (Nt, r bond)

        elif site == self.label_position + 1:
            if self.label_position != 0:
                left_site = ncon([samples[:, :, site - 1], self.label_tensor],
                                 [[-1, 1], [-2, 1, -3, -4]])                    # (Nt, l bond, nlabel, r bond)
                lenv = self._left_env[site - 1]                                 # (Nt, r bond)
                self._left_env[site] = self._map_multip([lenv, left_site],
                                                        [[1], [1,-1,-2]])       # (Nt, nlabel, r bond)
            else:
                self._left_env[site] = ncon([samples[:, :, site - 1], self[site - 1]],
                                            [[-1, 1], [1, -2, -3]])             # (Nt, nlabel, r bond)
            label_check = True

        else:
            left_site = ncon([samples[:,:,site-1], self[site-1]],
                             [[-1,1], [-2,1,-3]])                               # (Nt, l bond, r bond)
            lenv = self._left_env[site-1]                                       # (Nt, nlabel, r bond)
            self._left_env[site] = self._map_multip([lenv, left_site],
                                                    [[-1,1], [1,-2]])           # (Nt, nlabel, r bond)
            label_check = True

        # normalize the environment otherwise it will go to inf
        self._left_env[site], _ = normalize(self._left_env[site])
        # axis = [0] + (label_check) * [1]
        # self._left_env[site], _ = tf.linalg.normalize(
        #     self._left_env[site], axis = tuple(axis) if len(axis) > 1 else axis[0]
        # )
        return self._left_env[site]


    def compute_environments(
            self, samples: tf.Tensor, verbose: bool = True, only_keep_site: bool = False
    ) -> None:
        """
        Compute left and right environments for code efficiency

        Parameters
        ----------
        samples: tf.Tensor
            input tensor with shape (Nt, physical dims, nsites)
        verbose : bool, optional
            verbosity
        only_keep_site: bool, optional
            if true only keeps the current centre-of-orthogonality at the memory and rest
            will be removed during calculation. default False
        """

        if self.compressed_sites is not None and samples.shape[-1] != len(self):
            samples = tf.stack([samples[:,:,site] for site in self.compressed_sites], axis=2)

        self.reset_env()
        start_time = time.time()
        if self.center_position > 0:
            for site in range(self.center_position + 1):
                if verbose:
                    progress(site, self.center_position,
                             status="Computing Left Environments",
                             start_time=start_time)
                _ = self._compute_left_environments(samples, site)
                if only_keep_site and site - 1 >= 0:
                    self._left_env.pop(site - 1)

        if self.center_position < self.num_sites:
            for site in reversed(range(self.center_position, self.num_sites)):
                if verbose:
                    progress(self.num_sites-site, self.num_sites - self.center_position,
                             status = "Computing Right Environments",
                             start_time = start_time)
                _ = self._compute_right_environments(samples, site)
                if only_keep_site and site + 1 <= self.num_sites - 1:
                    self._right_env.pop(site + 1)


    def predict(self,
                samples: tf.Tensor,
                compute_environments: bool = True,
                return_norm: bool = False,
                delete_environments: bool = True,
        ) -> Union[tf.Tensor, Tuple[tf.Tensor, tf.Tensor]]:
        """
        Get normalized predictions

        Parameters
        ----------
        samples: tf.Tensor
            input samples
        compute_environments : bool, optional
            compute the left and right environments before the calculation. Default is True
        return_norm: bool, optional
            return norms of the predictions
        delete_environments: bool, optional
            free's the RAM by deleting environments, default True

        Returns
        -------
        tf.Tensor
            returns predictions
        """

        if self.compressed_sites is not None and samples.shape[-1] != len(self):
            samples = tf.stack([samples[:,:,site] for site in self.compressed_sites], axis=2)

        if compute_environments:
            self.compute_environments(samples, verbose = False, only_keep_site = True)

        if self.center_position == 0:
            if self.label_position == 0:
                site_0 = ncon([samples[:,:,0], self[0]],
                              [[-1,1], [1,-2,-3]])                              # (Nt, nlabel, rbond)
                prediction = self._map_multip([site_0, self._right_env[0]], [[-1, 1], [1]])
            else:
                site_0 = ncon([samples[:, :, 0], self[0]], [[-1, 1], [1, -2]])  # (Nt, rbond)
                prediction = self._map_multip([site_0, self._right_env[0]],
                                              [[1], [1,-1]])

        elif self.center_position == len(self) - 1:
            if self.label_position == len(self) - 1:
                site_0 = ncon([samples[:,:,len(self) - 1], self.center_tensor],
                              [[-1,1], [-2,1,-3]])                              # (Nt, lbond, nlabel)
                prediction = self._map_multip([site_0, self._left_env[len(self) - 1]],
                                              [[1,-1],[1]])
            else:
                site_0 = ncon([samples[:,:,len(self) - 1], self.center_tensor],
                              [[-1,1], [-2,1]])                                 # (Nt, lbond)
                prediction = self._map_multip([self._left_env[len(self) - 1], site_0],
                                              [[-1, 1], [1]])

        else:
            if self.label_position == self.center_position:
                site_0 = ncon([samples[:,:,self.center_position], self.center_tensor],
                              [[-1, 1], [-2,1,-3,-4]])                          # (Nt, lbond, nlabel, rbond)
                prediction = self._map_multip(
                    [
                        self._left_env[self.label_position], # (Nt,r bond)
                        site_0,                              # (Nt, l bond, label, r bond)
                        self._right_env[self.label_position] # (Nt, l bond)
                    ],
                    [[1],[1,-1,2],[2]],
                )
            else:
                site_0 = ncon([samples[:,:,self.center_position], self.center_tensor],
                              [[-1,1], [-2,1,-3]])                              # (Nt, lbond, rbond)
                if self.label_position < self.center_position:
                    prediction = self._map_multip(
                        [
                            self._left_env[self.center_position], # (Nt, nlabel, r bond)
                            site_0,                               # (Nt, l bond, r bond)
                            self._right_env[self.center_position] # (Nt, l bond)
                        ],
                        [[-1,1],[1,2],[2]],
                    )
                elif self.label_position > self.center_position:
                    prediction = self._map_multip(
                        [
                            self._left_env[self.center_position], # (Nt,r bond)
                            site_0,                               # (Nt, l bond, r bond)
                            self._right_env[self.center_position] # (Nt, l bond, nlabel)
                        ],
                        [[1],[1,2],[2,-1]],
                    )

        if delete_environments:
            self.reset_env()

        # normalize the environment otherwise it will go to inf
        prediction, norm = normalize(prediction)

        if self.activation is not None:
            prediction = self.activation(prediction)

        if return_norm:
            return prediction, norm

        return prediction


    @staticmethod
    @tf.function
    def _map_multip_one_vs_rest(inpt: List[tf.Tensor], order: List[List[int]]) -> tf.Tensor:
        def map_multip_double(x):
            return tn.ncon([inpt[0], *x], order, backend = "tensorflow", check_network = False)

        return tf.vectorized_map(map_multip_double, inpt[1:])


    def _right_step_gradient(self,
                             samples: tf.Tensor,
                             labels: tf.Tensor,
                             site: int
             ) -> Tuple[tf.Tensor, tf.Tensor, tf.Tensor, tf.Tensor]:
        """
        Calculate the gradient step towards right of the site

        Parameters
        ----------
        samples: tf.Tensor
            input samples shape: (Nt, phy, nsites)
        labels: tf.Tensor
            truth labels of the input shape: (Nt, nlabels)
        site: int
            site to be calculated

        Returns
        -------
        Tuple[tf.Tensor]
            gradient wrt (site, site+1), combined (site, site+1) tensor, loss, predictions
        """

        # At last site turn left and continue sweeping in left direction.
        assert site < len(self) - 1, f"Right gradient at site {site} is not allowed."

        ####################################
        #              DROPOUT             #
        ####################################
        tensor_at_site = self[site]
        if self.dropout > 0.0:
            TensorNode = tn.Node(tensor_at_site)
            connector_dim_at_site = TensorNode.shape[-1]
            connector = tf.linalg.diag(
                tf.random.uniform((connector_dim_at_site,), dtype=self.dtype)
            )
            connectorNode = tn.Node(
                tf.cast(tf.where(connector > self.dropout, 1., 0.), dtype=self.dtype)
            )
            TensorNode[-1] ^ connectorNode[0]
            tensor_at_site = (TensorNode @ connectorNode).tensor

        if self.compressed_sites is not None and samples.shape[-1] != len(self):
            samples = tf.stack([samples[:,:,site] for site in self.compressed_sites], axis=2)

        ####################################
        #             GRADIENT             #
        ####################################
        with tf.GradientTape() as tape:
            tape.watch(samples)
            tape.watch([tensor_at_site, self[site + 1]])

            if site + 1 < self.label_position:
                if site == 0:
                    current_tensor = ncon([tensor_at_site, self[1]],
                                          [[-1, 1], [1, -2, -3]])                   # (phy, phy, r bond)
                    right_env = self._right_env[site + 1]                           # (Nt, l bond, nlabel)
                    phi_n = [samples[:,:,site], samples[:,:,site+1], right_env]
                    # [(Nt, phy), (Nt, phy), (Nt, l bond, nlabel)]
                    yhat = self._map_multip_one_vs_rest([current_tensor, *phi_n],
                                                        [[1,2,3],[1],[2],[3,-1]])   # (Nt, nlabel)

                else:
                    current_tensor = ncon([tensor_at_site, self[site + 1]],
                                          [[-1,-2,1],[1,-3,-4]])                    # (l bond, phy, phy, r bond)
                    right_env = self._right_env[site + 1]                           # (Nt, l bond, nlabel)
                    left_env  = self._left_env[site]                                # (Nt, r bond)
                    phi_n     = [left_env, samples[:,:,site], samples[:,:,site+1], right_env]
                    yhat = self._map_multip_one_vs_rest([current_tensor, *phi_n],
                                                        [[1,2,3,4],[1],[2],[3],[4,-1]])

            elif self.label_position in (site, site+1):
                if site != 0:
                    if site + 1 < len(self) - 1:
                        right_env = self._right_env[site + 1]                   # (Nt, l bond)
                        left_env  = self._left_env[site]                        # (Nt, r bond)
                        phi_n = [left_env, samples[:, :, site], samples[:, :, site + 1], right_env]
                        if site + 1 == self.label_position:
                            current_tensor = ncon([tensor_at_site, self.label_tensor],
                                                  [[-1, -2, 1],[1, -3, -4, -5]])    # (l bond, phy, phy, nlabel, r bond)
                            yhat = self._map_multip_one_vs_rest([current_tensor, *phi_n],
                                                                [[1,2,3,-1,4],[1],[2],[3],[4]])
                        elif site == self.label_position:
                            current_tensor = ncon([tensor_at_site, self[site + 1]],
                                                  [[-1, -2, -3, 1], [1, -4,-5]])    # (l bond, phy, nlabel, phy, r bond)
                            yhat = self._map_multip_one_vs_rest([current_tensor, *phi_n],
                                                                [[1,2,-1,3,4],[1],[2],[3],[4]])
                    else:
                        left_env = self._left_env[site]  # (Nt, r bond)
                        phi_n = [left_env, samples[:, :, site], samples[:, :, site + 1]]
                        if site + 1 == self.label_position:
                            current_tensor = ncon([tensor_at_site, self.label_tensor],
                                                  [[-1, -2, 1], [1, -3, -4]])       # (l bond, phy, phy, nlabel)
                            yhat = self._map_multip_one_vs_rest([current_tensor, *phi_n],
                                                                [[1, 2, 3, -1], [1], [2], [3]])
                        elif site == self.label_position:
                            current_tensor = ncon([tensor_at_site, self[site + 1]],
                                                  [[-1, -2, 1, -3], [1, -4]])       # (l bond, phy, nlabel, phy)
                            yhat = self._map_multip_one_vs_rest([current_tensor, *phi_n],
                                                                [[1, 2, -1, 3], [1], [2], [3]])

                elif site == 0:
                    right_env = self._right_env[site + 1]                           # (Nt, l bond)
                    phi_n = [samples[:, :, site], samples[:, :, site + 1], right_env]
                    if site + 1 == self.label_position:
                        current_tensor = ncon([tensor_at_site, self.label_tensor],
                                              [[-1, 1],[1, -2, -3, -4]])            # (phy, phy, nlabel, r bond)
                        yhat = self._map_multip_one_vs_rest([current_tensor, *phi_n],
                                                            [[1,2,-1,3],[1],[2],[3]])
                    elif site == self.label_position:
                        current_tensor = ncon([tensor_at_site, self[site + 1]],
                                              [[-1, -2, 1], [1, -3,-4]])            # (phy, nlabel, phy, r bond)
                        yhat = self._map_multip_one_vs_rest([current_tensor, *phi_n],
                                                            [[1,-1,2,3],[1],[2],[3]])

            elif site > self.label_position:
                if site + 1 < len(self) - 1:
                    current_tensor = ncon([tensor_at_site, self[site+1]],
                                          [[-1,-2,1],[1,-3,-4]])                    # (l bond, phy, phy, r bond)
                    right_env = self._right_env[site + 1]                           # (Nt, l bond)
                    left_env = self._left_env[site]                                 # (Nt, nlabel, r bond)
                    phi_n = [left_env, samples[:, :, site], samples[:, :, site + 1], right_env]
                    yhat = self._map_multip_one_vs_rest([current_tensor, *phi_n],
                                                        [[1,2,3,4],[-1,1],[2],[3],[4]])
                else:
                    current_tensor = ncon([tensor_at_site, self[site + 1]],
                                          [[-1, -2, 1],[1, -3]])                    # (l bond, phy, phy)
                    left_env = self._left_env[site]                                 # (Nt, nlabel, r bond)
                    phi_n = [left_env, samples[:, :, site], samples[:, :, site + 1]]
                    yhat = self._map_multip_one_vs_rest([current_tensor, *phi_n],
                                                        [[1, 2, 3], [-1, 1], [2], [3]])

            # WARNING
            # yhat is equal to the prediction upto O(1e-14)
            yhat, _ = normalize(yhat)

            if self.activation is not None:
                yhat = self.activation(yhat)

            loss = self.loss(labels, yhat) + self.lambda_normB * tf.norm(current_tensor)**2

        return tape.gradient(loss, current_tensor), current_tensor, loss, yhat


    def _left_step_gradient(self,
                            samples: tf.Tensor,
                            labels: tf.Tensor,
                            site: int
                    ) -> Tuple[tf.Tensor, tf.Tensor, tf.Tensor, tf.Tensor]:
        """
        Calculate the gradient step towards left of the site

        Parameters
        ----------
        samples: tf.Tensor
            input samples shape: (Nt, phy, nsites)
        labels: tf.Tensor
            truth labels of the input shape: (Nt, nlabels)
        site: int
            site to be calculated

        Returns
        -------
        Tuple[tf.Tensor]
            gradient wrt (site, site+1), combined (site, site+1) tensor, loss, predictions
        """

        assert 0 < site < len(self)

        ####################################
        #              DROPOUT             #
        ####################################
        tensor_at_site = self[site]
        if self.dropout > 0.0:
            TensorNode = tn.Node(tensor_at_site)
            connector_dim_at_site = TensorNode.shape[0]
            connector = tf.linalg.diag(
                tf.random.uniform((connector_dim_at_site,), dtype=self.dtype)
            )
            connectorNode = tn.Node(
                tf.cast(tf.where(connector > self.dropout, 1., 0.), dtype=self.dtype)
            )
            connectorNode[1] ^ TensorNode[0]
            tensor_at_site = (connectorNode @ TensorNode).tensor

        if self.compressed_sites is not None and samples.shape[-1] != len(self):
            samples = tf.stack([samples[:,:,site] for site in self.compressed_sites], axis=2)

        ####################################
        #             GRADIENT             #
        ####################################
        with tf.GradientTape() as tape:
            tape.watch(samples)
            tape.watch([tensor_at_site, self[site-1]]) # is this necessary??

            if site - 1 > self.label_position:
                if site == len(self) - 1:
                    current_tensor = ncon([self[site - 1], tensor_at_site],
                                          [[-1, -2, 1], [1, -3]])                       # (l bond, phy, phy)
                    left_env = self._left_env[site - 1]                                 # (Nt, nlabel, r bond)
                    phi_n = [left_env, samples[:, :, site - 1], samples[:, :, site]]
                    yhat = self._map_multip_one_vs_rest([current_tensor, *phi_n],
                                                        [[1, 2, 3], [-1, 1], [2], [3]]) # (Nt, nlabel)

                else:
                    current_tensor = ncon([self[site - 1], tensor_at_site],
                                          [[-1,-2,1], [1,-3,-4]])                       # (l bond, phy, phy, r bond)
                    left_env = self._left_env[site - 1]                                 # (Nt, nlabel, r bond)
                    right_env = self._right_env[site]                                   # (Nt, l bond)
                    phi_n = [left_env, samples[:,:,site-1],samples[:,:,site],right_env]
                    yhat = self._map_multip_one_vs_rest([current_tensor, *phi_n],
                                                        [[1,2,3,4],[-1,1],[2],[3],[4]])

            elif self.label_position in (site, site - 1):
                if site < len(self) - 1:
                    if site - 1 > 0:
                        left_env  = self._left_env[site - 1]                            # (Nt, r bond)
                        right_env = self._right_env[site]                               # (Nt, l bond)
                        phi_n = [left_env, samples[:,:,site - 1], samples[:,:,site], right_env]
                        if site - 1 == self.label_position:
                            current_tensor = ncon(
                                [self.label_tensor, tensor_at_site], [[-1,-2,-3,1],[1,-4,-5]]
                            )                                                           # (l bond, phy, nlabel, phy, r bond)
                            yhat = self._map_multip_one_vs_rest([current_tensor, *phi_n],
                                                                [[1,2,-1,3,4],[1],[2],[3],[4]])
                        elif site == self.label_position:
                            current_tensor = ncon([self[site-1], tensor_at_site],
                                                  [[-1,-2,1],[1,-3,-4,-5]])             # (l bond, phy, phy, nlabel, r bond)
                            yhat = self._map_multip_one_vs_rest([current_tensor, *phi_n],
                                                                [[1,2,3,-1,4],[1],[2],[3],[4]])
                    else:
                        right_env = self._right_env[site]                               # (Nt, lbond)
                        phi_n = [samples[:,:,site-1],samples[:,:,site],right_env]
                        if site - 1 == self.label_position:
                            current_tensor = ncon([self.label_tensor, tensor_at_site],
                                                  [[-1,-2,1],[1,-3,-4]])                # (phy, nlabel, phy, r bond)
                            yhat  = self._map_multip_one_vs_rest([current_tensor, *phi_n],
                                                                 [[1,-1,2,3],[1],[2],[3]])
                        elif site == self.label_position:
                            current_tensor = ncon([self[site-1], tensor_at_site],
                                                  [[-1,1],[1,-2,-3,-4]])                # (phy, phy, nlabel, r bond)
                            yhat  = self._map_multip_one_vs_rest([current_tensor, *phi_n],
                                                                 [[1,2,-1,3],[1],[2],[3]])

                elif site == len(self) - 1:
                    left_env = self._left_env[site - 1] # (nt, rbond)
                    phi_n = [left_env, samples[:,:,site-1],samples[:,:,site]]
                    if site - 1 == self.label_position:
                        current_tensor = ncon([self.label_tensor, tensor_at_site],
                                              [[-1,-2,-3,1],[1,-4]])                    # (l bond, phy, nlabel, phy)
                        yhat = self._map_multip_one_vs_rest([current_tensor, *phi_n],
                                                            [[1,2,-1,3],[1],[2],[3]])
                    elif site == self.label_position:
                        current_tensor = ncon([self[site-1], tensor_at_site],
                                              [[-1,-2,1],[1,-3,-4]])                    # (l bond, phy, phy, nlabel)
                        yhat = self._map_multip_one_vs_rest([current_tensor, *phi_n],
                                                            [[1,2,3,-1],[1],[2],[3]])

            elif site < self.label_position:
                if site - 1 == 0:
                    current_tensor = ncon([self[0], tensor_at_site],
                                          [[-1,1], [1,-2,-3]])                          # (phy, phy, r bond)
                    right_env = self._right_env[site]                                   # (Nt, l bond, nlabel)
                    phi_n = [samples[:,:,0],samples[:,:,1],right_env]
                    yhat = self._map_multip_one_vs_rest([current_tensor, *phi_n],
                                                        [[1,2,3],[1],[2],[3,-1]])
                else:
                    current_tensor = ncon([self[site-1], tensor_at_site],
                                          [[-1,-2,1],[1,-3,-4]])                        # (l bond, phy, phy, r bond)
                    left_env = self._left_env[site-1]                                   # (nt, r bond)
                    right_env = self._right_env[site]                                   # (nt, lbond, nlabel)
                    phi_n = [left_env, samples[:,:,site-1],samples[:,:,site],right_env]
                    yhat = self._map_multip_one_vs_rest([current_tensor, *phi_n],
                                                        [[1,2,3,4],[1],[2],[3],[4,-1]])

            yhat, _ = normalize(yhat)

            if self.activation is not None:
                yhat = self.activation(yhat)

            loss = self.loss(labels, yhat) + self.lambda_normB * tf.norm(current_tensor)**2

        return tape.gradient(loss, current_tensor), current_tensor, loss, yhat


    def _right_step_update(self,
                           samples: tf.Tensor,
                           labels: tf.Tensor,
                           site: int,
                           optimizer: Union[float, tf.optimizers.Optimizer],
                           fixed_label: bool = True) -> tf.Tensor:
        """
        Two site update for site and site+1. Calculates the gradient of the loss function
        with respect to the MPS at site, site+1, updates the local MPS and decomposes it.

        Parameters
        ----------
        samples: tf.Tensor
            input samples
        labels: tf.Tensor
            labels of the input tensor
        site: int
            local MPS site
        optimizer: Union[float, tf.optimizers.Optimizer]
            either learning rate or an optimizer
        fixed_label: bool, optional
            should the label tensor be shifted to the right
        """

        if not fixed_label:
            assert site == self.label_position

        if self.compressed_sites is not None and samples.shape[-1] != len(self):
            samples = tf.stack([samples[:,:,site] for site in self.compressed_sites], axis=2)

        ( gradient,
          current_tensor,
          loss,
          prediction,
        ) = self._right_step_gradient(samples, labels, site)

        # CHECK it seems like normalized gradient gives much more stable results
        gradient = gradient/tf.norm(gradient) if self.normalized_gradient else gradient

        if is_nan(gradient).numpy() :
            self._add_debug_count = "Gradient tensor has NaN values at right step update"
            gradient = tf.where(tf.math.is_nan(gradient), 0.0, gradient)

        # update current tensor
        if isinstance(optimizer, float):
            updated_tensor = current_tensor - optimizer * gradient
        elif isinstance(optimizer, tf.optimizers.Optimizer):
            updated_tensor = tf.Variable(current_tensor, trainable = True, dtype = current_tensor.dtype)
            optimizer.apply_gradients(zip([gradient], [updated_tensor]))
        else:
            raise InvalidInput(f"Unknown optimizer type: {type(optimizer)}")


        updated_is_nan = tf.math.is_nan(updated_tensor).numpy()
        if updated_is_nan.any() :
            self._add_debug_count = "Updated Tensor has NaN values at right step update"
            if updated_is_nan.all():
                self.center_position = site + 1
                if not fixed_label:
                    self.label_position = self.center_position
                return loss
            else:
                updated_tensor = tf.where(tf.math.is_nan(updated_tensor), 0.0, updated_tensor)

        left, right = self[site], self[site + 1]

        try:
            if self.label_position not in (site, site+1):
                if site == 0:
                    # update_node shape (phy, phy, r bond)
                    left, right = split_node_full_svd(
                        updated_tensor, [0], [1, 2], direction = "right",
                        max_singular_values = self.max_singular_values,
                        max_truncation_err = self.max_truncation_err,
                        trimming_threshold = self.trimming_threshold,
                    )

                else:
                    if site + 1 < len(self) - 1:
                        # WARNING !!
                        #  we get NaN values for the tensors sometimes here. This is due
                        # to the tf precision so will try to wrap it up with numpy incase of NaN
                        # values.

                        # update_node shape (l bond, phy, phy, r bond)
                        left, right = split_node_full_svd(
                            updated_tensor, [0, 1], [2, 3], direction = "right",
                            max_singular_values = self.max_singular_values,
                            max_truncation_err = self.max_truncation_err,
                            trimming_threshold = self.trimming_threshold,
                        )
                        if is_nan(left) or is_nan(right):
                            self._add_debug_count = "TF-SVD failed in right step update"
                            # WARNING this will slow down the calculation
                            np_svd = partial(
                                split_node_full_svd_numpy,
                                left_edges = [0, 1],
                                right_edges = [2, 3],
                                direction = "right",
                                max_singular_values = self.max_singular_values,
                                max_truncation_err = self.max_truncation_err,
                                trimming_threshold = self.trimming_threshold,
                            )
                            left, right = tf.py_function(
                                np_svd, inp=[updated_tensor], Tout=[self.dtype]*2
                            )

            elif self.label_position in (site, site+1):
                if fixed_label:
                    if site == 0:
                        if site + 1 == self.label_position:
                            # update_node shape (phy, phy, nlabel, r bond)
                            left, right = split_node_full_svd(
                                updated_tensor, [0], [1, 2, 3], direction = "right",
                                max_singular_values = self.max_singular_values,
                                max_truncation_err = self.max_truncation_err,
                                trimming_threshold = self.trimming_threshold,
                            )
                        elif site == self.label_position:
                            # update_node shape (phy, nlabel, phy, r bond)
                            left, right = split_node_full_svd(
                                updated_tensor, [0, 1], [2, 3], direction = "right",
                                max_singular_values = self.max_singular_values,
                                max_truncation_err = self.max_truncation_err,
                                trimming_threshold = self.trimming_threshold,
                            )

                    else:
                        if site + 1 < len(self) - 1:
                            if site + 1 == self.label_position:
                                # update_node shape (l bond, phy, phy, nlabel, r bond)
                                left, right = split_node_full_svd(
                                    updated_tensor, [0, 1], [2, 3, 4], direction = "right",
                                    max_singular_values = self.max_singular_values,
                                    max_truncation_err = self.max_truncation_err,
                                    trimming_threshold = self.trimming_threshold,
                                )
                            elif site == self.label_position:
                                # update_node shape (l bond, phy, nlabel, phy, r bond)
                                left, right = split_node_full_svd(
                                    updated_tensor, [0, 1, 2], [3, 4], direction = "right",
                                    max_singular_values = self.max_singular_values,
                                    max_truncation_err = self.max_truncation_err,
                                    trimming_threshold = self.trimming_threshold,
                                )
                        else:
                            if site + 1 == self.label_position:
                                # update_node shape (l bond, phy, phy, nlabel)
                                left, right = split_node_full_svd(
                                    updated_tensor, [0, 1], [2, 3], direction = "right",
                                    max_singular_values = self.max_singular_values,
                                    max_truncation_err = self.max_truncation_err,
                                    trimming_threshold = self.trimming_threshold,
                                )
                            elif site == self.label_position:
                                # update_node shape (l bond, phy, nlabel, phy)
                                left, right = split_node_full_svd(
                                    updated_tensor, [0, 1, 2], [3], direction = "right",
                                    max_singular_values = self.max_singular_values,
                                    max_truncation_err = self.max_truncation_err,
                                    trimming_threshold = self.trimming_threshold,
                                )

                else:
                    raise NotImplementedError("This module is currently under construction.")
                    if site not in (len(self)-2, 0):
                        left, right = split_node_full_svd(
                            updated_tensor, [0, 1], [2, 3, 4], direction = "right",
                            max_singular_values = self.max_singular_values,
                            max_truncation_err = self.max_truncation_err,
                            trimming_threshold = self.trimming_threshold,
                        )
                        if is_nan(left).numpy() or is_nan(right).numpy():
                            np_svd = partial(
                                split_node_full_svd_numpy,
                                left_edges = [0, 1],
                                right_edges = [2, 3, 4],
                                direction = "right",
                                max_singular_values = self.max_singular_values,
                                max_truncation_err = self.max_truncation_err,
                                trimming_threshold = self.trimming_threshold,
                            )
                            left, right = tf.py_function(
                                np_svd, inp=[updated_tensor], Tout=[self.dtype]*2
                            )
                    elif site == 0:
                        left, right = split_node_full_svd(
                            updated_tensor, [0], [1, 2, 3], direction = "right",
                            max_singular_values = self.max_singular_values,
                            max_truncation_err = self.max_truncation_err,
                            trimming_threshold = self.trimming_threshold,
                        )
                    elif site + 1 == len(self) - 1:
                        left, right = split_node_full_svd(
                            updated_tensor, [0,1], [2, 3], direction = "right",
                            max_singular_values = self.max_singular_values,
                            max_truncation_err = self.max_truncation_err,
                            trimming_threshold = self.trimming_threshold,
                        )
                    self._label_position += 1

            if is_nan(left).numpy():
                self._add_debug_count = "Left tensor has NaN in right step update"
                if is_nan(right).numpy():
                    self._add_debug_count = "Right tensor has NaN in right step update"
                self._test_tmp = updated_tensor
                raise InvalidSingularValueDecomposition(f"left tensor has NaN at site {site} "
                                                        f"tensor: {left.shape} input tensor shape: "
                                                        f"{updated_tensor.shape} " )
            elif is_nan(right).numpy():
                self._add_debug_count = "Right tensor has NaN in right step update"
                self._test_tmp = updated_tensor
                raise InvalidSingularValueDecomposition(f"right tensor has non at site {site} "
                                                        f"tensor: {right.shape} updated tensor "
                                                        f"shape {updated_tensor.shape}" )

            # normalize center of orthogonality
            self[site], self[site + 1] = left, right #/ tf.norm(right)
            self._center_of_orthogonality_position = site + 1

        except Exception:# np.linalg.LinAlgError as err:
            self._add_debug_count = "SVD failed in right step update"
            self.center_position = site + 1
            if not fixed_label:
                self.label_position = self.center_position

        self._compute_left_environments(samples, site + 1)

        L2_regularization = self.lambda_normB * tf.norm(updated_tensor)**2

        prediction = self.predict(
            samples, compute_environments = False, delete_environments = False
        )
        for metric in self.metrics:
            try:
                metric.reset_states()
                metric.update_state(labels, prediction)
            except Exception:
                pass

        self.accuracy.reset_states()
        self.accuracy.update_state(labels, prediction)

        return loss, L2_regularization


    def _left_step_update(self,
                          samples: tf.Tensor,
                          labels: tf.Tensor,
                          site: int,
                          optimizer: Union[float, tf.optimizers.Optimizer],
                          fixed_label: bool = True) -> tf.Tensor:
        """
        Two site update for site and site-1. Calculates the gradient of the loss function
        with respect to the local MPS at site, site-1, updates the local MPS and decomposes it.

        Parameters
        ----------
        samples : tf.Tensor
            input samples
        labels : tf.Tensor
            sample labels
        site : int
            site to be updated
        optimizer: Union[float, tf.optimizers.Optimizer]
            either learning rate or an optimizer
        fixed_label : bool, optional
            should the label be fixed or floating

        Returns
        -------
        tf.Tensor
            loss value
        """
        if not fixed_label:
            assert site == self._label_position

        if self.compressed_sites is not None and samples.shape[-1] != len(self):
            samples = tf.stack([samples[:,:,site] for site in self.compressed_sites], axis=2)

        (gradient,
         current_tensor,
         loss,
         prediction,
         ) = self._left_step_gradient(samples, labels, site)

        # WARNING
        #  not sure if gradient should be normalized,
        #  it seems like its giving more stable results
        gradient = gradient/tf.norm(gradient) if self.normalized_gradient else gradient

        if is_nan(gradient).numpy() :
            self._add_debug_count = "Gradient tensor has NaN values at left step update"
            gradient = tf.where(tf.math.is_nan(gradient), 0.0, gradient)

        # update current tensor
        if isinstance(optimizer, float):
            updated_tensor = current_tensor - optimizer * gradient
        elif isinstance(optimizer, tf.optimizers.Optimizer):
            updated_tensor = tf.Variable(current_tensor, trainable = True, dtype = current_tensor.dtype)
            optimizer.apply_gradients(zip([gradient], [updated_tensor]))


        updated_is_nan = tf.math.is_nan(updated_tensor).numpy()
        if updated_is_nan.any() :
            self._add_debug_count = "Updated Tensor has NaN values at left step update"
            if updated_is_nan.all():
                self.center_position = site - 1
                if not fixed_label:
                    self.label_position = self.center_position
                return loss
            else:
                updated_tensor = tf.where(tf.math.is_nan(updated_tensor), 0.0, updated_tensor)

        left, right = self[site - 1], self[site]

        try:
            if self.label_position not in (site, site-1):
                if site == len(self) - 1:
                    # update node shape (l bond, phy, phy)
                    left, right = split_node_full_svd(
                        updated_tensor, [0,1], [2], direction = "left",
                        max_singular_values = self.max_singular_values,
                        max_truncation_err = self.max_truncation_err,
                        trimming_threshold = self.trimming_threshold,
                    )
                else:
                    if site - 1 > 0:
                        # WARNING !!
                        #  we get NaN values for the tensors sometimes here. This is due
                        # to the tf precision so will try to wrap it up with numpy incase of NaN
                        # values.

                        # (l bond, phy, phy, r bond)
                        left, right = split_node_full_svd(
                            updated_tensor, [0, 1], [2, 3], direction = "left",
                            max_singular_values = self.max_singular_values,
                            max_truncation_err = self.max_truncation_err,
                            trimming_threshold = self.trimming_threshold,
                        )
                        if is_nan(left) or is_nan(right):
                            self._add_debug_count = "TF-SVD failed in left step update"
                            # WARNING this will slow down the calculation
                            np_svd = partial(
                                split_node_full_svd_numpy,
                                left_edges = [0, 1],
                                right_edges = [2, 3],
                                direction = "left",
                                max_singular_values = self.max_singular_values,
                                max_truncation_err = self.max_truncation_err,
                                trimming_threshold = self.trimming_threshold,
                            )
                            left, right = tf.py_function(
                                np_svd, inp=[updated_tensor], Tout=[self.dtype]*2
                            )
                    else:
                        # (phy, phy, r bond)
                        left, right = split_node_full_svd(
                            updated_tensor, [0], [1,2], direction = "left",
                            max_singular_values = self.max_singular_values,
                            max_truncation_err = self.max_truncation_err,
                            trimming_threshold = self.trimming_threshold,
                        )

            elif self.label_position in (site, site - 1):
                if fixed_label:
                    if site == len(self) - 1:
                        if site == self.label_position:
                            # (lbond, phy, phy, nlabel)
                            left, right = split_node_full_svd(
                                updated_tensor, [0,1], [2,3], direction = "left",
                                max_singular_values = self.max_singular_values,
                                max_truncation_err = self.max_truncation_err,
                                trimming_threshold = self.trimming_threshold,
                            )
                        elif site - 1 == self.label_position:
                            # (lbond, phy, nlabel, phy)
                            left, right = split_node_full_svd(
                                updated_tensor, [0, 1, 2], [3], direction = "left",
                                max_singular_values = self.max_singular_values,
                                max_truncation_err = self.max_truncation_err,
                                trimming_threshold = self.trimming_threshold,
                            )
                    else:
                        if site - 1 == 0:
                            if site - 1 == self.label_position:
                                # (phy, nlabel, phy, rbond)
                                left, right = split_node_full_svd(
                                    updated_tensor, [0, 1], [2, 3], direction = "left",
                                    max_singular_values = self.max_singular_values,
                                    max_truncation_err = self.max_truncation_err,
                                    trimming_threshold = self.trimming_threshold,
                                )
                            elif site == self.label_position:
                                # (phy, phy, nlabel, rbond)
                                left, right = split_node_full_svd(
                                    updated_tensor, [0], [1, 2, 3], direction = "left",
                                    max_singular_values = self.max_singular_values,
                                    max_truncation_err = self.max_truncation_err,
                                    trimming_threshold = self.trimming_threshold,
                                )

                else:
                    raise NotImplementedError("Currently under development.")
                    # assuming label is on the right tensor and will be shifted to the left
                    if site not in (1, len(self) - 1):
                        left, right = split_node_full_svd(
                            updated_tensor, [0, 1, 4], [2, 3], direction = "left",
                            max_singular_values = self.max_singular_values,
                            max_truncation_err = self.max_truncation_err,
                            trimming_threshold = self.trimming_threshold,
                        )
                        if is_nan(left).numpy() or is_nan(right).numpy():
                            np_svd = partial(
                                split_node_full_svd_numpy,
                                left_edges = [4, 0, 1],
                                right_edges = [2, 3],
                                direction = "left",
                                max_singular_values = self.max_singular_values,
                                max_truncation_err = self.max_truncation_err,
                                trimming_threshold = self.trimming_threshold,
                            )
                            left, right = tf.py_function(
                                np_svd, inp=[updated_tensor], Tout=[self.dtype]*2
                            )
                        left = transpose(left, (0, 1, 3, 2))
                    elif site == len(self) - 1:
                        left, right = split_node_full_svd(
                            updated_tensor, [0, 1, 3], [2], direction = "left",
                            max_singular_values = self.max_singular_values,
                            max_truncation_err = self.max_truncation_err,
                            trimming_threshold = self.trimming_threshold,
                        )
                        left = transpose(left, (0, 1, 3, 2))
                    elif site == 1:
                        left, right = split_node_full_svd(
                            updated_tensor, [0, 3], [1, 2], direction = "left",
                            max_singular_values = self.max_singular_values,
                            max_truncation_err = self.max_truncation_err,
                            trimming_threshold = self.trimming_threshold,
                        )
                        left = transpose(left, (0, 2, 1))

                    self._label_position -= 1

            if is_nan(left).numpy():
                self._add_debug_count = "Left tensor has NaN in left step update"
                if is_nan(right).numpy():
                    self._add_debug_count = "Right tensor has NaN in left step update"
                self._test_tmp = updated_tensor
                raise Exception(f"left tensor has non at site {site} tensor: {left.shape}"
                                f"input tensor shape: {updated_tensor.shape} " + \
                                str(updated_tensor.numpy()))
            if is_nan(right).numpy():
                self._test_tmp = updated_tensor
                raise Exception(f"right tensor has non at site {site} tensor: {right.shape}" + \
                                str(updated_tensor.numpy()))

            # normalize centre of orthogonality
            self[site - 1], self[site] = left , right #/ tf.norm(left)
            self._center_of_orthogonality_position = site - 1

        except Exception:# np.linalg.LinAlgError as err:
            self._add_debug_count = "SVD failed in left step update"
            self.center_position = site - 1
            if not fixed_label:
                self.label_position = self.center_position

        self._compute_right_environments(samples, site - 1)

        L2_regularization = self.lambda_normB * tf.norm(updated_tensor)**2
        prediction = self.predict(
            samples, compute_environments = False, delete_environments = False
        )
        # loss = self.loss(labels, prediction) + L2_regularization

        for metric in self.metrics:
            try:
                metric.reset_states()
                metric.update_state(labels, prediction)
            except Exception:
                pass

        self.accuracy.reset_states()
        self.accuracy.update_state(labels, prediction)

        return loss, L2_regularization


    def get_gradients(
            self, samples: tf.Tensor, labels: tf.Tensor
    ) -> Tuple[List[tf.Tensor], tf.Tensor, tf.Tensor]:
        """
        Take gradients of Loss function with respect to each MPS node.

        Parameters
        ----------
        samples : tf.Tensor
            input samples
        labels : tf.Tensor
            sample labels

        Returns
        -------
        Tuple[List[tf.Tensor], tf.Tensor]
            gradients, loss and predictions
        """

        if self.compressed_sites is not None and samples.shape[-1] != len(self):
            samples = tf.stack([samples[:,:,site] for site in self.compressed_sites], axis=2)

        with tf.GradientTape() as tape:
            tape.watch(samples)
            tape.watch(self._tensors)
            yhat = self.predict(samples)
            loss = self.loss(labels, yhat)

        if self.normalized_gradient:
            return (
                [grad / tf.norm(grad) for grad in tape.gradient(loss, self._tensors)],
                loss,
                yhat
            )

        return tape.gradient(loss, self._tensors), loss, yhat


    def compressed_prediction(
            self, sample: tf.Tensor = None, dE: float  = 0.0, entropy_threshold: float = 0.0,
    ) -> tf.Tensor:

        assert dE < 1. , "dE needs to be smaller than one."

        entanglement_entropy, _ = self.entanglement_entropy

        sites = []

        entropy_bin = []
        current_entropy = 0
        for site, entropy in enumerate(entanglement_entropy):
            if site in (0, len(self)-1, self.label_position):
                sites.append(site)
            else:
                if entropy_bin == []:
                    entropy_bin.append((entropy,site))
                    current_entropy = entropy
                else:
                    if (entropy > current_entropy * (1.+dE) or entropy < current_entropy * (1.-dE)) or\
                            dE == 0.:
                        entropy_bin.sort(key=lambda x : x[0], reverse=True)
                        if entropy_bin[0][0] >= entropy_threshold:
                            sites.append(entropy_bin[0][1])
                        current_entropy = entropy
                        entropy_bin = []
                        entropy_bin.append((entropy,site))
                    else:
                        entropy_bin.append((entropy,site))

        if entropy_bin != []:
            entropy_bin.sort(key=lambda x : x[0], reverse=True)
            sites.append(entropy_bin[0][1])

        sites += [len(self)-1]

        # print(f"dE = {dE}, Entropy threshold = {entropy_threshold}, Nsites = {len(sites)}")

        compressed_network = []
        for ix, site in enumerate(sites):
            if ix == 0:
                compressed_network.append(self[site])
            else:
                if compressed_network[-1].shape[-1] == self[site].shape[0]:
                    compressed_network.append(self[site])
                else:
                    new_tensor = tn.Node(self[site])
                    eye = tn.Node(
                        tf.eye(compressed_network[-1].shape[-1],self[site].shape[0], dtype=self.dtype)
                    )
                    eye[-1] ^ new_tensor[0]
                    compressed_network.append((eye@new_tensor).tensor)
            if site == self.label_position:
                new_label_position = ix

        new_mps = MPS(
            compressed_network,
            label_position = new_label_position,
            compressed_sites = sites,
            max_truncation_err = self.max_truncation_err,
            max_singular_values = self.max_singular_values,
            trimming_threshold = self.trimming_threshold,
            dropout = self.dropout,
        )
        new_mps.activation = self.activation
        new_mps.loss = self.loss
        new_mps.canonicalize()

        if sample is None:
            return new_mps

        return new_mps.predict(sample)