import tensorflow    as tf
import tensornetwork as tn

tn.set_default_backend("tensorflow")

from tensorflow.keras.layers import Layer
from tensorflow.keras        import initializers, activations

from typing import List, Optional, Text, Tuple, Union, Dict

from TNClassifier.system.exceptions                    import InvalidInput
# from TNClassifier.tensortools.nonadaptive_optimization import calcEnv

@tf.keras.utils.register_keras_serializable(package='TNClassifier')
class MPSlayer(Layer):
    """
    Matrix product state layer input shape is expected to be (Nt, physical dims, N sites)
    This class is based on:
        https://github.com/google/TensorNetwork/blob/master/tensornetwork/tn_keras/mpo.py
    Modified to be consistent with mps.py

    Parameters
    ----------
    N_sites : int
        number of sites
    physical_dim : int
        number of Hilber-space dimensions
    bond_dim :  int
        bond dimensions between tensors
    n_labels : int
        number of labels or output dimension
    label_position : str or int, optional
        position of the label matrix. Default center
    initializer : str, optional
        random value initializer. Default glorot_normal
    dtype : tf.DType, optional
        data type. default `tf.float64`
    kwargs :
        Other keras layer inputs
    """
    def __init__(self,
                 N_sites: int,
                 physical_dim: int,
                 bond_dim: int,
                 n_labels: int,
                 label_position: Union[int,Text] = "center",
                 initializer: Text = "glorot_uniform",
                 activation: Optional[Text] = None,
                 method: Optional[int] = 1,
                 **kwargs) -> None:

        kwargs["input_shape"] = (None, physical_dim, N_sites)

        assert N_sites > 2, "Need at least 2 sites"

        super().__init__(**kwargs)

        if isinstance(label_position, str):
            if label_position in ("left", "start", "beginning") :
                label_position = 0
            elif label_position in ("right", "end") :
                label_position = N_sites - 1
            elif label_position in ("middle", "center", "centre"):
                label_position = N_sites//2
            else:
                raise InvalidInput(f"Unknown label position : {label_position}")
        elif isinstance(label_position, int):
            label_position = label_position if label_position < N_sites else N_sites - 1
        else:
            raise InvalidInput(f"Unknown label position : {label_position}")

        self.num_sites = N_sites
        self.nlabels = n_labels
        self.physical_dims = physical_dim
        self.label_position = label_position
        self.bond_dims = bond_dim
        self._tensors = []
        self.kernel_initializer = initializers.get(initializer)
        self.activation = activations.get(activation)
        self.method = method


    def __len__(self):
        return len(self._tensors)


    def __getitem__(self, site):
        if site < len(self):
            return self._tensors[site]
        else:
            raise InvalidInput("input is beyond the limit of the tensors "
                               f"{site}>={len(self)} !")

    def __iter__(self):
        return iter(self._tensors)


    def build(self, input_shape: List[int]) -> None:

        assert input_shape[-1] == self.num_sites and input_shape[-2] == self.physical_dims, \
            "Number of sites and/or Hilbert dimensions does not match " \
            f"number of sites: {input_shape[-1]}?={self.num_sites}, " \
            f"Hilbert dims {input_shape[-2]}?={self.physical_dims}"

        super().build(input_shape)

        for n in range(self.num_sites):
            if n == 0:
                shape = (self.physical_dims, self.bond_dims)
            elif n == self.num_sites -1:
                shape = (self.bond_dims, self.physical_dims)
            else:
                shape = (self.bond_dims, self.physical_dims, self.bond_dims)
            if n == self.label_position:
                shape = (*shape, self.nlabels)
            self._tensors.append(
                self.add_weight(
                    name=f"Node_{n}",
                    shape=shape,
                    trainable=True,
                    initializer = self.kernel_initializer
                )
            )

    @tf.function
    def call(self, inputs: tf.Tensor, **kwargs) -> tf.Tensor:

        def calcEnviron(sample: tf.Tensor, network: List[tf.Tensor], label_position: int) -> tf.Tensor:
            for site, tensor in enumerate(network):
                if label_position > site:
                    if site == 0:
                        left_env = tn.ncon([tensor, sample[:,0]], [[1,-1],[1]])
                    else:
                        left_env = tn.ncon([left_env, tensor, sample[:,site]],
                                           [[1],[1,2,-1],[2]])

                elif label_position < site:
                    if site == len(network) - 1:
                        # Label index is always at the very right
                        left_env = tn.ncon([left_env, tensor, sample[:,site]],
                                           [[1,-1],[1,2],[2]])
                    else:
                        # Label index is always at the very right
                        left_env = tn.ncon([left_env, tensor, sample[:,site]],
                                           [[1,-2],[1,2,-1],[2]])

                elif label_position == site:
                    if site == 0:
                        # Label index is always at the very right
                        left_env = tn.ncon([tensor, sample[:,0]], [[1,-1,-2],[1]])
                    elif site == len(network) - 1:
                        left_env = tn.ncon([left_env, tensor, sample[:,site]],
                                           [[1],[1,2,-1],[2]])
                    else:
                        left_env = tn.ncon([left_env, tensor, sample[:,site]],
                                           [[1],[1,2,-1,-2],[2]])
                left_env /= tf.norm(left_env)

            return left_env

        result = tf.vectorized_map(
            lambda vec: calcEnviron(vec, self._tensors, self.label_position), inputs
        )
        if self.activation is not None:
            result = self.activation(result)
        return result

    def compute_output_shape(self, input_shape: List[int]) -> Tuple[int, int]:
        return (input_shape[0], self.nlabels)

    def get_config(self) -> Dict:
        config = {}

        args = ["num_sites","nlabels","physical_dims","label_position","bond_dims"]
        for arg in args:
            config[arg] = getattr(self, arg)
        config["kernel_initializer"] = initializers.serialize(getattr(self, "kernel_initializer"))

        # Get base config
        base_config = super().get_config()
        return dict(list(base_config.items()) + list(config.items()))



def calcEnviron_method2(sample: tf.Tensor, mps: List[tf.Tensor], label_position: int) -> tf.Tensor:
    x = tf.unstack(sample, axis = 1)
    for site, tensor in enumerate(mps):
        x_node = tn.Node(x[site], name=f"smp_{site}", axis_names = [f"phy_{site}"])

        if site == 0:
            if site != label_position:
                tensor_node = tn.Node(
                    tensor,
                    name=f"site_{site}",
                    axis_names=[f"phy_{site}","right"],
                )
                tensor_node[f"phy_0"] ^ x_node[f"phy_0"]

                left_env = tn.contract_between(
                    tensor_node, x_node,
                    allow_outer_product=True,
                    name="left_env",
                    output_edge_order=[tensor_node["right"]],
                    axis_names=["right"]
                )
                left_env.tensor /= tf.norm(left_env.tensor)

            else:
                tensor_node = tn.Node(
                    tensor,
                    name=f"site_{site}",
                    axis_names=[f"phy_{site}", "right", "label"],
                )
                tensor_node[f"phy_0"] ^ x_node[f"phy_0"]

                left_env = tn.contract_between(
                    tensor_node, x_node,
                    allow_outer_product=True,
                    name="left_env",
                    output_edge_order=[left_env["right"], left_env["label"]],
                    axis_names=["right", "label"]
                )
                left_env.tensor /= tf.norm(left_env.tensor)

        elif site == len(mps) - 1:
            if site != label_position:
                tensor_node = tn.Node(
                    tensor,
                    name=f"site_{site}",
                    axis_names=["left", f"phy_{site}"],
                )
                tensor_node[f"phy_{site}"] ^ x_node[f"phy_{site}"]

                tensor_node = tn.contract_between(
                    tensor_node, x_node,
                    allow_outer_product=True,
                    output_edge_order=[tensor_node["left"]],
                    axis_names=["left"]
                )
                tensor_node.tensor /= tf.norm(tensor_node.tensor)

                tensor_node["left"] ^ left_env["right"]
                left_env = tn.contract_between(
                    tensor_node, left_env,
                    allow_outer_product=True,
                    name="left_env",
                    output_edge_order=[left_env["label"]],
                    axis_names=["label"]
                )

            else:
                tensor_node = tn.Node(
                    tensor,
                    name=f"site_{site}",
                    axis_names=["left", f"phy_{site}", "label"],
                )
                tensor_node[f"phy_{site}"] ^ x_node[f"phy_{site}"]

                tensor_node = tn.contract_between(
                    tensor_node, x_node,
                    allow_outer_product=True,
                    output_edge_order=[tensor_node["left"], tensor_node["label"]],
                    axis_names=["left", "label"]
                )
                tensor_node.tensor /= tf.norm(tensor_node.tensor)

                tensor_node["left"] ^ left_env["right"]
                left_env = tn.contract_between(
                    tensor_node, left_env,
                    allow_outer_product=True,
                    name="left_env",
                    output_edge_order=[tensor_node["label"]],
                    axis_names=["label"]
                )
        else:
            if site != label_position:
                tensor_node = tn.Node(
                    tensor,
                    name=f"site_{site}",
                    axis_names= ["left", f"phy_{site}", "right"],
                )
                tensor_node[f"phy_{site}"] ^ x_node[f"phy_{site}"]

                tensor_node = tn.contract_between(
                    tensor_node, x_node,
                    allow_outer_product=True,
                    output_edge_order=[tensor_node["left"],tensor_node["right"]],
                    axis_names=["left","right"]
                )
                tensor_node.tensor /= tf.norm(tensor_node.tensor)

                tensor_node["left"] ^ left_env["right"]
                additional = [[],[]]
                if site > label_position:
                    additional = [[left_env["label"]],["label"]]

                left_env = tn.contract_between(
                    tensor_node, left_env,
                    allow_outer_product=True,
                    name="left_env",
                    output_edge_order=[tensor_node["right"]] + additional[0],
                    axis_names=["right"] + additional[1]
                )

            else:
                tensor_node = tn.Node(
                    tensor,
                    name=f"site_{site}",
                    axis_names= ["left", f"phy_{site}", "right", "label"],
                )

                tensor_node[f"phy_{site}"] ^ x_node[f"phy_{site}"]

                tensor_node = tn.contract_between(
                    tensor_node, x_node,
                    allow_outer_product=True,
                    output_edge_order=[tensor_node["left"],tensor_node["right"],tensor_node["label"]],
                    axis_names=["left","right","label"]
                )
                tensor_node.tensor /= tf.norm(tensor_node.tensor)

                tensor_node["left"] ^ left_env["right"]
                left_env = tn.contract_between(
                    tensor_node, left_env,
                    allow_outer_product=True,
                    name="left_env",
                    output_edge_order=[tensor_node["right"],tensor_node["label"]],
                    axis_names=["right","label"]
                )

    return left_env.tensor