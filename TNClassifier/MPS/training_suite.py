import tensorflow as tf
import numpy      as np

from TNClassifier.MPS.MPSBase                          import MPSBase
from TNClassifier.MPS.DataGeneratorBase                import DataGeneratorBase
from TNClassifier.system.feedback                      import get_elapsed_time, progress
from TNClassifier.system.exceptions                    import InvalidInput
from TNClassifier.tensortools.nonadaptive_optimization import nonadaptive_optimization_step
import time

from typing import List, Callable, Union, Text, Tuple, Optional
from functools import partial

class TrainingSuite(object):
    def __init__(self,
                 training_generator: DataGeneratorBase,
                 network: MPSBase,
                 validation_generator: DataGeneratorBase = None):
        """
        Training Suite for TNClassifier. Tracks the error messages

        Parameters
        ----------
        training_generator : DataGenerator
            training data
        network : MPSBase
            Tensor Networ defined with respect to the training data
        validation_generator : DataGenerator, optional
            validation data
        """

        assert isinstance(training_generator, DataGeneratorBase) and \
               isinstance(validation_generator, (DataGeneratorBase, type(None))) and \
               isinstance(network, MPSBase), "Incorrect input."

        self.training_sample = training_generator
        self.validation_sample = validation_generator
        self.network = network
        for attr in ["_epochs","_epoch", "_nbatch"]:
            setattr(self,attr,0)


    def __enter__(self):
        if self.validation_sample is not None:
            if self.network.compressed_sites is None:
                assert self.training_sample.n_pixels == self.validation_sample.n_pixels == self.network.num_sites,\
                    "Number of sites for training, validation and MPS does not match, " \
                    f"{self.training_sample.n_pixels} != {self.validation_sample.n_pixels} != {self.network.num_sites}"
        else:
            if self.network.compressed_sites is None:
                assert self.training_sample.n_pixels == self.network.num_sites, \
                    "Number of sites for training and MPS does not match, " \
                    f"{self.training_sample.n_pixels} != {self.network.num_sites}"
        return self


    def __exit__(self, exception_type, exception_value, traceback):
        # TODO implement full exception handling
        if hasattr(self, "history"):
            print("\n   * Training stats:")
            for key, item in self.history.items():
                try:
                    if len(item) > 0:
                        print(' '.ljust(6) + key.ljust(30) + f"{item[-1]:.4e}")
                except Exception:
                    print(f"problem with {key} -> {item}")
        if len(list(self.network._debug_counter.keys())) > 0:
            print("\n   * Stats for errors and exceptions:")
            for name, value in self.network._debug_counter.items():
                print(f"      - {value} times -> {name}")
        else:
            print("\n   * No errors have been generated during the optimization.")

        if hasattr(self, "_start_time"):
            print(f"   * Elapsed Time: {get_elapsed_time(self._start_time)}")


    def fit(self,
            epochs: int,
            n_sweep_per_batch: int = 1,
            learning_rate: float = 1e-2,
            fixed_label: bool = True,
            callbacks: Union[List[Callable], Callable] = None) -> dict:
        """
        Fit the network with respect to the training data

        Parameters
        ----------
        epochs : int
            Number of epochs to run
        n_sweep_per_batch : int, optional
            Number of sweeps to be performed during each batch training. A sweep is defined as
            optimization sequence running from left to right then coming back to left end site.
            Default is 1.
        learning_rate : float, optional
            learning rate
        fixed_label : bool, optional
            To run fixed label sweep or not.

        Returns
        -------
        dict
            epoch history
        """

        if not isinstance(callbacks, list):
            callbacks = [callbacks] if callbacks is not None else []
        for callback in callbacks:
            if not hasattr(callback, "__call__"):
                raise InvalidInput("Callbacks need to be callable.")

        n_sweep_per_batch = n_sweep_per_batch if n_sweep_per_batch > 0 else 1

        self._start_time = time.time()
        self.history = {"train_loss":[], "train_acc":[], "val_loss":[], "val_acc":[]}

        try:
            # Training loop
            for epoch in range(1,epochs+1):

                # Batch loop
                for nbatch, (sample, label) in enumerate(self.training_sample):
                    self.network.compute_environments(sample, verbose = 0)

                    # Sweep loop
                    for _ in range(n_sweep_per_batch):
                        for site in range(self.network.num_sites - 1):
                            loss = self.network._right_step_update(
                                sample, label, site, learning_rate, fixed_label
                            )
                            acc = self.network.accuracy.result()

                            status = f"sweep {site}, loss {loss.numpy():.3e}, " \
                                     f"acc {acc.numpy():.1%}, " \
                                     f"[L,R] bond {self.network.bond_at_site(site)}"

                            progress(
                                nbatch,
                                len(self.training_sample),
                                status = status,
                                start_time = self._start_time,
                                id = f"Epoch {epoch}/{epochs}"
                            )

                        for site in reversed(range(1, self.network.num_sites)):
                            loss = self.network._left_step_update(
                                sample, label, site, learning_rate, fixed_label
                            )
                            acc = self.network.accuracy.result()

                            status = f"sweep {site}, loss {loss.numpy():.3e}, " \
                                     f"acc {acc.numpy():.1%}, " \
                                     f"[L,R] bond {self.network.bond_at_site(site)}"

                            progress(
                                nbatch,
                                len(self.training_sample),
                                status = status,
                                start_time = self._start_time,
                                id = f"Epoch {epoch}/{epochs}"
                            )

                    if nbatch == 0:
                        batch_loss = tf.reshape(loss, (-1,1))
                        batch_acc = tf.reshape(acc, (-1,1))
                    else:
                        batch_loss = tf.concat((batch_loss, tf.reshape(loss, (-1,1))), axis=0)
                        batch_acc = tf.concat(
                            (batch_acc, tf.reshape(acc, (-1,1))),
                            axis=0
                        )

                    # For testing
                    # break
                self.training_sample.Reset()
                self.history["train_loss"].append(tf.reduce_mean(batch_loss).numpy())
                self.history["train_acc"].append(tf.reduce_mean(batch_acc).numpy())

                # Validation
                if self.validation_sample is not None:
                    print()
                    for nbatch, (sample, label) in enumerate(self.validation_sample):
                        prediction = self.network.predict(sample)

                        self.network.accuracy.reset_states()
                        self.network.accuracy.update_state(label, prediction)

                        if nbatch == 0:
                            batch_loss = tf.reshape(self.network.loss(label, prediction), (-1,1))
                            batch_acc = tf.reshape(self.network.accuracy.result(), (-1,1))
                        else:
                            batch_loss = tf.concat(
                                (batch_loss, tf.reshape(self.network.loss(label, prediction), (-1,1))),
                                axis=0
                            )
                            batch_acc = tf.concat(
                                (batch_acc, tf.reshape(self.network.accuracy.result(), (-1,1))),
                                axis=0
                            )

                        status = f"loss {tf.reduce_mean(batch_loss).numpy():.3e}, " \
                                 f"acc {tf.reduce_mean(batch_acc).numpy():.1%}, "
                        progress(
                            nbatch,
                            len(self.validation_sample),
                            id="Validation",
                            status = status,
                            start_time = self._start_time,
                        )

                    self.validation_sample.Reset()
                    self.history["val_loss"].append(tf.reduce_mean(batch_loss).numpy())
                    self.history["val_acc"].append(tf.reduce_mean(batch_acc).numpy())

                # run call backs
                for callback in callbacks:
                    callback(self)

        except KeyboardInterrupt:
            print("\n   * Training stopped by the user.")


        return self.history


    def evaluate(
            self,
            validation_sample: Union[Tuple[tf.Tensor], None] = None,
            tag: Text ="_",
            verbose: bool = True,
            numbatch=None,
            L2_regularization = 0.0
    ):
        """
        Evaluate network metrics with respect to a test sample.

        Parameters
        ----------
        validation_sample : Union[Tuple[tf.Tensor], None]
            Test sample to be evaluated. If `None`; the module will look for the `DataGenerator`
            validation sample given during the initiation. If `Tuple[tf.Tensor]` given which
            should include `(data, label)` the evaluation will be calculated according to that
            specific data.
        tag : Text
            tag for history dictionary.
        verbose : bool
            switch to turn off feedback
        numbatch : int
            number of batches to execute. Only valid if `validation_sample` is `None`
        """

        if validation_sample is None and self.validation_sample is not None:
            # if verbose:
            #     print("\n")
            max_n_batch = len(self.validation_sample)-1 if numbatch is None else numbatch

            batch_dict = {}
            for metric in self.network.metrics:
                batch_dict["val_"+metric.name] = []

            for nbatch, (sample, label) in enumerate(self.validation_sample):
                prediction = self.network.predict(sample)

                self.network.accuracy.reset_states()
                self.network.accuracy.update_state(label, prediction)

                if nbatch == 0:
                    batch_loss = tf.reshape(self.network.loss(label, prediction) + L2_regularization, (-1,1))
                    batch_acc = tf.reshape(self.network.accuracy.result(), (-1,1))
                else:
                    batch_loss = tf.concat(
                        (batch_loss, tf.reshape(self.network.loss(label, prediction) + L2_regularization, (-1,1))),
                        axis=0
                    )
                    batch_acc = tf.concat(
                        (batch_acc, tf.reshape(self.network.accuracy.result(), (-1,1))),
                        axis=0
                    )

                for metric in self.network.metrics:
                    try:
                        metric.reset_states()
                        metric.update_state(label, prediction)
                    except Exception:
                        pass
                    batch_dict["val_"+metric.name].append(metric.result().numpy())

                status = f"loss {tf.reduce_mean(batch_loss).numpy():.3e}, " \
                         f"acc {tf.reduce_mean(batch_acc).numpy():.1%}, "
                if verbose:
                    progress(
                        nbatch+1,
                        max_n_batch+1,
                        id="Validation",
                        status = status,
                        start_time = self._start_time,
                    )

                if nbatch >= max_n_batch:
                    break

            self.validation_sample.Reset()
            if hasattr(self, "history"):
                if f"val{tag}loss" not in self.history.keys():
                    self.history[f"val{tag}loss"] = []
                self.history[f"val{tag}loss"].append(tf.reduce_mean(batch_loss).numpy())
                if f"val{tag}acc" not in self.history.keys():
                    self.history[f"val{tag}acc"] = []
                self.history[f"val{tag}acc"].append(tf.reduce_mean(batch_acc).numpy())
                txt = ""
                for metric in self.network.metrics:
                    if f"val{tag}"+metric.name not in self.history.keys():
                        self.history[f"val{tag}"+metric.name] = []
                    self.history[f"val{tag}"+metric.name].append(
                        np.mean(batch_dict["val_"+metric.name])
                    )
                    txt += f" {f'val{tag}'+metric.name} {self.history[f'val{tag}'+metric.name][-1]:.3e}"
            else:
                return tf.reduce_mean(batch_loss).numpy(), tf.reduce_mean(batch_acc).numpy()

            if verbose:
                print("\n"+txt)

        elif validation_sample is not None:
            sample, label = validation_sample
            assert tf.is_tensor(sample), f"Validation sample is not tensor."
            prediction = self.network.predict(sample)
            self.network.accuracy.reset_states()
            self.network.accuracy.update_state(label, prediction)

            for metric in self.network.metrics:
                try:
                    metric.reset_states()
                    metric.update_state(label, prediction)
                except Exception:
                    pass

            loss = self.network.loss(label, prediction) + L2_regularization
            if hasattr(self, "history"):
                if f"val{tag}loss" not in self.history.keys():
                    self.history[f"val{tag}loss"] = []
                self.history[f"val{tag}loss"].append(loss.numpy())
                if f"val{tag}acc" not in self.history.keys():
                    self.history[f"val{tag}acc"] = []
                self.history[f"val{tag}acc"].append(self.network.accuracy.result().numpy())
                for metric in self.network.metrics:
                    if f"val{tag}"+metric.name not in self.history.keys():
                        self.history[f"val{tag}"+metric.name] = []
                    self.history[f"val{tag}"+metric.name].append(metric.result().numpy())
            else:
                return loss.numpy(), self.network.accuracy.result().numpy()



    def sweep_based_fit(
            self,
            epochs: int,
            n_sweep_per_batch: int = 1,
            optimizer: Union[float, tf.optimizers.Optimizer] = 1e-2,
            fixed_label: bool = True,
            sweep_callbacks: Union[List[Callable], Callable] = None,
            epoch_callbacks: Union[List[Callable], Callable] = None,
            time_benchmark: bool = False,
    ) -> dict:

        if not isinstance(sweep_callbacks, list):
            sweep_callbacks = [sweep_callbacks] if sweep_callbacks is not None else []
        for sweep_callback in sweep_callbacks:
            if not hasattr(sweep_callback, "__call__"):
                raise InvalidInput("Callbacks need to be callable.")
        if not isinstance(epoch_callbacks, list):
            epoch_callbacks = [epoch_callbacks] if epoch_callbacks is not None else []
        for epoch_callback in epoch_callbacks:
            if not hasattr(epoch_callback, "__call__"):
                raise InvalidInput("Callbacks need to be callable.")

        n_sweep_per_batch = n_sweep_per_batch if n_sweep_per_batch > 0 else 1

        self._start_time = time.time()
        self.history = dict(
            train_loss = [],
            train_acc = [],
            val_loss = [],
            val_acc = [],
        )

        self.optimizer = optimizer
        self._epochs = epochs

        for metric in self.network.metrics:
            for name in ["train_","val_","train_sweep_","val_sweep_"]:
                self.history[name+metric.name] = []

        try:
            # Training loop
            for epoch in range(1,epochs+1):
                self._epoch = epoch
                # self.lr = learning_rate

                # Batch loop
                batch_history = {}
                for nbatch, (sample, label) in enumerate(self.training_sample):
                    self._nbatch = nbatch

                    # Sweep loop
                    validation_sample = None
                    if self.validation_sample is not None:
                        self.validation_sample.on_epoch_end()
                        validation_sample = self.validation_sample[0]

                    sweep_time = time.time()
                    sweep_history = self._sweep_step(
                        sample,
                        label,
                        n_sweep_per_batch,
                        self.optimizer,
                        validation_sample,
                        sweep_callbacks,
                        fixed_label,
                    )

                    if time_benchmark: print(f"Sweep time: {get_elapsed_time(sweep_time)}")

                    for key, item in sweep_history.items():
                        if key not in batch_history.keys():
                            batch_history[key] = []
                        batch_history[key].append(item)

                self.training_sample.Reset()
                self.validation_sample.Reset()

                # End of the batch
                self.history["train_loss"].append(
                    tf.reduce_mean(batch_history["train_sweep_loss"]).numpy()
                )
                self.history["train_acc"].append(
                    tf.reduce_mean(batch_history["train_sweep_acc"]).numpy()
                )
                txt = f"Epoch {epoch}/{epochs}: train loss {self.history['train_loss'][-1]:.3e}, " \
                      f"train accuracy {self.history['train_acc'][-1]:.1%} "
                for metric in self.network.metrics:
                    self.history["train_"+metric.name].append(
                        np.mean(batch_history["train_sweep_"+metric.name])
                    )
                    txt += f", {'train_'+metric.name} {self.history['train_'+metric.name][-1]:.3e}"
                print(txt)
                self.evaluate(L2_regularization = self._L2_regularization)

                stop_training = False
                for callback in epoch_callbacks:
                    stop_training = True if callback(self) == True else False
                    if stop_training: break

                if stop_training:
                    break

        except KeyboardInterrupt:
            print("\n   * Training stopped by the user.")

        return self.history


    def _sweep_step(
            self,
            batch: tf.Tensor,
            batch_labels: tf.Tensor,
            n_sweeps: int = 1,
            optimizer: Union[float, tf.optimizers.Optimizer] = 1e-4,
            validation_sample: Optional[Tuple[tf.Tensor]] = None,
            callbacks: Optional[Callable] = None,
            fixed_label: bool = True,
    ) -> dict:

        if n_sweeps <= 0:
            return {}

        for nsweep in range(n_sweeps):
            self.network.compute_environments(batch, verbose = 0)
            # sweep to the right
            for site in range(self.network.num_sites - 1):
                loss, _ = self.network._right_step_update(
                    batch, batch_labels, site, optimizer, fixed_label
                )
                acc = self.network.accuracy.result()

                status = f"site {site}, loss {loss.numpy():.3e}, " \
                         f"acc {acc.numpy():.1%}, " \
                         f"[L,R] bond {self.network.bond_at_site(site)}"

                progress(
                    self._nbatch+1,
                    len(self.training_sample),
                    status = status,
                    start_time = self._start_time,
                    id = f"Epoch {self._epoch}/{self._epochs}, "
                         f"sweep {nsweep+1}/{n_sweeps}",
                    bar_len = 15,
                    )

            # sweep to the left
            for site in reversed(range(1, self.network.num_sites)):
                loss, self._L2_regularization = self.network._left_step_update(
                    batch, batch_labels, site, optimizer, fixed_label
                )
                acc = self.network.accuracy.result()

                status = f"site {site}, loss {loss.numpy():.3e}, " \
                         f"acc {acc.numpy():.1%}, " \
                         f"[L,R] bond {self.network.bond_at_site(site)}"

                progress(
                    self._nbatch+1,
                    len(self.training_sample),
                    status = status,
                    start_time = self._start_time,
                    id = f"Epoch {self._epoch}/{self._epochs}, "
                         f"sweep {nsweep+1}/{n_sweeps}",
                    bar_len = 15,
                    )

            if "train_sweep_loss" not in self.history.keys():
                self.history["train_sweep_loss"] = []
            self.history["train_sweep_loss"].append(loss.numpy())
            if "train_sweep_acc" not in self.history.keys():
                self.history["train_sweep_acc"] = []
            self.history["train_sweep_acc"].append(acc.numpy())

            for metric in self.network.metrics:
                if "train_sweep_"+metric.name not in self.history.keys():
                    self.history["train_sweep_"+metric.name] = []
                self.history["train_sweep_"+metric.name].append(metric.result().numpy())

            if isinstance(validation_sample, tuple):
                self.evaluate(
                    validation_sample = validation_sample,
                    tag="_sweep_",
                    numbatch=1,
                    verbose=True if nsweep == n_sweeps-1 else False,
                    L2_regularization = self._L2_regularization,
                )

            # run call backs
            stop_sweep = False
            for callback in callbacks:
                if callback(self, sweep_info=(nsweep, n_sweeps)):
                    stop_sweep = True
                    break

            if stop_sweep:
                break

        return {
            key : item
            for key, item in self.history.items()
            if "_sweep_" in key
        }


    def _sgd_step(self, samples, labels, optimizer) -> Tuple[tf.Tensor]:
        trainable_tensors = self.network.trainable_variables
        gradients, loss, predictions = self.network.get_gradients(samples, labels)
        optimizer.apply_gradients(zip(gradients, trainable_tensors))
        self.network.set_tensors(trainable_tensors)
        return loss, predictions


    def mixed_fit(
            self,
            epochs: int,
            optimizer: tf.optimizers.Optimizer,
            n_sweep_per_epoch: int = 1,
            DMRG_frac: float = 0.5,
            fixed_label: bool = True,
            sweep_callbacks: Union[List[Callable], Callable] = None,
            epoch_callbacks: Union[List[Callable], Callable] = None,
            time_benchmark: bool = False,
    ) -> dict:

        if not isinstance(sweep_callbacks, list):
            sweep_callbacks = [sweep_callbacks] if sweep_callbacks is not None else []
        for sweep_callback in sweep_callbacks:
            if not hasattr(sweep_callback, "__call__"):
                raise InvalidInput("Callbacks need to be callable.")
        if not isinstance(epoch_callbacks, list):
            epoch_callbacks = [epoch_callbacks] if epoch_callbacks is not None else []
        for epoch_callback in epoch_callbacks:
            if not hasattr(epoch_callback, "__call__"):
                raise InvalidInput("Callbacks need to be callable.")

        self._start_time = time.time()
        self.history = dict(
            train_loss = [],
            train_acc = [],
            val_loss = [],
            val_acc = [],
        )

        for metric in self.network.metrics:
            for name in ["train_","val_"]:
                self.history[name+metric.name] = []

        self.optimizer = optimizer

        try:
            # Training loop
            self._epochs = epochs
            for epoch in range(1,epochs+1):
                self._epoch = epoch

                batch_loss = []
                for nbatch, (sample, label) in enumerate(self.training_sample):
                    self._nbatch = nbatch

                    # Use DMRG update for first batch
                    if nbatch < int(len(self.training_sample) * DMRG_frac) and\
                            n_sweep_per_epoch > 0:
                        sweep_history = self._sweep_step(
                            sample,
                            label,
                            n_sweep_per_epoch,
                            float(self.optimizer.lr.numpy()),
                            None,
                            sweep_callbacks,
                            fixed_label,
                        )
                        batch_loss.append(sweep_history["train_sweep_loss"][-1])

                    else:
                        if time_benchmark:
                            step_time = time.time()
                            from datetime import datetime
                            precise_time = datetime.now()
                        loss, predictions = self._sgd_step(
                            samples = sample,
                            labels = label,
                            optimizer = self.optimizer,
                        )
                        if time_benchmark:
                            print(f"One step of SGD time: {get_elapsed_time(step_time)} "
                                  f"({(datetime.now() - precise_time).total_seconds() * 1000} milisec)")
                        batch_loss.append(loss.numpy())
                        self.network.accuracy.update_state(label, predictions)
                        for metric in self.network.metrics:
                            try:
                                metric.update_state(label, predictions)
                            except tf.errors.InvalidArgumentError:
                                pass

                    status = f"loss {np.mean(batch_loss):.3e}, " \
                             f"acc {self.network.accuracy.result().numpy():.1%}, "
                    progress(
                        self._nbatch+1,
                        len(self.training_sample),
                        status = status,
                        start_time = self._start_time,
                        id = f"Epoch {self._epoch}/{self._epochs} ",
                    )

                self.history["train_loss"].append(np.mean(batch_loss))
                self.history["train_acc"].append(self.network.accuracy.result().numpy())
                txt = f"Epoch {epoch}/{epochs}: train loss {self.history['train_loss'][-1]:.3e}, " \
                      f"train accuracy {self.history['train_acc'][-1]:.1%} "
                for metric in self.network.metrics:
                    self.history["train_"+metric.name].append(metric.result().numpy())
                    txt += f", {'train_'+metric.name} {self.history['train_'+metric.name][-1]:.3e}"
                print(txt)
                self.evaluate()

                stop_training = False
                for callback in epoch_callbacks:
                    stop_training = True if callback(self) == True else False
                    if stop_training: break

                if stop_training:
                    break

        except KeyboardInterrupt:
            print("\n   * Training stopped by the user.")

        return self.history