import tensorflow as tf


@tf.function
def mean_squared_error(y_truth: tf.Tensor, y_pred: tf.Tensor) -> tf.Tensor:
    return tf.reduce_sum(tf.losses.mean_squared_error(y_truth, y_pred))
