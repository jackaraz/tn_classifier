import tensorflow as tf
import numpy      as np


class DataGeneratorBase:
    def __init__(
            self, batch_size: int = 128, shuffle: bool = True, dtype: tf.DType = tf.float64
    ):

        self.batch_size = batch_size
        self.shuffle = shuffle
        self.dtype = dtype
        self.sample_size = 0
        self.datasample = []

    def __len__(self):
        'Denotes the number of batches per epoch'
        return self.sample_size//self.batch_size + 1

    @property
    def size(self):
        return len(self)

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        raise NotImplementedError()

    def __data_generation(self):
        raise NotImplementedError()

    def Reset(self):
        del self.datasample
        self.datasample = []

    @staticmethod
    def _downsample(image: np.ndarray, pad: bool = False) -> np.ndarray:
        Nt = image.shape[0]
        im_x, im_y = image.shape[1:]
        padded = image
        if pad:
            padded  = np.pad(padded,pad_width=((0,0),(1,1),(1,1)),mode='constant')
        rolled1 = np.roll(padded,shift=(0,-1,-1),axis=(0,1,2))
        rolled2 = np.roll(padded,shift=(0,0,-1),axis=(0,1,2))
        rolled3 = np.roll(padded,shift=(0,-1,0),axis=(0,1,2))
        ave = (padded + rolled1 + rolled2 + rolled3)/4

        return ave[:,1:-1:2, 1:-1:2]