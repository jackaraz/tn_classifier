import tensorflow    as tf
import tensornetwork as tn
import numpy         as np

tn.set_default_backend('tensorflow')

from typing                                 import List, Any, Union, Tuple, Text, Dict

from TNClassifier.system.exceptions         import InvalidInput, InvalidFileFormat
from TNClassifier.tensortools.decomposition import (split_node_full_svd, split_node_qr,
                                                    split_node_rq)
from TNClassifier.tensortools.misc          import ncon, normalize, transpose, is_nan

Tensor = Any

"""
    Based on: 
        https://github.com/google/TensorNetwork/tree/0.1.0/experiments/MPS_classifier
        https://github.com/google/TensorNetwork/tree/0.1.0/experiments/MPS
"""


class MPSBase:
    def __init__(
            self,
            tensors: List[Tensor],
            label_position: int,
            max_truncation_err: float = None,
            max_singular_values: int = None,
            trimming_threshold: float = 0.0,
            dropout: float = 0.0
        ) -> None:

        self._tensors = tensors
        self._label_position = label_position
        dtype = self.dtype
        self._center_of_orthogonality_position = 0  # always start from left

        self.max_truncation_err = max_truncation_err
        self.max_singular_values = max_singular_values
        self.trimming_threshold = trimming_threshold
        self.dropout = dropout

        self._debug_count = {}

    @property
    def right_bond_dims(self) -> List[int]:
        return [self[site].shape[-1] for site in range(len(self)-1)]

    @property
    def left_bond_dims(self) -> List[int]:
        return [self[site].shape[0] for site in range(1, len(self)-1)] + \
               [self[len(self)-1].shape[0]]

    @property
    def bond_dims(self) -> Union[List[Tuple[int]], List[Tuple[int, int]]]:
        """
        Bond dimensions of each tensor
        """
        return [(None, self[0].shape[-1])] + \
               [(self[site].shape[0], self[site].shape[-1]) for site in range(1, len(self)-1)] + \
               [(self[len(self)-1].shape[0],)]

    def bond_at_site(self, site) -> List[int]:
        if site == 0:
            return [None, self[0].shape[-1]]
        if site == len(self) - 1:
            return [self[len(self) - 1].shape[0], None]
        else:
            return [self[site].shape[0], self[site].shape[-1]]

    @property
    def max_bond_dim(self) -> int:
        return max([max(x) for x in self.bond_dims])

    @property
    def physical_dims(self) -> List[int]:
        """
        Hilbert Space dimensions
        """
        return [self[0].shape[0]] + [self[site].shape[1] for site in range(1, len(self)+1)]

    @property
    def num_sites(self) -> int:
        return len(self)

    @property
    def num_params(self) -> int:
        return sum([np.prod(t.shape) for t in self])

    def __len__(self) -> int:
        return len(self._tensors)


    def __getitem__(self, site) -> tf.Tensor:
        return self._tensors[site]
        # if site < len(self):
        #     return self._tensors[site]
        # else:
        #     raise InvalidInput(f"input {site} is beyond the limit of the tensors "
        #                        f"len(mps) = {len(self)} !")


    def __setitem__(self, site: int, tensor: Tensor) -> None:
        assert 0 <= site < self.num_sites, f"site {site} is beyond the limit of the boundaries " \
                                           f"{site}>={self.num_sites} !"
        if not tf.is_tensor(tensor):
            tensor = tf.convert_to_tensor(tensor, dtype=self.dtype)

        self._tensors[site] = tensor


    def __iter__(self):
        return iter(self._tensors)

    @property
    def dtype(self):
        if not hasattr(self, "_dtype"):
            assert ( all([self[0].dtype == t.dtype for t in self]) )
            self._dtype = self[0].dtype

        return self._dtype


    @property
    def _debug_counter(self):
        return self._debug_count


    @_debug_counter.setter
    def _add_debug_count(self, name: Text):
        if name in self._debug_count.keys():
            self._debug_count[name] += 1
        else:
            self._debug_count[name] = 1

    @property
    def label_indices(self):
        if self.label_position is not None:
            return (
                self.label_tensor.shape[-2]
                if self.label_position < len(self) - 1 else self.label_tensor.shape[-1]
            )

        return 1

    def save_mps(self,filename: Text) -> None:
        """
        Save MPS tensors as compressed numpy file `.npz`, `.h5` or `.hdf5` format

        Parameters
        ----------
        filename : str
            name of the file with `.npz`, `.h5` or `.hdf5` extention
        """
        if filename.endswith(".npz"):
            import numpy as np
            numpy_tensors = np.array([t.numpy() for t in self._tensors], dtype=object)
            np.savez_compressed(filename, mps=numpy_tensors, num_sites=self.num_sites,
                                center_of_orthogonality=self.center_position,
                                label_position=self.label_position,
                                max_singular_values=(self.max_singular_values \
                                             if self.max_singular_values is not None else -1),
                                max_truncation_err = (self.max_truncation_err \
                                              if self.max_truncation_err is not None else -1),
                                trimming_threshold = self.trimming_threshold,
                                dropout = self.dropout)


        elif any([filename.endswith(x) for x in [".h5",".hdf5"]]):
            import h5py
            hf = h5py.File(filename, "w")
            mps = hf.create_group("mps")
            for site in range(self.num_sites):
                mps.create_dataset(f"site_{site}", data=self[site], compression="gzip")
            meta = hf.create_group("meta")
            meta.create_dataset("num_sites",shape=(),dtype=int,data=self.num_sites)
            meta.create_dataset("center_of_orthogonality",shape=(),dtype=int,
                                data=self.center_position)
            meta.create_dataset("label_position",shape=(),dtype=int,data=self.label_position)
            msv = self.max_singular_values if self.max_singular_values is not None else -1
            meta.create_dataset("max_singular_values",shape=(),dtype=int,data=msv)
            mte = self.max_truncation_err if self.max_truncation_err is not None else -1
            meta.create_dataset("max_truncation_err",shape=(),dtype=float,data=mte)
            meta.create_dataset("trimming_threshold",shape=(),dtype=float, data=self.trimming_threshold)
            meta.create_dataset("dropout",shape=(),dtype=float, data=self.dropout)
            hf.close()

        else:
            raise InvalidFileFormat(f"Unknown file format: '{filename.split('.')[-1]}'")



    @classmethod
    def load_from_file(cls, filename: Text, dtype: tf.DType = tf.float64) -> None:
        """
        Load MPS tensor from a compressed numpy or hdf5 file where tensor is saved with
        "mps" keyword

        Parameters
        ----------
        filename : str
            name of the numpy file
        dtype : tf.DType, optional
            data type of the network
        """
        import numpy as np

        if filename.endswith(".npz"):
            data = np.load(filename, allow_pickle=True)
            tensors = [tf.convert_to_tensor(t, dtype=dtype) for t in data["mps"]]

            assert len(tensors) == int(data["num_sites"]), "Corrupted file."

            label_position = int(data["label_position"])
            center_of_orthogonality = int(data["center_of_orthogonality"])
            max_singular_values = (int(data["max_singular_values"]) \
                                       if data["max_singular_values"] > 0 else None)
            max_truncation_err = (float(data["max_truncation_err"]) \
                                      if data["max_truncation_err"] >= 0 else None)
            trimming_threshold = float(data["trimming_threshold"])
            dropout = float(data["dropout"])

        elif any([filename.endswith(x) for x in [".h5",".hdf5"]]):
            import h5py
            hf = h5py.File(filename,"r")
            meta = hf.get("meta")
            num_sites = int(np.array(meta.get("num_sites")))
            tensors = []
            mps = hf.get("mps")
            for site in range(num_sites):
                tensors.append(tf.convert_to_tensor(mps.get(f"site_{site}"), dtype=dtype))
            label_position = int(np.array(meta.get("label_position")))
            center_of_orthogonality = int(np.array(meta.get("center_of_orthogonality")))
            max_singular_values = int(np.array(meta.get("max_singular_values")))
            max_singular_values = max_singular_values if max_singular_values > 0 else None
            max_truncation_err = int(np.array(meta.get("max_truncation_err")))
            max_truncation_err = max_truncation_err if max_truncation_err >= 0 else None
            trimming_threshold = float(np.array(meta.get("trimming_threshold")))
            dropout = int(np.array(meta.get("dropout")))
            hf.close()

        mps = cls(tensors,
                  label_position,
                  max_truncation_err,
                  max_singular_values,
                  trimming_threshold,
                  dropout)
        mps._center_of_orthogonality_position = center_of_orthogonality
        return mps


    def to_json(self, filename: Text = None) -> Union[Dict, None]:
        """
        Save MPS as json file

        Parameters
        ----------
        filename : Text
            path and name of the file
        """
        mps = dict(
            bond_dim = int(self[0].shape[1]),
            phys_dim = int(self[0].shape[0]),
            nlabels = int(self.label_tensor.shape[-1]),
            nsites = int(self.num_sites),
            label_position=self.label_position,
            max_truncation_err = self.max_truncation_err,
            max_singular_values = self.max_singular_values,
            trimming_threshold = self.trimming_threshold,
            dropout = self.dropout,
        )

        if filename is None:
            return mps

        import json
        with open(filename,"w") as f:
            f.write(json.dumps(mps, indent=4))


    @property
    def label_position(self) -> int:
        """
        Accessor to label tensor position
        """
        return self._label_position

    @label_position.setter
    def label_position(self, site: int) -> None:
        """
        Shift the label tensor position to the site

        Parameters
        ----------
        site : int
            site that label tensor position to be shifted.
        """
        # TODO change this to QR decomposition
        if site < 0:
            site = 0
        elif site > len(self) -1:
            site = len(self) -1

        initLP = self.label_position
        if site > initLP:
            for n in range(initLP, site):
                if n == 0:
                    tensor = ncon([self[n], self[n+1]],
                                  [[-1,1,-4],[1,-2,-3]])
                    left, right = split_node_full_svd(tensor, [0], [1,2,3], direction="right")
                elif n + 1 == self.num_sites -1 :
                    tensor = ncon([self[n], self[n+1]],
                                  [[-1,-2,1,-4],[1,-3]])
                    left, right = split_node_full_svd(tensor, [0,1], [2,3], direction="right")
                else:
                    tensor = ncon([self[n], self[n+1]],
                                  [[-1,-2,1,-5],[1,-3,-4]])
                    left, right = split_node_full_svd(tensor, [0,1], [2,3,4], direction="right")

                self[n], self[n+1] = left, right / tf.norm(right)
                self._label_position += 1

        elif site < initLP:
            for n in reversed(range(site+1, initLP+1)):
                if n-1 == 0:
                    tensor = ncon([self[n-1], self[n]],
                                  [[-1,1],[1,-2,-3,-4]])
                    left, right = split_node_full_svd(tensor, [0,3], [1,2], direction="left")
                    left = transpose(left, (0,2,1))
                elif n == self.num_sites - 1 :
                    tensor = ncon([self[n-1], self[n]],
                                  [[-1,-2,1],[1,-3,-4]])
                    left, right = split_node_full_svd(tensor, [0,1,3], [2], direction="left")
                    left = transpose(left, (0,1,3,2))
                else:
                    tensor = ncon([self[n-1], self[n]],
                                  [[-1,-2,1],[1,-3,-4,-5]])
                    left, right = split_node_full_svd(tensor, [0,1,4], [2,3], direction="left")
                    left = transpose(left, (0,1,3,2))

                self[n-1], self[n] = left / tf.norm(left), right
                self._label_position -= 1

    # TODO change label shift to QR based instead of SVD base
    def shift_label(self, site):
        if site < 0:
            site = 0
        elif site > len(self) -1:
            site = len(self) -1

        initLP = self.label_position
        if site > initLP:
            for n in range(initLP, min(site,len(self) -1)):

                indices = [n for n in range(len(self[n].shape))]
                Q, R = split_node_qr(self[n], indices[:-2], indices[-2:])

                self[n] = Q
                nodeR = tn.Node(R)
                nextTensor = tn.Node(self[n+1])
                nodeR.edges[-1] ^ nextTensor[0]
                self[n+1] = (nodeR @ nextTensor).tensor

                self[n+1] /= tf.norm(self[n+1])

                self._label_position = n+1

        elif site < initLP:
            for n in reversed(range(site+1, initLP + 1)):
                indices = [n for n in range(len(self[n].shape))]

                R, Q = split_node_rq(self[n], [0], [n for n in range(1,len(self[n].shape))])

    @property
    def label_tensor(self) -> tf.Tensor:
        """
        Accessor to label tensor
        """
        return self[self.label_position]


    @property
    def center_position(self):
        """
        position of center of orthogonality
        """
        return self._center_of_orthogonality_position

    @property
    def center_tensor(self) -> tf.Tensor:
        return self[self.center_position]


    # SVD - Based 2site center shift -> too expensive not necessary
    # @center_position.setter
    # def center_position(self, site: int) -> None:
    #     """
    #     Shift the center of orthogonality site of the MPS to `site`
    #
    #     Parameters
    #     ----------
    #     site : int
    #         Site that center to be shifted.
    #     """
    #
    #     if site < 0:
    #         site = 0
    #     elif site > len(self) -1:
    #         site = len(self) -1
    #
    #     initialCoO = self.center_position
    #     if site > initialCoO:
    #         for n in range(initialCoO, min(site,len(self) -1)):
    #             if self.label_position not in (n, n+1):
    #                 if n == 0:
    #                     current_tensor = ncon([self[n],self[n+1]], [[-1,1],[1,-2,-3]])
    #                     left, right = split_node_full_svd(current_tensor, [0],[1,2], "right")
    #                 elif n+1 == len(self) -1:
    #                     current_tensor = ncon([self[n],self[n+1]], [[-1,-2,1],[1,-3]])
    #                     left, right = split_node_full_svd(current_tensor, [0,1],[2], "right")
    #                 else:
    #                     current_tensor = ncon([self[n],self[n+1]], [[-1,-2,1],[1,-3,-4]])
    #                     try:
    #                         left, right = split_node_full_svd(current_tensor, [0,1],[2,3], "right")
    #                     except Exception:
    #                         # Warning I dont know what causing this, everything seems to be fine
    #                         left, right = self[n], self[n+1]
    #             else:
    #                 if self.label_position == n:
    #                     if n == 0:
    #                         current_tensor = ncon([self[n],self[n+1]], [[-1,1,-4],[1,-2,-3]])
    #                         left, right = split_node_full_svd(current_tensor, [0,3],[1,2], "right")
    #                         left, right = transpose(left, (0,2,1))
    #                     elif n+1 == len(self) - 1:
    #                         current_tensor = ncon([self[n],self[n+1]], [[-1,-2,1,-3],[1,-2]])
    #                         left, right = split_node_full_svd(current_tensor, [0,1,3], [2], "right")
    #                         left = transpose(left, (0,1,3,2))
    #                     else:
    #                         current_tensor = ncon([self[n],self[n+1]], [[-1,-2,1,-5],[1,-3,-4]])
    #                         left, right = split_node_full_svd(current_tensor, [0, 1, 4],[2, 3], "right")
    #                         left = transpose(left, (0,1,3,2))
    #                 elif self.label_position == n+1:
    #                     if n == 0:
    #                         current_tensor = ncon([self[n],self[n+1]], [[-1,1],[1,-2,-3,-4]])
    #                         left, right = split_node_full_svd(current_tensor, [0],[1,2,3], "right")
    #                     elif n+1 == len(self) - 1:
    #                         current_tensor = ncon([self[n],self[n+1]], [[-1,-2,1],[1,-3, -4]])
    #                         left, right = split_node_full_svd(current_tensor, [0,1],[2,3], "right")
    #                     else:
    #                         current_tensor = ncon([self[n],self[n+1]], [[-1,-2,1],[1,-3,-4,-5]])
    #                         left, right = split_node_full_svd(current_tensor, [0, 1],[2, 3,4], "right")
    #
    #             self[n], self[n+1] = left, right / tf.norm(right)
    #             self._center_of_orthogonality_position += 1
    #
    #     elif site < initialCoO:
    #         for n in reversed(range(site+1, initialCoO + 1)):
    #             if self.label_position not in (n, n-1):
    #                 if n-1 == 0:
    #                     current_tensor = ncon([self[n-1],self[n]], [[-1,1],[1,-2,-3]])
    #                     left, right = split_node_full_svd(current_tensor, [0],[1,2], "left")
    #                 elif n == len(self) - 1:
    #                     current_tensor = ncon([self[n-1],self[n]], [[-1,-2,1],[1,-3]])
    #                     left, right = split_node_full_svd(current_tensor, [0,1],[2], "left")
    #                 else:
    #                     current_tensor = ncon([self[n-1],self[n]], [[-1,-2,1],[1,-3,-4]])
    #                     try:
    #                         left, right = split_node_full_svd(current_tensor, [0,1],[2,3], "left")
    #                     except Exception:
    #                         # Warning I dont know what causing this, everything seems to be fine
    #                         left, right = self[n-1], self[n]
    #             else:
    #                 if self.label_position == n - 1:
    #                     if n - 1 == 0:
    #                         current_tensor = ncon([self[n-1],self[n]], [[-1,1,-4],[1,-2,-3]])
    #                         left, right = split_node_full_svd(current_tensor, [0,3],[1,2], "left")
    #                         left = transpose(left, (0,2,1))
    #                     elif n == len(self) - 1:
    #                         current_tensor = ncon([self[n-1],self[n]], [[-1,-2,1,-3],[1,-2]])
    #                         left, right = split_node_full_svd(current_tensor, [0,1,3], [2], "left")
    #                         left = transpose(left, (0,1,3,2))
    #                     else:
    #                         current_tensor = ncon([self[n-1],self[n]], [[-1,-2,1,-5],[1,-3,-4]])
    #                         left, right = split_node_full_svd(current_tensor, [0, 1, 4],[2, 3], "left")
    #                         left = transpose(left, (0,1,3,2))
    #                 elif self.label_position == n:
    #                     if n-1 == 0:
    #                         current_tensor = ncon([self[n-1],self[n]], [[-1,1],[1,-2,-3,-4]])
    #                         left, right = split_node_full_svd(current_tensor, [0],[1,2,3], "left")
    #                     elif n == len(self) - 1:
    #                         current_tensor = ncon([self[n-1],self[n]], [[-1,-2,1],[1,-3, -4]])
    #                         left, right = split_node_full_svd(current_tensor, [0,1],[2,3], "left")
    #                     else:
    #                         current_tensor = ncon([self[n-1],self[n]], [[-1,-2,1],[1,-3,-4,-5]])
    #                         left, right = split_node_full_svd(current_tensor, [0, 1],[2, 3,4], "left")
    #
    #             self[n-1], self[n] = left  / tf.norm(left), right
    #             self._center_of_orthogonality_position -= 1


    @center_position.setter
    def center_position(self, site: int) -> None:
        """
        Shift the center of orthogonality site of the MPS to `site`

        Parameters
        ----------
        site : int
            Site that center to be shifted.
        """
        if site < 0:
            site = 0

        initialCoO = self.center_position
        if site > initialCoO:
            for n in range(initialCoO, min(site,len(self) -1)):

                Q, R = split_node_qr(
                    self[n],
                    [x for x in range(len(self[n].shape)-1)],
                    [max([x for x in range(len(self[n].shape))])]
                )

                self[n] = Q
                nodeR = tn.Node(R)
                nextTensor = tn.Node(self[n+1])
                nodeR.edges[-1] ^ nextTensor[0]
                self[n+1] = (nodeR @ nextTensor).tensor
                # for an mps with > O(10) sites one needs to normalize to avoid
                # over or underflow errors; this takes care of the normalization
                self[n+1] /= tf.norm(self[n+1])

                self._center_of_orthogonality_position = n+1

        elif site < initialCoO:
            for n in reversed(range(site+1, initialCoO + 1)):

                R, Q = split_node_rq(self[n], [0], [x for x in range(1,len(self[n].shape))])
                self[n] = Q
                nodeR = tn.Node(R)
                nextTensor = tn.Node(self[n-1])
                nextTensor.edges[-1] ^ nodeR[0]
                contractedNext = (nextTensor @ nodeR).tensor
                contractedNext /= tf.norm(contractedNext)

                self[n-1] = contractedNext
                self._center_of_orthogonality_position = n-1


    def center_position_svd(
            self,
            site: int,
            max_truncation_err: float = None,
            max_singular_values: int = None,
            trimming_threshold: float = 0.0,
        ) -> None:
        """
        Shift the center of orthogonality site of the MPS to `site`

        Parameters
        ----------
        site : int
            site that center to be shifted
        max_truncation_err : float
            max truncation error for SVD, default is none
        max_singular_values : int
            number of singular values for svd, default is none
        trimming_threshold : float
            min value for singular values, default is zero
        """
        if site < 0:
            site = 0

        if max_singular_values is None:
            max_singular_values = -1
            for ix, tensor in enumerate(self):
                if ix == 0:
                    if tensor.shape[-1] > max_singular_values:
                        max_singular_values = tensor.shape[-1]
                elif ix < len(self) - 1:
                    if max(tensor.shape[-1], tensor.shape[0]) > max_singular_values:
                        max_singular_values = max(tensor.shape[-1], tensor.shape[0])
                else:
                    if tensor.shape[0] > max_singular_values:
                        max_singular_values = tensor.shape[0]


        initialCoO = self.center_position
        if site > initialCoO:
            for n in range(initialCoO, min(site,len(self) -1)):

                left, right = split_node_full_svd(
                    self[n],
                    [x for x in range(len(self[n].shape)-1)],
                    [max([x for x in range(len(self[n].shape))])],
                    direction = "right",
                    max_singular_values = max_singular_values,
                    max_truncation_err = max_truncation_err,
                    trimming_threshold = trimming_threshold,
                )

                self[n] = left
                nodeR = tn.Node(right)
                nextTensor = tn.Node(self[n+1])
                nodeR.edges[-1] ^ nextTensor[0]
                self[n+1] = (nodeR @ nextTensor).tensor
                # for an mps with > O(10) sites one needs to normalize to avoid
                # over or underflow errors; this takes care of the normalization
                self[n+1] /= tf.norm(self[n+1])

                self._center_of_orthogonality_position = n+1

        elif site < initialCoO:
            for n in reversed(range(site+1, initialCoO + 1)):

                left, right = split_node_full_svd(
                    self[n],
                    [0], [x for x in range(1,len(self[n].shape))],
                    direction = "left",
                    max_singular_values = max_singular_values,
                    max_truncation_err = max_truncation_err,
                    trimming_threshold = trimming_threshold,
                )
                # print(f"left {left.shape}, right {right.shape}, site {n-1} {self[n-1].shape}")
                self[n] = right
                nodeR = tn.Node(left)
                nextTensor = tn.Node(self[n-1])
                nextTensor.edges[-1] ^ nodeR[0]
                contractedNext = (nextTensor @ nodeR).tensor
                contractedNext /= tf.norm(contractedNext)

                self[n-1] = contractedNext
                self._center_of_orthogonality_position = n-1


    def compressed_canonical(
            self,
            max_truncation_err: float = None,
            max_singular_values: int = None,
            trimming_threshold: float = 0.0,
    ) -> None:
        self.center_position_svd(
            len(self) - 1, max_truncation_err, max_singular_values, trimming_threshold
        )
        self.center_position_svd(
            0, max_truncation_err, max_singular_values, trimming_threshold
        )


    def check_orthonormality(self, site: int, direction: Union[Text, int]) -> tf.Tensor:

        assert direction in ("r","right",1,-1,"left","l"), f"Unknown direction: {direction}"

        node = tn.Node(self[site])
        conj = tn.linalg.node_linalg.conj(node)

        if direction in ("r","right",1):
            assert site != 0
            node[1] ^ conj[1]
            n, m = node.shape[0], conj.shape[0]
            if site != len(self)-1:
                node[2] ^ conj[2]
                if site == self.label_position and site != len(self)-1:
                    node[3] ^ conj[3]
                elif site == self.label_position and site == len(self)-1:
                    node[2] ^ conj[2]

        elif direction in (-1,"left","l"):
            assert site != len(self) - 1
            node[0] ^ conj[0]
            if site != 0:
                node[1] ^ conj[1]
                n, m = node.shape[2], conj.shape[2]
                if site == self.label_position:
                    node[3] ^ conj[3]
            elif site == 0:
                n, m = node.shape[1], conj.shape[1]
                if site == self.label_position:
                    node[2] ^ conj[2]

        result = node @ conj
        return tf.norm(abs(result.tensor - tn.eye(N=n, M=m, dtype=self.dtype).array))


    def check_canonical(self) -> tf.Tensor:
        deviations = []
        for site in range(len(self)):
            if site < self.center_position:
                deviation = self.check_orthonormality(site, "left")
            elif site > self.center_position:
                deviation = self.check_orthonormality(site, "right")
            else:
                continue
            deviations.append(deviation**2)
        return tf.sqrt(sum(deviations))


    def canonicalize(self) -> None:
        self.center_position = len(self) - 1
        self.center_position = 0


    def get_densitymatrix(self, site: int, operator: tf.Tensor = None, label: int = 0):
        """
        Calculate the density matrix at given site

        Parameters
        ----------
        site : int
            Site to calculate the density matrix
        operator : tf.Tensor
            Square matrix (p_i x p_i), generator of a group.

        Returns
        -------
        tf.Tensor
        """
        if site not in (self.label_position, 0, len(self)-1):
            if operator is None:
                rho = ncon([self[site], tf.transpose(self[site], conjugate = True)],
                           [[-1,1,-3],[-4,1,-2]])
            else:
                rho = ncon([self[site], operator, tf.transpose(self[site], conjugate = True)],
                           [[-1,1,-3],[1,2],[-4,2,-2]])

        elif site not in (0, len(self) - 1) and site == self.label_position:
            if operator is None:
                rho = ncon([
                    self[site][:,:,label,:],
                    tf.transpose(self[site][:,:,label,:], conjugate = True),
                ],
                    [[-1,1,-3],[-4,1,-2]])
            else:
                rho = ncon([
                    self[site][:,:,label,:],
                    operator,
                    tf.transpose(self[site][:,:,label,:], conjugate = True)
                ],
                    [[-1,1,-3], [1,2], [-4,2,-2]])

        elif site in (0, len(self) - 1):
            if site == 0:
                if self.label_position != 0:
                    if operator is None:
                        rho = ncon([self[0], tf.transpose(self[0], conjugate = True)],
                                   [[1,-1],[-2,1]])
                    else:
                        rho = ncon([self[0], operator, tf.transpose(self[0], conjugate = True)],
                                   [[1,-1], [1,2], [-2,2]])
                else:
                    if operator is None:
                        rho = ncon([
                            self[0][:,label,:],
                            tf.transpose(self[0][:,label,:], conjugate = True)
                        ],
                            [[1,-1],[-2,1]])
                    else:
                        rho = ncon([
                            self[0][:,label,:],
                            operator,
                            tf.transpose(self[0][:,label,:], conjugate = True)
                        ],
                            [[1,-1], [1,2], [-2,2]])
            else:
                if self.label_position != site:
                    if operator is None:
                        rho = ncon([self[site], tf.transpose(self[site], conjugate = True)],
                                   [[-1,1],[1,-2]])
                    else:
                        rho = ncon(
                            [self[site], operator, tf.transpose(self[site], conjugate = True)],
                            [[-1,1], [1,2], [2,-2]]
                        )
                else:
                    if operator is None:
                        rho = ncon([
                            self[site][:,:,label],
                            tf.transpose(self[site][:,:,label], conjugate = True)
                        ],
                            [[-1,1],[1,-2]])
                    else:
                        rho = ncon([
                            self[site][:,:,label],
                            operator,
                            tf.transpose(self[site][:,:,label], conjugate = True),
                        ],
                            [[-1,1], [1,2], [2,-2]],
                        )

        return rho


    def norm(self, full = False):
        """
        Calculate the norm of the matrix product state

        Parameters
        ----------
        full : bool
            if true the norm will be calculated with the entire MPS, if not it will be calculated
            with respect to centre of orthogonality.

        Returns
        -------
        Scalar

        """
        nindices = self.label_indices

        if not full:
            init_center = self.center_position
            self.center_position = self.label_position

            labels = []
            for l in range(nindices):
                tensor = self.get_densitymatrix(self.center_position, label = l)
                shape = tensor.shape
                if len(shape) > 2:
                    tensor = tf.reshape(tensor, (shape[0]*shape[1],shape[2]*shape[3]))
                norm = ncon([tensor, tf.transpose(tensor, conjugate=True)], [[1,2],[2,1]])
                labels.append(norm)

            self.center_position = init_center
            return tf.sqrt(tf.stack(labels))

        else:
            labels = [1]*nindices
            for site in reversed(range(1, len(self))):
                if site == len(self) - 1:
                    for l in range(nindices):
                        labels[l] = self.get_densitymatrix(site, label = l)
                else:
                    for l in range(nindices):
                        labels[l] = ncon(
                            [self.get_densitymatrix(site, label = l), labels[l]],
                            [[-1,-2,1,2],[1,2]]
                        )

            return tf.sqrt(tf.stack(
                [
                    ncon([self.get_densitymatrix(0, label = l), labels[l]],[[1,2],[1,2]])
                    for l in range(nindices)
                ]))


    def get_expectation(self, operator: Dict = {}):
        """
        Expectation value of the MPS with respect to given operators. Operators must be square
        matrices with (p_i x p_i)

        Parameters
        ----------
        operator : dict
            {site : operator}

        Returns
        -------
        tf.Tensor
            Expectation value with respect to given operator(s)
        """
        assert isinstance(operator, dict), \
            f"Operator needs to be a dictionary not {type(operator)}"

        if operator == {}:
            return tf.square(self.norm(1))

        nindices = self.label_indices

        labels = [1]*nindices
        for site in reversed(range(1, len(self))):
            if site == len(self) - 1:
                for l in range(nindices):
                    labels[l] = self.get_densitymatrix(
                        site, operator = operator.get(site, None), label = l
                    )
            else:
                for l in range(nindices):
                    labels[l] = ncon([
                        self.get_densitymatrix(
                            site, operator = operator.get(site, None), label = l
                        ),
                        labels[l]
                    ],
                        [[-1,-2,1,2],[1,2]])

        return tf.stack(
            [
                ncon([
                    self.get_densitymatrix(0, operator = operator.get(site, None), label = l),
                    labels[l]
                ],
                    [[1,2],[1,2]]) for l in range(nindices)
            ])


    @property
    def entanglement_entropy(self) -> List[float]:
        """
        Get left and right entanglemetn entropy via von Neumann formulae

        Entropy = \sum -\rho_A log(\rho_A)

        Returns
        -------
        List[float]
        """
        initial_center_pos = self.center_position
        entanglement_entropy = []
        s_vals = []
        for site in range(self.num_sites - 1):
            self.center_position = site
            # get right link
            indices = [n for n in range(len(self[site].shape))]
            u, s, v = split_node_full_svd(self[site], indices[:-1],[max(indices)])
            singular_values = tf.linalg.diag_part(s)
            s_vals.append(singular_values.numpy())
            rhoA = tf.square(singular_values)
            entropy = 0.0
            for si in rhoA.numpy():
                if si > 1e-12:
                    entropy += - si * np.log(si)
            entanglement_entropy.append(entropy)
            # entanglement_entropy.append(
            #     tf.reduce_sum(-rhoA * tf.experimental.numpy.log2(rhoA)).numpy()
            # )

        self.center_position = initial_center_pos
        return entanglement_entropy, s_vals


    def entanglement_densitymatrix(self, additive = False, square = False, density_diag = False):
        """
        Calculate the von Neumann entropy with respect to local density matrix
        Parameters
        ----------
        additive : bool
            contract the density matrices from left to right and then calculate the entropy
        square : bool
            take the square of the diagonal terms
        density_diag : bool
            use diagonal elements of density matrix instead of singular values

        Returns
        -------
        List of entropies and singular values
        """
        initial_center_pos = self.center_position
        s_vals, entanglement_entropy = [], []
        for site in range(self.num_sites - 1):
            self.center_position = site
            if site == 0:
                tensor = self.get_densitymatrix(site)
            else:
                if additive:
                    tensor = ncon([tensor, self.get_densitymatrix(site)], [[1,2],[1,2,-1,-2]])
                else:
                    tensor = self.get_densitymatrix(site)
                    tshape = tensor.shape
                    tensor = tf.reshape(tensor, (tshape[0]*tshape[1],tshape[2]*tshape[3]))
            if not density_diag:
                u, s, v = split_node_full_svd(tensor, [0],[1])
                singular_values = tf.linalg.diag_part(s)
                s_vals.append(singular_values.numpy())
            else:
                singular_values = tf.linalg.diag_part(tensor)
                s_vals.append(singular_values.numpy())
            rhoA = singular_values if not square else tf.square(singular_values)
            entropy = 0.0
            for si in rhoA.numpy():
                if si > 1e-12:
                    entropy += - si * np.log(si)
            entanglement_entropy.append(entropy)

        self.center_position = initial_center_pos
        return entanglement_entropy, s_vals

    @property
    def left_entanglement_entropy(self) -> List[float]:
        """
        Get left and right entanglemetn entropy via von Neumann formulae

        Entropy = \sum -\rho_A log(\rho_A)

        Returns
        -------
        List[float]
        """
        initial_center_pos = self.center_position
        entanglement_entropy = []

        for site in range(1, self.num_sites):
            self.center_position = site
            # get right link
            indices = [n for n in range(len(self[site].shape))]
            u, s, v = split_node_full_svd(self[site], [0],indices[1:])

            rhoA = tf.square(tf.linalg.diag_part(s))
            entanglement_entropy.append(
                tf.reduce_sum(-rhoA * tf.experimental.numpy.log2(rhoA)).numpy()
            )

        self.center_position = initial_center_pos
        return entanglement_entropy