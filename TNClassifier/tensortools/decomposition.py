import tensorflow    as tf
import tensornetwork as tn
import numpy         as np

tf.get_logger().setLevel("ERROR")
tn.set_default_backend("tensorflow")

from typing import List, Union, Tuple, Text, Optional



@tf.function
def split_node_full_svd(
        tensor: tf.Tensor,
        left_edges: List[int],
        right_edges: List[int],
        direction: Optional[Union[int, Text, None]] = None,
        max_singular_values: Optional[Union[int, None]] = None,
        max_truncation_err: Optional[Union[float, None]] = None,
        trimming_threshold: Optional[float] = 0.0,
        dropout: Optional[float] = 0.0,
) -> Union[Tuple[tf.Tensor, tf.Tensor], Tuple[tf.Tensor, tf.Tensor, tf.Tensor]]:
    """
    Singular value decomposition with respect to left and right edges.

    Let M be the matrix created by flattening left_edges and right_edges into
    2 axes. Let :math:`U S V^* = M` be the Singular Value Decomposition of
    :math:`M`.

    The left most node will be :math:`U` tensor of the SVD, the middle node is
    the diagonal matrix of the singular values, ordered largest to smallest,
    and the right most node will be the :math:`V*` tensor of the SVD.

    The singular value decomposition is truncated if `max_singular_values` or
    `max_truncation_err` is not `None`.

    The truncation error is the 2-norm of the vector of truncated singular
    values. If only `max_truncation_err` is set, as many singular values will
    be truncated as possible while maintaining:
    `norm(truncated_singular_values) <= max_truncation_err`.

    If only `max_singular_values` is set, the number of singular values kept
    will be `min(max_singular_values, number_of_singular_values)`, so that
    `max(0, number_of_singular_values - max_singular_values)` are truncated.

    If both `max_truncation_err` and `max_singular_values` are set,
    `max_singular_values` takes priority: The truncation error may be larger
    than `max_truncation_err` if required to satisfy `max_singular_values`.

    Parameters
    ----------
    tensor: tf.Tensor
        tensor to be decomposed
    left_edges: List[int]
        list of left edge indices
    right_edges: List[int]
        list of right edge indices
    direction: Union[int, str, None], optional
        direction of the decomposition if left U-S will be merged,
        if right U-V will be merged, if none U,S,V will be returned
    max_singular_values: Union[int, None], optional
        maximum number of singular values
    max_truncation_err: Union[float, None], optional
        max truncation error
    trimming_threshold: Union[float,None], optional
        Acceptance threshold for the singular values.
        The singular values below this threshold will be truncated. trimming_threshold > 0

    Returns
    -------
    Tuple[tf.Tensor]
        Depending on the direction of execution 3 or 2 tensor will be returned
    """
    assert direction in (1, -1, "l", "r", "left", "right") or direction is None, \
        f"Unknown direction {direction}"

    node = tn.Node(tensor)
    left_node_edges  = [node[edge] for edge in left_edges ]
    right_node_edges = [node[edge] for edge in right_edges]

    u, s, v, _ = tn.split_node_full_svd(
        node,
        left_node_edges,
        right_node_edges,
        max_singular_values = max_singular_values,
        max_truncation_err = max_truncation_err
    )

    # Setting singular values below threshold to zero is
    # much faster than shrinking the tensor
    # s.tensor = tf.where(tf.greater_equal(s.tensor, trimming_threshold), s.tensor, 0.)
    mask  = tf.greater_equal(tf.linalg.diag_part(s.tensor), trimming_threshold)
    new_s = tf.linalg.diag(tf.boolean_mask(tf.linalg.diag_part(s.tensor), mask))

    s.tensor = new_s

    ix = 0
    for i in tf.linalg.diag_part(new_s):
        ix += 1

    u_shape = [slice(None,None) for _ in range(len(u.tensor.shape)-1)] + [slice(None,ix)]
    u.tensor = u.tensor[u_shape]
    v_shape = [slice(None,ix)] + [slice(None,None) for _ in range(len(v.tensor.shape)-1)]
    v.tensor = v.tensor[v_shape]

    if direction in (1,"r","right"):
        s.tensor /= tf.norm(s.tensor)
        return u.tensor, (s @ v).tensor
    elif direction in (-1,"l","left"):
        s.tensor /= tf.norm(s.tensor)
        return (u @ s).tensor, v.tensor

    return u.tensor, s.tensor, v.tensor


@tf.function
def split_node_qr(
        tensor: tf.Tensor,
        left_edges: List[int],
        right_edges: List[int],
) -> Tuple[tf.Tensor, tf.Tensor]:

    node = tn.Node(tensor)
    left_node_edges  = [node[edge] for edge in left_edges ]
    right_node_edges = [node[edge] for edge in right_edges]

    q, r = tn.split_node_qr(node, left_node_edges, right_node_edges)

    return q.tensor, r.tensor


@tf.function
def split_node_rq(
        tensor: tf.Tensor,
        left_edges: List[int],
        right_edges: List[int],
) -> Tuple[tf.Tensor, tf.Tensor]:

    node = tn.Node(tensor)
    left_node_edges  = [node[edge] for edge in left_edges ]
    right_node_edges = [node[edge] for edge in right_edges]

    r, q = tn.split_node_rq(node, left_node_edges, right_node_edges)

    return r.tensor, q.tensor


def split_node_full_svd_numpy(
        tensor: Union[tf.Tensor],
        left_edges: List[int],
        right_edges: List[int],
        direction: Optional[Union[int, Text, None]] = None,
        max_singular_values: Optional[Union[int, None]] = None,
        max_truncation_err: Optional[Union[float, None]] = None,
        trimming_threshold: Optional[float] = 0.,
) -> Union[Tuple[tf.Tensor, tf.Tensor], Tuple[tf.Tensor, tf.Tensor, tf.Tensor]]:
    """
    Numpy version of Singular value decomposition with respect to left and right edges.

    Let M be the matrix created by flattening left_edges and right_edges into
    2 axes. Let :math:`U S V^* = M` be the Singular Value Decomposition of
    :math:`M`.

    The left most node will be :math:`U` tensor of the SVD, the middle node is
    the diagonal matrix of the singular values, ordered largest to smallest,
    and the right most node will be the :math:`V*` tensor of the SVD.

    The singular value decomposition is truncated if `max_singular_values` or
    `max_truncation_err` is not `None`.

    The truncation error is the 2-norm of the vector of truncated singular
    values. If only `max_truncation_err` is set, as many singular values will
    be truncated as possible while maintaining:
    `norm(truncated_singular_values) <= max_truncation_err`.

    If only `max_singular_values` is set, the number of singular values kept
    will be `min(max_singular_values, number_of_singular_values)`, so that
    `max(0, number_of_singular_values - max_singular_values)` are truncated.

    If both `max_truncation_err` and `max_singular_values` are set,
    `max_singular_values` takes priority: The truncation error may be larger
    than `max_truncation_err` if required to satisfy `max_singular_values`.

    Parameters
    ----------
    tensor: Union[tf.Tensor]
        tensor to be decomposed
    left_edges: List[int]
        list of left edge indices
    right_edges: List[int]
        list of right edge indices
    direction: Union[int, str, None], optional
        direction of the decomposition if left U-S will be merged,
        if right U-V will be merged, if none U,S,V will be returned
    max_singular_values: Union[int, None], optional
        maximum number of singular values
    max_truncation_err: Union[float, None], optional
        max truncation error
    trimming_threshold: Union[float,None], optional
        Acceptance threshold for the singular values.
        The singular values below this threshold will be truncated. trimming_threshold > 0

    Returns
    -------
    Tuple[tf.Tensor]
        Depending on the direction of execution 3 or 2 tensor will be returned
    """

    assert direction in (1, -1, "l", "r", "left", "right") or direction is None, \
        f"Unknown direction {direction}"

    if tf.is_tensor(tensor):
        node = tn.Node(tensor.numpy(), backend = "numpy")
    elif isinstance(tensor, np.ndarray):
        node = tn.Node(tensor, backend = "numpy")
    else:
        raise InvalidInput("Input tensor needs to be either `numpy.ndarray` or `tf.Tensor`")

    left_node_edges  = [node[edge] for edge in left_edges ]
    right_node_edges = [node[edge] for edge in right_edges]

    u, s, v, _ = tn.split_node_full_svd(
        node,
        left_node_edges,
        right_node_edges,
        max_singular_values = max_singular_values,
        max_truncation_err = max_truncation_err,
    )

    s.tensor = np.where(s.tensor >= trimming_threshold, s.tensor, 0.)

    if direction in (1, "r", "right"):
        return tf.convert_to_tensor(u.tensor, dtype=tensor.dtype), \
               tf.convert_to_tensor((s @ v).tensor, dtype=tensor.dtype)

    elif direction in (-1, "l", "left"):
        return tf.convert_to_tensor((u @ s).tensor, dtype=tensor.dtype), \
               tf.convert_to_tensor(v.tensor, dtype=tensor.dtype)

    return tf.convert_to_tensor(u.tensor, dtype=tensor.dtype), \
           tf.convert_to_tensor(s.tensor, dtype=tensor.dtype), \
           tf.convert_to_tensor(v.tensor, dtype=tensor.dtype)
