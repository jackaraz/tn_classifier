import tensorflow    as tf
import tensornetwork as tn
import numpy         as np
from typing import List, Union, Tuple

tf.get_logger().setLevel("ERROR")
tn.set_default_backend("tensorflow")

from functools import partial


_partial_ncon = partial(tn.ncon, backend = "tensorflow", check_network = False)
ncon = tf.function(_partial_ncon)


transpose = tf.function(tf.transpose)


@tf.function
def is_nan(tensor: Union[List[tf.Tensor], tf.Tensor]) -> tf.Tensor:
    return tf.reduce_any(tf.math.is_nan(tensor))


@tf.function
def normalize(tensor: tf.Tensor, with_respect_to: int = 0):
    axis = [i for i in range(len(tensor.shape)) if i != with_respect_to]
    normalized_tensor, norm =  tf.linalg.normalize(
        tensor,
        axis= axis if len(axis) > 1 else axis[0]
    )
    return normalized_tensor, norm

