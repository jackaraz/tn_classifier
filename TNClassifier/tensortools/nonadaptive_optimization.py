import tensorflow    as tf
import tensornetwork as tn

from typing import List, Callable, Optional


def nonadaptive_optimization_step(
        samples: tf.Tensor,
        labels: tf.Tensor,
        mps: List[tf.Variable],
        loss: Callable,
        optimizer: tf.optimizers.Optimizer,
        label_position: int,
        activation: Optional[Callable] = lambda x: x,
):
    """
    Non-Adaptive optimization step for Matrix Product States. Calculates the full contraction
    of the MPS with respect to the samples, updates the network wrt the loss function.

    Parameters
    ----------
    samples : tf.Tensor
        samples to train on
    labels : tf.Tensor
        labels of the tensors
    mps : List[tf.Variable]
        MPS tensors in tf.Variable format
    loss : Callable
        loss function
    optimizer : tf.optimizers.Optimizer
        optimizer e.g. Adam
    label_position : int
        position of the label tensor
    activation : Optional[Callable]
        activation function, default is `lambda x: x`

    Returns
    -------
    tf.Tensor, List[tf.Tensor]
        loss value and updated MPS tensors
    """


    def calcEnviron(sample: tf.Tensor, network: List[tf.Tensor], label_position: int) -> tf.Tensor:
        for site, tensor in enumerate(network):
            if label_position > site:
                if site == 0:
                    left_env = lambda : tn.ncon([tensor, sample[:,0]], [[1,-1],[1]])
                else:
                    left_env = lambda : tn.ncon([left_env, tensor, sample[:,site]],
                                                [[1],[1,2,-1],[2]])
                left_env = left_env()
            elif label_position < site:
                if site == len(network) - 1:
                    # Label index is always at the very right
                    left_env = tn.ncon([left_env, tensor, sample[:,site]],
                                       [[1,-1],[1,2],[2]])
                else:
                    # Label index is always at the very right
                    left_env = tn.ncon([left_env, tensor, sample[:,site]],
                                       [[1,-2],[1,2,-1],[2]])

            elif label_position == site:
                if site == 0:
                    # Label index is always at the very right
                    left_env = tn.ncon([tensor, sample[:,0]], [[1,-1,-2],[1]])
                elif site == len(network) - 1:
                    left_env = tn.ncon([left_env, tensor, sample[:,site]],
                                       [[1],[1,2,-1],[2]])
                else:
                    left_env = tn.ncon([left_env, tensor, sample[:,site]],
                                       [[1],[1,2,-1,-2],[2]])
            left_env /= tf.norm(left_env)

        return left_env



    with tf.GradientTape() as tape:
        tape.watch(samples)
        tape.watch(mps)
        yhat = activation(
            tf.vectorized_map(lambda vec: calcEnviron(vec, mps, label_position), samples)
        )
        loss = loss(labels, yhat)

    gradients = tape.gradient(loss, mps)
    optimizer.apply_gradients(zip(gradients, mps))

    return loss, yhat
