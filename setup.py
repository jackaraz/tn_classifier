from setuptools import setup

with open("README.md", "r", encoding="utf-8") as f:
    long_description = f.read()

with open("requirements.txt", "r") as f:
    requirements = f.read()
requirements = [x for x in requirements.split("\n") if x != ""]

#import os, sys
#sys.path.append(os.path.dirname(os.path.realpath( __file__ )))
#from TNClassifier import __version__

setup(
    name="TNClassifier",
    version="0.0.1",
    description=("A Tensor Network based classifier"),
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/jackaraz/tn_classifier",
    author="J. Y. Araz, M. Spannowsky",
    author_email=("jack.araz@durham.ac.uk, michael.spannowsky@durham.ac.uk"),
    license="MIT",
    packages=[
        "TNClassifier",
        "TNClassifier.MPS",
        "TNClassifier.system",
        "TNClassifier.tensortools"
    ],
    install_requires=requirements,
    python_requires=">=3.6",
    classifiers=[
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
        "Topic :: Scientific/Engineering :: Physics",
    ],
)
