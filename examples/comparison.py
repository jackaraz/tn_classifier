#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json, os, argparse
from glob import glob as glb

import pandas as pd
import numpy  as np

# ================================= #
# ==== Set the Input Arguments ==== #
# ================================= #

class SplitArgs(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, [int(x) for x in values.split(',')])

parser = argparse.ArgumentParser(
    description="Run specific network architecture and save weights, "
                "training history into a designated folder."
)


# =============== #
# ==== Model ==== #
# =============== #

architectures = ["cnn","fcnn","rnn"]

modelgroup = parser.add_argument_group("Model handling")

modelgroup.add_argument("MODEL", choices=architectures, type=str, help="Hypothesis for classification.")

modelgroup.add_argument("-id", "--name", default="Model", dest="name", type=str,
                        help="Name of the training")

# modelgroup.add_argument("-d","--bond-dim", default=10, dest="BOND", type=int, metavar="INT",
#                         help="Initial size of the bond dimensions between MPS tensors, default is 10. "+\
#                              "Only available for nonadaptive MPS")

modelgroup.add_argument("--n-labels",default=2,dest="NLABELS", type=int,
                        help="Number of labels of the classification 1 is binary, 2 is one-hot,"+\
                             " default is 2 (binary is not supported atm)", choices = [2, 10])

# modelgroup.add_argument("--label-pos", default="center", dest="LABELPOS", type=str,
#                         help="Position of the label tensor, default is center. Only available for nonadaptive MPS",
#                         choices = ["center","beginning","end"])

modelgroup.add_argument("--activation", default="softmax", choices=["relu", "softmax"], dest="ACTIVATION", type=str,
                        help="Activation function for the MPS, default is softmax.")

modelgroup.add_argument("--loss", default="categorical_crossentropy", type=str, dest="LOSS",
                        choices=["categorical_crossentropy", "binary_crossentropy","sparse_categorical_crossentropy"],
                        help="Loss function, default is categorical_crossentropy.")

modelgroup.add_argument("--n-dense", default=5, type=int, dest="NDENSE",
                        help="Number of dense layers. Only available for fcnn model. Default is 5.")

modelgroup.add_argument("--n-node", default=10, type=int, dest="NNODE",
                        help="Number of nodes per dense layer. Only available for fcnn model. Default is 10.")

modelgroup.add_argument("-dp", "--dropout", default=0.25, type=float, dest="DROPOUT",
                        help="Dropout probability. Only available for fcnn model. Default is 0.25.")

# ================== #
# ==== Training ==== #
# ================== #

traingroup = parser.add_argument_group("Training handling")

traingroup.add_argument("-lr","--learning-rate", default=1e-2,dest="LR",type=float,
                        metavar = "FLOAT", help="Learning rate, default 1e-2")

traingroup.add_argument("--epochs", default=500, dest="EPOCHS",type=int,
                        metavar = "INT", help="Number of epochs to run during training, default is 500")

traingroup.add_argument("--queue", default=20, dest="QUEUE",type=int,
                        metavar = "INT", help="Max queue size, default is 20")

traingroup.add_argument("--patience", default=20, dest="PATIENCE",type=int,
                        metavar = "INT", help="patience to reduce the learning rate, default is 20")

traingroup.add_argument("--early-stop-patience", default=500//2, dest="EARLYSTOP",type=int,
                        metavar = "INT", help="patience for early stopping, default is 250")

# ================ #
# ==== Sample ==== #
# ================ #

samplegroup = parser.add_argument_group("Sample handling")


samplegroup.add_argument( "-tb", "--train-batch-size", type=int, dest="train_batch",
                          default=128, help="Batch size for the training sample, default is 128")

samplegroup.add_argument( "-vb", "--val-batch-size", type=int, dest="val_batch", default=128,
                          help="Batch size for the validation sample, default is 128")

samplegroup.add_argument("--slice", default=8, dest="SLICE", type=int, metavar="INT",
                         help="Throw X number of pixels from top, bottom, "
                              "left and right of the image, default is 8")

samplegroup.add_argument( "-nt", "--ntrain", type=int, dest="ntrain", default=122, metavar = "INT",
                          help="Number of files to be included during training, default is 122",
                          choices=list(range(1,123)))

samplegroup.add_argument( "--downsample", dest="DOWNSAMPLE", default=False, action="store_true",
                          help="Downsample the image by averaging every 4 square pixels. "
                               "With the default slice this reduces the number of sites to 100.")

samplegroup.add_argument( "--pad", dest="PAD", default=False, action="store_true",
                          help="Pad before downsampling")

samplegroup.add_argument("--rnn-normalize-by", default=None, type=float, dest="RNNNORM",
                         help="Instead of using robust scaler, normalize the RNN sequence")
samplegroup.add_argument("--rnn-cutoff", default=1., type=float, dest="RNNCUTOFF",
                         help="Cut off for the values in RNN sequence.")
samplegroup.add_argument("--rnn-sequence-lim", default=[2,10,6,6], dest="RNNSEQLIM", action=SplitArgs,
                         help="Reduce the number of elements in RNN sequence. First term is for jet masses,"
                              "and following 3 term is for leading, second leading and third "
                              "leading branch respectively. "
                              "Values are splitted via comma without space. ")

# =============== #
# ==== Paths ==== #
# =============== #

paths = parser.add_argument_group("Paths")

paths.add_argument( "--data-path", type=str, dest="data_path",
                    default="/mt/user-batch/jaraz/RNN_CNN/EnsembleNN/samples_v2",
                    help="Location of the training, validation and test data.")

paths.add_argument("--out-path", type=str, dest="output_path", default="output",
                   help="Output location.")


args = parser.parse_args()

if args.name == "Model":
    args.name = args.MODEL

if not os.path.isdir(args.output_path):
    os.mkdir(args.output_path)
if not os.path.isdir(os.path.join(args.output_path, "comparison")):
    os.mkdir(os.path.join(args.output_path, "comparison"))

n_folder = [int(x.split('_')[-1])
            for x in glb(os.path.join(args.output_path,  "comparison", args.name+'_*'))]
n_folder = 0 if n_folder == [] else max(n_folder)+1
output_path = os.path.join(args.output_path, "comparison", args.name+'_'+str(n_folder))

if not os.path.isdir(output_path):
    os.mkdir(output_path)

log = {}
for arg, value in sorted(vars(args).items()):
    log[arg] = value

import json
with open(os.path.join(output_path,"arguments.log"),'w') as f:
    f.write(json.dumps(log, indent=4))

# ========================= #
# ==== Prepare Samples ==== #
# ========================= #

import TNClassifier as tnc
import tensorflow as tf

if not os.path.isdir(args.data_path):
    raise tnc.system.exceptions.InvalidPath(args.data_path)

sort = lambda x : int(x.split('_')[-1].split('.')[0])
if args.MODEL != "rnn":
    train_dict = {'bkg' : glb(args.data_path+'/train/QCD_CovNet_*')[:args.ntrain],
                  'sig' : glb(args.data_path+'/train/tt_CovNet_*')[:args.ntrain]}
    val_dict   = {'bkg' : glb(args.data_path+'/val/QCD_CovNet_*'),
                  'sig' : glb(args.data_path+'/val/tt_CovNet_*')}
else:
    train_dict = {'bkg' : glb(args.data_path+'/train/QCD_LSTM_*')[:args.ntrain],
                  'sig' : glb(args.data_path+'/train/tt_LSTM_*')[:args.ntrain]}
    val_dict   = {'bkg' : glb(args.data_path+'/val/QCD_LSTM_*'),
                  'sig' : glb(args.data_path+'/val/tt_LSTM_*')}

train_dict["sig"].sort(key=sort); train_dict["sig"] = train_dict["sig"][:args.ntrain]
train_dict["bkg"].sort(key=sort); train_dict["bkg"] = train_dict["bkg"][:args.ntrain]

label_format = "onehot" if args.NLABELS == 2 else "binary"

if args.MODEL != "rnn":
    training_samples = tnc.DataGeneratorKeras(
        train_dict,
        batch_size = args.train_batch,
        label_format = label_format,
        image_slice = args.SLICE,
        shuffle = True,
        downsample = args.DOWNSAMPLE,
        pad_downsample = args.PAD,
        flatten = (args.MODEL == "nonadaptive-mps"),
    )

    validation_samples = tnc.DataGeneratorKeras(
        val_dict,
        batch_size = args.val_batch,
        label_format = label_format,
        image_slice = args.SLICE,
        shuffle = True,
        downsample = args.DOWNSAMPLE,
        pad_downsample = args.PAD,
        flatten = (args.MODEL == "nonadaptive-mps"),
    )
else:
    if args.RNNNORM is None:
        class lstm_std:
            def __init__(self,train_dict):
                from sklearn.preprocessing import RobustScaler
                # minmaxscaler ruins the outliers which are crutial for kT distances
                X = np.zeros(sum(args.RNNSEQLIM))
                i=0
                while X.shape[0]<100000 and i<len(train_dict['sig']):
                    sig, bkg = list(zip(train_dict['sig'], train_dict['bkg']))[i]

                    data = np.vstack((np.load(sig)['sample'], np.load(bkg)['sample']))

                    mass                  = data[:,:2][:,:args.RNNSEQLIM[0]]
                    leading_branch        = data[:,2:20][:,:args.RNNSEQLIM[1]]
                    second_leading_branch = data[:,20:30][:,:args.RNNSEQLIM[2]]
                    third_leading_branch  = data[:,30:40][:,:args.RNNSEQLIM[3]]

                    mass                  = np.where(mass>args.RNNCUTOFF, mass, 0.)
                    leading_branch        = np.where(leading_branch>args.RNNCUTOFF, leading_branch, 0.)
                    second_leading_branch = np.where(
                        second_leading_branch>args.RNNCUTOFF, second_leading_branch, 0.
                    )
                    third_leading_branch  = np.where(
                        third_leading_branch>args.RNNCUTOFF, third_leading_branch, 0.
                    )

                    data = np.hstack(
                        (mass, leading_branch, second_leading_branch, third_leading_branch)
                    )

                    X = np.vstack((X,data))
                    i+=1
                X = X[1:]
                np.random.shuffle(X)
                self.transformer = RobustScaler().fit(X)
            def __call__(self,X):
                return self.transformer.transform(X)
        lstm_std = lstm_std(train_dict)
    else:
        lstm_std = lambda x: x/args.RNNNORM

    training_samples = tnc.DataGeneratorRNN(
        train_dict,
        batch_size = args.train_batch,
        shuffle = True,
        label_format = label_format,
        processor = None,
        standardize = lstm_std,
        cutoff = args.RNNCUTOFF,
        trim = args.RNNSEQLIM,
    )

    validation_samples = tnc.DataGeneratorRNN(
        val_dict,
        batch_size = args.val_batch,
        shuffle = True,
        label_format = label_format,
        processor = None,
        standardize = lstm_std,
        cutoff = args.RNNCUTOFF,
        trim = args.RNNSEQLIM,
    )

# ========================== #
# ==== Set Architecture ==== #
# ========================== #

if args.MODEL == "nonadaptive-mps":

    model = tf.keras.Sequential(
        [
            tf.keras.Input((2, training_samples.n_pixels), dtype=training_samples.dtype),
            tnc.MPSlayer(
                N_sites = training_samples.n_pixels,
                physical_dim = 2,
                bond_dim = args.BOND,
                n_labels = args.NLABELS,
                label_position = args.LABELPOS,
                activation=args.ACTIVATION,
                name="MPSlayer",
                dtype=training_samples.dtype,
            )
        ]
    )

elif args.MODEL == "cnn":
    from tensorflow.keras.layers       import Conv2D, MaxPooling2D, Flatten, Input
    from tensorflow.keras.layers       import Dense, BatchNormalization, Dropout

    model = tf.keras.Sequential(
        [
            Input(shape=training_samples.n_pixels, dtype=training_samples.dtype),
            Conv2D(8, (4, 4), activation='relu', padding='same'),
            BatchNormalization(),
            MaxPooling2D(pool_size=(2, 2)),
            Flatten(),
            Dense(16, activation='relu'),
            Dropout(0.25),
            Dense(args.NLABELS, activation=args.ACTIVATION, name='Final_Output')
        ]
    )

    open(output_path+'/model.json','w').write(json.dumps(model.to_json(),indent = 4))

elif args.MODEL == "rnn":
    from tensorflow.keras.layers import Dropout, LSTM, Input, Dense

    model = tf.keras.Sequential(
        [
            Input(shape=(training_samples.n_pixels,1), dtype=training_samples.dtype),
            LSTM(16, activation="tanh", recurrent_activation='sigmoid'),
            # Dense(64, activation='relu'),
            # Dropout(0.25),
            Dense(16, activation='relu'),
            Dropout(0.25),
            Dense(8, activation='relu'),
            Dropout(0.25),
            Dense(args.NLABELS, activation=args.ACTIVATION, name='Final_Output')
        ]
    )

    open(output_path+'/model.json','w').write(json.dumps(model.to_json(),indent = 4))

elif args.MODEL == "fcnn":
    from tensorflow.keras.layers import Dense, Dropout, Flatten

    layers = []
    for size in range(args.NDENSE):
        layers.append(Dense(args.NNODE, activation="relu"))
        layers.append(Dropout(args.DROPOUT))

    model = tf.keras.Sequential(
        [tf.keras.Input(shape=training_samples.n_pixels, dtype=training_samples.dtype), Flatten()]
        + layers
        + [Dense(args.NLABELS, activation=args.ACTIVATION)]
    )

    open(output_path+'/model.json','w').write(json.dumps(model.to_json(),indent = 4))


model.summary()

summary = []
write_summary = lambda x : summary.append(x+'\n')
model.summary(print_fn=write_summary)

summary.append(f"\n\nNumber of training samples used = {len(train_dict['bkg'])*2}\n")
if args.MODEL == "nonadaptive-mps":
    summary.append(f"Nsites {training_samples.n_pixels}, bond dim {args.BOND}, nlabel {args.NLABELS}\n\n")
summary_file = open(output_path+'/model_summary.txt','w')
summary_file.writelines(summary)
summary_file.close()

# ============================ #
# ==== Fit Neural Network ==== #
# ============================ #

from tensorflow.keras.callbacks import ( ReduceLROnPlateau, TerminateOnNaN,
                                         ModelCheckpoint, EarlyStopping )
from tensorflow.keras           import optimizers

AUC = tf.metrics.AUC(curve='ROC',
                     summation_method='interpolation',
                     name="auc",
                     multi_label=False)
Precision = tf.metrics.Precision(name = "precision")
KL = tf.metrics.KLDivergence(name='kld')

adam = optimizers.Adam(learning_rate=args.LR)

model.compile(loss=args.LOSS, optimizer=adam, metrics=['accuracy','mse', AUC, Precision, KL])

print('   * Training begins...')
try:
    model.fit(
        training_samples.on_epoch_end(),
        epochs=args.EPOCHS,
        verbose=1,
        validation_data = validation_samples.on_epoch_end(),
        workers = 4,
        # use_multiprocessing = True,
        validation_freq = 1,
        max_queue_size=args.QUEUE,#max_queue_size,  # defaults to 10
        callbacks = [
            ModelCheckpoint(output_path+'/model.h5',
                          monitor='val_loss', verbose=1, save_best_only=True),
            EarlyStopping(verbose=1, patience=args.EARLYSTOP, monitor='val_loss',
                        mode='min',min_delta=0.01),
            ReduceLROnPlateau(monitor='val_loss', factor=0.5,
                            patience=args.PATIENCE, verbose=1,
                            mode='min', min_delta=0.01,
                            cooldown=0, min_lr=1e-10),
            TerminateOnNaN()
        ]
    )
except KeyboardInterrupt:
    print('   * Training stopped by the user.')

training_samples.Reset()
validation_samples.Reset()

# save model history
model_history = pd.DataFrame.from_dict(model.history.history)
model_history.to_csv(output_path+'/model_history.csv.gz')


# ============================= #
# ==== Extract the results ==== #
# ============================= #

import matplotlib.pyplot as plt

for key, item in model.history.history.items():
    if 'val' not in key:
        legend = []
        fig = plt.figure(figsize=(8, 7), facecolor='w', edgecolor='k')
        plt.plot(item)
        legend.append(key)
        if 'lr' not in key:
            plt.plot(model.history.history['val_'+key])
            legend.append('val_'+key)
        plt.yscale('log')
        plt.title('Training History')
        plt.ylabel(key)
        plt.xlabel('epoch')
        plt.legend(legend, loc='best')
        plt.savefig(output_path+'/'+key+'.png', bbox_inches = 'tight')
        plt.close()

print('   * Output Path : '+ os.path.join(output_path))
