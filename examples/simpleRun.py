#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json, os, argparse
from glob import glob as glb

import pandas as pd


# ================================= #
# ==== Set the Input Arguments ==== #
# ================================= #

class SplitArgs(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, [int(x) for x in values.split(',')])


parser = argparse.ArgumentParser(
    description="Run specific network architecture and save weights, "
                "train history into a designated folder."
)

# =============== #
# ==== Model ==== #
# =============== #

modelgroup = parser.add_argument_group("Model handling")

modelgroup.add_argument("-id", "--name", default="Model", dest="name", type=str,
                        help="Name of the training")
modelgroup.add_argument("-d","--bond-dim", default=1, dest="BOND", type=int, metavar="INT",
                        help="Initial size of the bond dimensions between MPS tensors, "
                             "default is 1")
modelgroup.add_argument("--n-labels",default=2,dest="NLABELS", type=int,
                        help="Number of labels of the classification 1 is binary, 2 is one-hot,"
                             " default is 2 (binary is not supported atm)", choices = [2, 10])
modelgroup.add_argument("--label-pos", default="beginning", dest="LABELPOS", type=str,
                        help="Position of the label tensor, default is beginning",
                        choices = ["center","beginning","end"])
modelgroup.add_argument("--svd-err", default=None, dest="SVDERR", type=float, metavar="FLOAT",
                        help="Max truncation error for SVD")
modelgroup.add_argument("--svd-val", default=None, dest="SVDVAL", type=int, metavar="INT",
                        help="Max singular values for SVD")
modelgroup.add_argument("--trim-threshold", default=0.0, dest="TRIMTHRES", type=float,
                        metavar="FLOAT", help="Trimming threshold for SVD")
modelgroup.add_argument("--floating-label",default=True, action="store_false", dest="FIXEDLABEL",
                        help="Should optimization be done via fixed label tensor or floating one")
modelgroup.add_argument("--canonicalize",default=False, action="store_true", dest="CANONICALIZE",
                        help="Canonicalize the MPS before training.")
modelgroup.add_argument("--unravel-mode",default="reshape", dest="UNRAVELMODE", type = str,
                        choices = ["reshape", "s-shaped"],
                        help="Unraveling options for image processing. default is reshape.")
modelgroup.add_argument("--image-ordering",default="eta-based", dest="ORDERING", type = str,
                        choices = ["eta-based", "phi-based"],
                        help="Pixel ordering; based on eta or phi. default is eta-based.")
modelgroup.add_argument("--continue",default=None, dest="MPSMODEL", type=str,
                        help="Continue from an already existing model.")
modelgroup.add_argument("--compress-de",default=0.0, dest="COMPRESSDE", type=float,
                        help="DeltaS compression. To be used on continuation.")
modelgroup.add_argument("--compress-entropy-threshold",default=0.0, dest="ENTTHRES", type=float,
                        help="Entropy threshold compression. To be used on continuation.")

# ================== #
# ==== Training ==== #
# ================== #

traingroup = parser.add_argument_group("Training handling")
traingroup.add_argument("OPTIMIZATIONMODE", default="sweep-based-fit",
                        choices = ["sweep-based-fit", "mixed-fit", "epoch-based-fit"],
                        help="Set optimization mode. `sweep-based-fit` runs DMRG algorighm over" \
                             "batches and saves sweep history. `mixed-fit` runs DMRG algorithm at" \
                             "the beginning of each epoch and then optimizes the MPS using traditional" \
                             "stochastic gradient decent algorithms.")
traingroup.add_argument("-lr","--learning-rate", default=1e-5,dest="LR",type=float,
                        metavar = "FLOAT", help="Learning rate, default 1e-5")
traingroup.add_argument("--epochs", default=1, dest="EPOCHS",type=int,
                        metavar = "INT", help="Number of epochs to run during training,"
                                              " default is 1")
traingroup.add_argument("--nsweep-per-batch",default=1,dest="NSWEEP",type=int,metavar = "INT",
                        help = "Number of sweeps to run per batch, default is 1. "
                               "Only available in adaptive mode")
traingroup.add_argument("--dmrg-frac",default=None,dest="DMRGFRAC",type=float,metavar = "FLOAT",
                        help = "(mixed fit only) fraction of DMRG sweeps per epoch, "
                               "default is one DMRG sweep per epoch.")
traingroup.add_argument("-dp","--dropout", default=0.0, dest="DROPOUT", type=float,
                        help="Dropout probability, default is 0: no dropout")
traingroup.add_argument("-ng", "--normalize-grad", dest="NORMALIZEGRAD", default=False,
                        action="store_true", help="Normalize gradient tensor before update, "
                                                  "default is False")
traingroup.add_argument("--lambda-normb", default=0.0, type=float, dest="LAMBDANORMB",
                        help="L2 regularization for squared norm of the connected tensors to be updated.")
traingroup.add_argument("--optimizer", default=None, dest="OPTIMIZER", type=str,
                        help="Optimizer, default is simple gradient decent. "
                             "For mixed-fit default is adam.",
                        choices = ["adam","adadelta","adagrad","adamax","sgd","rmsprop"])
traingroup.add_argument("--activation", default=None, choices=["relu", "softmax", "square"],
                        dest="ACTIVATION", type=str,
                        help="Activation function for the MPS, default is None.")
traingroup.add_argument("--loss", default=None, type=str, dest="LOSS",
                        choices=["categorical_crossentropy", "binary_crossentropy",
                                 "sparse_categorical_crossentropy", "kl_divergence"],
                        help="Loss function, default is MSE.")

traingroup.add_argument("--store-mps-evolution", dest="MPSEVO", default=None, type=int,
                        help="Store MPS evolution. This value will indicate the epochs to be stored.")

traingroup.add_argument("--time-benchmark", dest="GETTIME", default=False, action="store_true",
                        help="Print timing of each step.")

# =================== #
# ==== Callbacks ==== #
# =================== #
callbackgroup = parser.add_argument_group("Callback handling")
callbackgroup.add_argument("--reducelr-factor", default=None, dest="REDUCELR_FACTOR", type=float,
                           metavar = "FLOAT", help = "ReduceLRonPlateau LR decay factor. ")
callbackgroup.add_argument("--reducelr-patience", default=None, dest="REDUCELR_PATIENCE", type=int,
                           metavar = "INT", help = "ReduceLRonPlateau patience. ")
callbackgroup.add_argument("--reducelr-minlr", default=None, dest="REDUCELR_MINLR", type=float,
                           metavar = "FLOAT", help = "ReduceLRonPlateau minimum LR. ")
callbackgroup.add_argument("--reducelr-mindelta", default=None, dest="REDUCELR_MINDELTA", type=float,
                           metavar = "FLOAT", help = "ReduceLRonPlateau minimum change in validation loss. ")
callbackgroup.add_argument("--increasebond-factor", default=None, dest="INCREASEBOND_FACTOR",
                           type=float, metavar = "INT", help = "GradualIncrease auxiliary dim increase factor.")
callbackgroup.add_argument("--increasebond-patience", default=None, dest="INCREASEBOND_PATIENCE",
                           type=int, metavar = "INT", help = "GradualIncrease patience. ")
callbackgroup.add_argument("--increasebond-maxbond", default=None, dest="INCREASEBOND_MAXBOND",
                           type=int, metavar = "INT", help = "GradualIncrease maximum auxiliary dimensions.")
callbackgroup.add_argument("--earlystopping-mindelta", default=1e-3, dest="EARLYSTOPPING_MINDELTA",
                           type=float, metavar = "FLOAT",
                           help = "Stop the training if change in the val_loss is smaller than "
                                  "this value. Default is 1e-3.")
callbackgroup.add_argument("--earlystopping-patience", default=20, dest="EARLYSTOPPING_PATIENCE",
                           type=int, metavar = "INT",
                           help = "Patience for early stopping, default is 20.")
# ================ #
# ==== Sample ==== #
# ================ #

samplegroup = parser.add_argument_group("Sample handling")
samplegroup.add_argument("-mnist","--train-mnist", default=False, action="store_true",
                         dest="MNIST", help="Train on MNIST dataset.")
samplegroup.add_argument( "-tb", "--train-batch-size", type=int, dest="train_batch",
                          default=10000, help="Batch size for the training sample, "
                                              "default is 10000")
samplegroup.add_argument( "-vb", "--val-batch-size", type=int, dest="val_batch", default=10000,
                          help="Batch size for the validation sample, default is 10000")
samplegroup.add_argument("--slice", default=8, dest="SLICE", type=int, metavar="INT",
                         help="Throw X number of pixels from top, bottom, "
                              "left and right of the image, default is 8")
samplegroup.add_argument( "-nt", "--ntrain", type=int, dest="ntrain", default=122, metavar = "INT",
                          help="Number of files to be included during training, default is 122",
                          choices=list(range(1,123)))
samplegroup.add_argument( "--downsample", dest="DOWNSAMPLE", default=False, action="store_true",
                          help="Downsample the image by averaging every 4 square pixels. "
                               "With the default slice this reduces the number of sites to 100.")
samplegroup.add_argument( "--pad", dest="PAD", default=False, action="store_true",
                          help="Pad before downsampling")
samplegroup.add_argument("--mapping", default="hypersphere", dest="MAPPING", type=str,
                         choices = ["hypersphere","polynomial"],
                         help="Feature tensor mapping, default is hypersphere")
samplegroup.add_argument("--mapping-dim", default=2, dest="MAPPINGDIM", type=int,
                         help="mapping dimensions, default is 2 which corresponds to "
                              "[cos(x pi/2), sin(x pi/2)] for a hypersphere and [1,x] for "
                              "polynomial mapping.")

samplegroup.add_argument("-rnn","--train-rnn", default=False, action="store_true",
                         dest="RNN", help="Train on RNN type dataset.")
samplegroup.add_argument("--rnn-normalize-by", default=None, type=float, dest="RNNNORM",
                         help="Instead of using robust scaler, normalize the RNN sequence")
samplegroup.add_argument("--rnn-cutoff", default=0., type=float, dest="RNNCUTOFF",
                         help="Cut off for the values in RNN sequence.")
samplegroup.add_argument("--rnn-sequence-lim", default=[2,18,10,10], dest="RNNSEQLIM", action=SplitArgs,
                         help="Reduce the number of elements in RNN sequence. First term is for jet masses,"
                              "and following 3 term is for leading, second leading and third "
                              "leading branch respectively. "
                              "Values are splitted via comma without space. ")


paths = parser.add_argument_group("Paths")
paths.add_argument( "--data-path", type=str, dest="data_path",
                    default="/mt/user-batch/jaraz/RNN_CNN/EnsembleNN/samples_v2",
                    help="Location of the training, validation and test data.")
paths.add_argument("--out-path", type=str, dest="output_path", default="output",
                   help="Output location.")


args = parser.parse_args()

output_main = "MPS" if not args.MNIST else "MNIST"

if args.OPTIMIZATIONMODE == "mixed-fit" and args.OPTIMIZER is None:
    args.OPTIMIZER = "adam"


if args.LABELPOS != "beginning" and not args.FIXEDLABEL:
    print("   * WARNING: Floating label analysis requires label to be at the beginning. "
          "Label tensor will be automatically shifted to the center position")

# Set callbacks
REDUCELR_args     = [item for key, item in vars(args).items() if "REDUCELR"     in key ]
INCREASEBOND_args = [item for key, item in vars(args).items() if "INCREASEBOND" in key ]

if any([item is not None for item in REDUCELR_args]):
    args.REDUCELR_FACTOR   = args.REDUCELR_FACTOR   if args.REDUCELR_FACTOR   is not None else 0.5
    args.REDUCELR_PATIENCE = args.REDUCELR_PATIENCE if args.REDUCELR_PATIENCE is not None else 10
    args.REDUCELR_MINLR    = args.REDUCELR_MINLR    if args.REDUCELR_MINLR    is not None else 1e-10
    args.REDUCELR_MINDELTA = args.REDUCELR_MINDELTA if args.REDUCELR_MINDELTA is not None else 1e-2
if any([item is not None for item in INCREASEBOND_args]):
    args.INCREASEBOND_FACTOR   = args.INCREASEBOND_FACTOR   if args.INCREASEBOND_FACTOR   is not None else 2
    args.INCREASEBOND_PATIENCE = args.INCREASEBOND_PATIENCE if args.INCREASEBOND_PATIENCE is not None else 1
    args.INCREASEBOND_MAXBOND  = args.INCREASEBOND_MAXBOND  if args.INCREASEBOND_MAXBOND  is not None else 10


if not os.path.isdir(args.output_path):
    os.mkdir(args.output_path)
if not os.path.isdir(os.path.join(args.output_path, output_main)):
    os.mkdir(os.path.join(args.output_path, output_main))

if args.MPSMODEL is None:
    n_folder = [int(x.split('_')[-1])
                for x in glb(os.path.join(args.output_path,  output_main, args.name+'_*'))]
    n_folder = 0 if n_folder == [] else max(n_folder)+1
    output_path = os.path.join(args.output_path, output_main, args.name+'_'+str(n_folder))
else:
    output_path = args.MPSMODEL

if not os.path.isdir(output_path):
    os.mkdir(output_path)

print(f"   * Running MPS: {output_path}")

if args.MNIST:
    args.NLABELS = 10

log = {}
for arg, value in sorted(vars(args).items()):
    log[arg] = value
    print("      - "+arg.ljust(25) + f" : {value}" )

import json
with open(os.path.join(output_path,"arguments.log"),'w') as f:
    f.write(json.dumps(log, indent=4))

# ========================= #
# ==== Prepare Samples ==== #
# ========================= #

import TNClassifier as tnc

if not os.path.isdir(args.data_path):
    raise tnc.system.exceptions.InvalidPath(args.data_path)

sort = lambda x : int(x.split('_')[-1].split('.')[0])

if not args.RNN:
    train_dict = {'bkg' : glb(args.data_path+'/train/QCD_CovNet_*')[:args.ntrain],
                  'sig' : glb(args.data_path+'/train/tt_CovNet_*')[:args.ntrain]}
    val_dict   = {'bkg' : glb(args.data_path+'/val/QCD_CovNet_*'),
                  'sig' : glb(args.data_path+'/val/tt_CovNet_*')}
else:
    train_dict = {'bkg' : glb(args.data_path+'/train/QCD_LSTM_*')[:args.ntrain],
                  'sig' : glb(args.data_path+'/train/tt_LSTM_*')[:args.ntrain]}
    val_dict   = {'bkg' : glb(args.data_path+'/val/QCD_LSTM_*'),
                  'sig' : glb(args.data_path+'/val/tt_LSTM_*')}

train_dict["sig"].sort(key=sort); train_dict["sig"] = train_dict["sig"][:args.ntrain]
train_dict["bkg"].sort(key=sort); train_dict["bkg"] = train_dict["bkg"][:args.ntrain]


label_format = "onehot" if args.NLABELS == 2 else "binary"

# ======================= #
# ==== Adaptive Mode ==== #
# ======================= #
import numpy as np
from math import factorial
if args.MAPPING == "hypersphere":
    def hypersphere(x, dim):
        binom_coeff = lambda a,b: factorial(a) / (factorial(b) * factorial(a-b))
        return [
            tf.cast(tf.math.sqrt(binom_coeff(dim-1,d-1)), dtype=x.dtype) * \
            tf.math.cos(x*np.pi*0.5)**(dim - d) * tf.math.sin(x*np.pi*0.5)**(d-1)
            for d in range(1,dim+1)
        ]
    phi = lambda im : hypersphere(im, args.MAPPINGDIM)
    # phi = lambda im: [tf.math.cos(im * np.pi * 0.5), tf.math.sin(im * np.pi * 0.5)]
elif args.MAPPING == "polynomial":
    phi = lambda im: [tf.pow(im, d) for d in range(args.MAPPINGDIM)]


if args.MNIST:
    training_samples = tnc.DataGeneratorMNIST.train(
        batch_size = args.train_batch,
        shuffle = True,
        image_slice = args.SLICE,
        downsample = args.DOWNSAMPLE,
        pad_downsample = args.PAD,
        image_processor = phi,
    )

    validation_samples = tnc.DataGeneratorMNIST.validation(
        batch_size = args.val_batch,
        shuffle = True,
        image_slice = args.SLICE,
        downsample = args.DOWNSAMPLE,
        pad_downsample = args.PAD,
        image_processor = phi,
    )

else:

    if not args.RNN:
        training_samples = tnc.DataGenerator(
            train_dict,
            batch_size = args.train_batch,
            shuffle = True,
            label_format = label_format,
            image_slice = args.SLICE,
            downsample = args.DOWNSAMPLE,
            pad_downsample = args.PAD,
            image_processor = phi,
            unravel_mode = args.UNRAVELMODE,
            order = args.ORDERING,
        )

        validation_samples = tnc.DataGenerator(
            val_dict,
            batch_size = args.val_batch,
            shuffle = True,
            label_format = label_format,
            image_slice = args.SLICE,
            downsample = args.DOWNSAMPLE,
            pad_downsample = args.PAD,
            image_processor = phi,
            unravel_mode = args.UNRAVELMODE,
            order = args.ORDERING,
        )

    else:
        if args.RNNNORM is None:
            class lstm_std:
                def __init__(self,train_dict):
                    from sklearn.preprocessing import RobustScaler
                    # minmaxscaler ruins the outliers which are crutial for kT distances
                    X = np.zeros(sum(args.RNNSEQLIM))
                    i=0
                    while X.shape[0]<100000 and i<len(train_dict['sig']):
                        sig, bkg = list(zip(train_dict['sig'], train_dict['bkg']))[i]

                        data = np.vstack((np.load(sig)['sample'], np.load(bkg)['sample']))

                        mass                  = data[:,:2][:,:args.RNNSEQLIM[0]]
                        leading_branch        = data[:,2:20][:,:args.RNNSEQLIM[1]]
                        second_leading_branch = data[:,20:30][:,:args.RNNSEQLIM[2]]
                        third_leading_branch  = data[:,30:40][:,:args.RNNSEQLIM[3]]

                        mass                  = np.where(mass>args.RNNCUTOFF, mass, 0.)
                        leading_branch        = np.where(leading_branch>args.RNNCUTOFF, leading_branch, 0.)
                        second_leading_branch = np.where(
                            second_leading_branch>args.RNNCUTOFF, second_leading_branch, 0.
                        )
                        third_leading_branch  = np.where(
                            third_leading_branch>args.RNNCUTOFF, third_leading_branch, 0.
                        )

                        data = np.hstack(
                            (mass, leading_branch, second_leading_branch, third_leading_branch)
                        )

                        X = np.vstack((X,data))
                        i+=1
                    X = X[1:]
                    np.random.shuffle(X)
                    self.transformer = RobustScaler().fit(X)
                def __call__(self,X):
                    return self.transformer.transform(X)
            lstm_std = lstm_std(train_dict)
        else:
            lstm_std = lambda x: x/args.RNNNORM

        training_samples = tnc.DataGeneratorRNN(
            train_dict,
            batch_size = args.train_batch,
            shuffle = True,
            label_format = label_format,
            processor = phi,
            standardize = lstm_std,
            cutoff = args.RNNCUTOFF,
            trim = args.RNNSEQLIM,
        )

        validation_samples = tnc.DataGeneratorRNN(
            val_dict,
            batch_size = args.val_batch,
            shuffle = True,
            label_format = label_format,
            processor = phi,
            standardize = lstm_std,
            cutoff = args.RNNCUTOFF,
            trim = args.RNNSEQLIM,
        )

# ============= #
# ==== MPS ==== #
# ============= #
if args.MPSMODEL is None:
    mps = tnc.mps.random(
        N_sites = training_samples.n_pixels, #args.NSITES,
        physical_dim = args.MAPPINGDIM,
        bond_dim = args.BOND,
        n_labels = args.NLABELS,
        label_position = args.LABELPOS,
        max_truncation_err = args.SVDERR,
        max_singular_values = args.SVDVAL,
        trimming_threshold = args.TRIMTHRES,
        dropout = args.DROPOUT,
    )
else:
    if os.path.isfile(os.path.join(args.MPSMODEL, "mps.h5")):
        mps = tnc.mps.load_from_file(os.path.join(args.MPSMODEL, "mps.h5"))
        if args.COMPRESSDE != 0.0 or args.ENTTHRES != 0.0:
            mps = mps.compressed_prediction(
                dE = args.COMPRESSDE, entropy_threshold = args.ENTTHRES
            )
            mps.trimming_threshold = args.TRIMTHRES
            mps.max_singular_values = args.SVDVAL
            mps.max_truncation_err = args.SVDERR
    else:
        tnc.system.exceptions.InvalidPath(os.path.join(args.MPSMODEL, "mps.h5") + \
                                          " file does not exist.")

import tensorflow as tf
AUC = tf.metrics.AUC(curve='ROC',
                     summation_method='interpolation',
                     name="auc",
                     multi_label=False)
Precision = tf.metrics.Precision(name = "precision")
KL = tf.metrics.KLDivergence(name='kld')
MSE = tf.metrics.MeanSquaredError(name="mse")

mps.metrics = [MSE] + (args.ACTIVATION is not None)*[AUC, Precision] + \
              (args.LOSS != "kl_divergence")*[KL]

mps.normalized_gradient = args.NORMALIZEGRAD
mps.lambda_normB        = args.LAMBDANORMB

if args.ACTIVATION is not None:
    if args.ACTIVATION == "relu":
        mps.activation = tf.nn.relu
    elif args.ACTIVATION == "softmax":
        mps.activation = tf.nn.softmax
    elif args.ACTIVATION == "square":
        mps.activation = tf.square

if args.LOSS is not None:
    if args.LOSS == "categorical_crossentropy":
        mps.loss = tf.losses.CategoricalCrossentropy()
    elif args.LOSS == "binary_crossentropy":
        mps.loss = tf.losses.BinaryCrossentropy()
    elif args.LOSS == "sparse_categorical_crossentropy":
        mps.loss = tf.losses.SparseCategoricalCrossentropy()
    elif args.LOSS == "kl_divergence":
        mps.loss = tf.losses.KLDivergence()
    # elif args.LOSS == "xentropy_kl":
    #     xentropy = tf.losses.CategoricalCrossentropy()
    #     kl = tf.losses.KLDivergence()
    #     mps.loss = lambda label, yhat: xentropy(label, yhat) + kl(label, yhat)

optimizer = args.LR
if args.OPTIMIZER is not None:
    optimizer = tf.optimizers.get(args.OPTIMIZER)
    optimizer.lr = args.LR
elif args.OPTIMIZER is None and args.OPTIMIZATIONMODE == "mixed-fit":
    optimizer = tf.optimizers.get("adam")
    optimizer.lr = args.LR

if args.CANONICALIZE:
    mps.canonicalize()

# =================== #
# ==== CallBacks ==== #
# =================== #

from TNClassifier.callbacks.callbacks import ModelCheckpoint, EarlyStopping

epoch_callbacks = [
    ModelCheckpoint(filepath = os.path.join(output_path,"mps.h5")),
    EarlyStopping(
        min_delta = args.EARLYSTOPPING_MINDELTA, patience = args.EARLYSTOPPING_PATIENCE
    ),
]

if any([item is not None for item in REDUCELR_args]):
    from TNClassifier.callbacks.LRscheduler import ReduceLRonPlateau

    reduceLR = ReduceLRonPlateau(
        factor    = args.REDUCELR_FACTOR,
        patience  = args.REDUCELR_PATIENCE,
        min_delta = args.REDUCELR_MINDELTA,
        min_lr    = args.REDUCELR_MINLR
    )
    epoch_callbacks.append(reduceLR)


if any([item is not None for item in INCREASEBOND_args]):
    from TNClassifier.callbacks.BondDimScheduler import GradualIncrease

    auxincrease = GradualIncrease(
        factor   = args.INCREASEBOND_FACTOR,
        patience = args.INCREASEBOND_PATIENCE,
        max_bond = args.INCREASEBOND_MAXBOND,
    )
    epoch_callbacks.append(auxincrease)


class EntropyEvolution:
    def __init__(self):
        self.entropy = []

    def __call__(self,training_suite):
        if isinstance(training_suite, tnc.MPSTrainingSuite):
            entropy, _ = training_suite.network.entanglement_entropy
        else:
            entropy, _ = training_suite.entanglement_entropy
        self.entropy.append(entropy)

    def save(self, filepath):
        np.savez_compressed(os.path.join(filepath,"entropy.npz"), entropy=self.entropy)

entropyevolution = EntropyEvolution()
entropyevolution(mps)
epoch_callbacks.append(entropyevolution)

class MPSEvolution:
    def __init__(self, epochs, path):
        self.epochs = epochs
        self.path = path
        if epochs is not None:
            if not os.path.isdir(os.path.join(path,"mps_evo")):
                os.mkdir(os.path.join(path,"mps_evo"))
                self.path = os.path.join(path,"mps_evo")

    def __call__(self, training_suite):
        if self.epochs is not None:
            if isinstance(training_suite, tnc.MPSTrainingSuite):
                if training_suite._epoch % self.epochs == 0:
                    training_suite.network.save_mps(
                        os.path.join(self.path, f"epoch_{training_suite._epoch}.h5")
                    )
            else:
                training_suite.save_mps(
                    os.path.join(self.path, f"epoch_init.h5")
                )

mps_evo = MPSEvolution(args.MPSEVO, output_path)
mps_evo(mps)
epoch_callbacks.append(mps_evo)

# ================== #
# ==== Training ==== #
# ================== #

DMRGFRAC = args.DMRGFRAC if args.DMRGFRAC is not None else 1./len(training_samples)

with tnc.MPSTrainingSuite(
        training_generator=training_samples,
        validation_generator = validation_samples,
        network=mps) as training:

        if args.OPTIMIZATIONMODE == "sweep-based-fit":
            history = training.sweep_based_fit(
                epochs = args.EPOCHS,
                n_sweep_per_batch = args.NSWEEP,
                optimizer = optimizer,
                fixed_label = args.FIXEDLABEL,
                epoch_callbacks = epoch_callbacks,
                time_benchmark = args.GETTIME,
                # sweep_callbacks = LRdecay_sweep
            )
        elif args.OPTIMIZATIONMODE == "epoch-based-fit":
            history = training.fit(
                epochs = args.EPOCHS,
                n_sweep_per_batch = args.NSWEEP,
                learning_rate = args.LR,
                fixed_label = args.FIXEDLABEL,
                callbacks = epoch_callbacks
            )
        elif args.OPTIMIZATIONMODE == "mixed-fit":
            history = training.mixed_fit(
                epochs = args.EPOCHS,
                optimizer = optimizer,
                n_sweep_per_epoch = args.NSWEEP,
                DMRG_frac = DMRGFRAC,
                fixed_label = args.FIXEDLABEL,
                epoch_callbacks = epoch_callbacks,
                time_benchmark = args.GETTIME,
            )


# ================= #
# ==== Outputs ==== #
# ================= #

import matplotlib.pyplot as plt

if args.OPTIMIZATIONMODE == "sweep-based-fit":
    fig = plt.figure(figsize=(8, 7), facecolor='w', edgecolor='k')
    plt.plot(history["train_sweep_loss"])
    plt.plot(history["val_sweep_loss"])
    plt.legend(["train","val"])
    plt.title("Loss per sweep")
    plt.xlabel("sweep")
    plt.yscale('log')
    plt.savefig(output_path+'/loss_sweep.png',bbox_inches='tight')
    plt.close()

    fig = plt.figure(figsize=(8, 7), facecolor='w', edgecolor='k')
    plt.plot(history["train_sweep_acc"])
    plt.plot(history["val_sweep_acc"])
    plt.legend(["train","val"])
    plt.title("Accuracy per sweep")
    plt.xlabel("sweep")
    plt.yscale('log')
    plt.savefig(output_path+'/acc_sweep.png',bbox_inches='tight')
    plt.close()

    for metric in mps.metrics:
        name = metric.name
        fig = plt.figure(figsize=(8, 7), facecolor='w', edgecolor='k')
        plt.plot(history["train_sweep_"+name])
        plt.plot(history["val_sweep_"+name])
        plt.legend(["train","val"])
        plt.title(name+" per sweep")
        plt.xlabel("sweep")
        plt.yscale('log')
        plt.savefig(output_path+'/'+name+'_sweep.png',bbox_inches='tight')
        plt.close()

if args.EPOCHS > 1:
    fig = plt.figure(figsize=(8, 7), facecolor='w', edgecolor='k')
    plt.plot(history["train_loss"])
    plt.plot(history["val_loss"])
    plt.legend(["train","val"])
    plt.title("Loss per epoch")
    plt.xlabel("epoch")
    plt.yscale('log')
    plt.savefig(output_path+'/loss.png',bbox_inches='tight')
    plt.close()

    fig = plt.figure(figsize=(8, 7), facecolor='w', edgecolor='k')
    plt.plot(history["train_acc"])
    plt.plot(history["val_acc"])
    plt.legend(["train","val"])
    plt.title("Accuracy per epoch")
    plt.xlabel("epoch")
    plt.yscale('log')
    plt.savefig(output_path+'/acc.png',bbox_inches='tight')
    plt.close()

    if "num_params" in history.keys():
        fig = plt.figure(figsize=(8, 7), facecolor='w', edgecolor='k')
        plt.plot(history["num_params"])
        plt.xlabel("epoch")
        plt.ylabel("Number of parameters")
        plt.savefig(output_path+'/num_params.png',bbox_inches='tight')
        plt.close()

    for metric in mps.metrics:
        name = metric.name
        fig = plt.figure(figsize=(8, 7), facecolor='w', edgecolor='k')
        plt.plot(history["train_"+name])
        plt.plot(history["val_"+name])
        plt.legend(["train","val"])
        plt.title(name+" per Epoch")
        plt.xlabel("epoch")
        plt.yscale('log')
        plt.savefig(output_path+'/'+name+'.png',bbox_inches='tight')
        plt.close()

entropy, _ = mps.entanglement_entropy
fig = plt.figure(figsize=(8, 7), facecolor='w', edgecolor='k')
plt.plot(entropy)
plt.xlabel("site")
plt.ylabel("Entanglement entropy")
plt.savefig(output_path+'/entanglement_entropy.png',bbox_inches='tight')
plt.close()

print('   * Output Path : '+ os.path.join(output_path))


sweep_history, epoch_history = {}, {}
for key, item in history.items():
    if "sweep" in key:
        sweep_history[key] = item
    else:
        epoch_history[key] = item

if sweep_history != {}:
    model_history = pd.DataFrame.from_dict(sweep_history)
    model_history.to_csv(output_path+'/model_sweep_history.csv.gz')

maxlen = max([len(item) for key, item in epoch_history.items()])
for key, item in epoch_history.items():
    if len(item) < maxlen:
        epoch_history[key] = item + [0.]*(maxlen - len(item))

epoch_history = pd.DataFrame.from_dict(epoch_history)
epoch_history.to_csv(output_path+'/model_epoch_history.csv.gz')
entropyevolution.save(output_path)