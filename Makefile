#! /usr/bin/env bash


.PHONY: clean
clean:
	rm -rf tnclassifier.egg-info/


.PHONY: requirements
requirements: requirements.txt
	pip install -r requirements.txt


.PHONY: install
install:
	pip install -e .


.PHONY: all
all:
	make requirements
	make install
